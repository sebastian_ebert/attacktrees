﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.Trees;


namespace UserInterface.Samples
{
    

    public abstract class SampleTree1
    {

        public static Tree TreeSample()
        {
            var tree = new Tree();
            var root = new Node("Burgle House");
            Node node1 = new Node(" Defeat Door security");
            node1.AddAttribute("Possible", true);
            node1.AddAttribute("Cost", 100m);
            Node node2 = new Node(" Open front OR back doors ");
            node2.AddAttribute("Possible", true);
            node2.AddAttribute("Cost", 25m);
            Node node3 = new Node(" Open Door using gift card or credit card");
            node3.AddAttribute("Possible", true);
            node3.AddAttribute("Cost", 70m);
            Node node4 = new Node(" using trees");
            node4.AddAttribute("Possible", true);
            node4.AddAttribute("Cost", 80m);
            Node node5 = new Node(" Enter via Windows ");
            node5.AddAttribute("Possible", true);
            node5.AddAttribute("Cost", 100m);
            Node node6 = new Node(" Cover the window in duct tape and gently shatter the entire window");
            node6.AddAttribute("Possible", true);
            node6.AddAttribute("Cost", 20m);
            Node node7 = new Node(" Garage Attack ");
            node7.AddAttribute("Cost", 30m);
            node7.AddAttribute("Possible", true);
            Node node8 = new Node(" Enter Garage ");
            node8.AddAttribute("Possible", true);
            node8.AddAttribute("Cost", 50m);
            Node node9 = new Node(" Open car door ");
            node9.AddAttribute("Possible", true);
            node9.AddAttribute("Cost", 50m);
            Node node10 = new Node(" Brutal Force on the Garage Door)");
            node10.AddAttribute("Possible", true);
            node10.AddAttribute("Cost", 250m);
            Node node11 = new Node(" Put Door opener code");
            node11.AddAttribute("Possible", true);
            node11.AddAttribute("Cost", 50m);
            Node node12 = new Node(" EavesDrop opener code)");
            node12.AddAttribute("Possible", true);
            node12.AddAttribute("Cost", 500m);
            Node node13 = new Node(" Steal opener from Car)");
            node13.AddAttribute("Possible", true);
            node13.AddAttribute("Cost", 30m);
            Node node14 = new Node(" Cut the hole in the wall or roof ");
            node14.AddAttribute("Possible", false);
            node14.AddAttribute("Cost", 250m);
            Node node15 = new Node(" Penitrate House ");
            node15.AddAttribute("Possible", true);
            node15.AddAttribute("Cost", 60m);
            Node node16 = new Node(" Open passger door ");
            node16.AddAttribute("Possible", true);
            node16.AddAttribute("Cost", 20m);
            Node node17 = new Node("break Down the passanger door");
            node17.AddAttribute("Possible", true);
            node17.AddAttribute("Cost", 25m);
            Node node18 = new Node(" Pick lock");
            node18.AddAttribute("Possible", false);
            node18.AddAttribute("Cost", 250m);
            Node node19 = new Node(" Steal Keys");
            node19.AddAttribute("Possible", true);
            node19.AddAttribute("Cost", 20m);
            Node node20 = new Node(" Cut the hole in the wall or roof ");
            node20.AddAttribute("Possible", false);
            node20.AddAttribute("Cost", 250m);
            Node node21 = new Node(" Cut the wall or roof ");
            node21.AddAttribute("Possible", false);
            node21.AddAttribute("Cost", 250m);
            Node node22 = new Node(" Chimney attack ");
            node22.AddAttribute("Possible", false);
            node22.AddAttribute("Cost", 25m);
            Node node23 = new Node(" Tunnel through the floor ");
            node23.AddAttribute("Possible", false);
            node23.AddAttribute("Cost", 2000m);
            Node node24 = new Node(" Social engineering ");
            node24.AddAttribute("Possible", false);
            node24.AddAttribute("Cost", 50m);
            Node node25 = new Node(" A bomb blast");
            node25.AddAttribute("Possible", false);
            node25.AddAttribute("Cost", 300m);
            Node node26 = new Node(" Call a locksmith");
            node26.AddAttribute("Possible", false);
            node26.AddAttribute("Cost", 200m);









            tree.AddNode(root, null);
            tree.AddNode(node1, root);
            tree.AddNode(node2, node1);
            tree.AddNode(node3, node1);
            tree.AddNode(node4, root);
            tree.AddNode(node5, root);
            tree.AddNode(node6, node5);
            tree.AddNode(node7, root);
            tree.AddNode(node8, node7);
            tree.AddNode(node9, node8);
            tree.AddNode(node10, node9);
            tree.AddNode(node11, node9);
            tree.AddNode(node12, node11);
            tree.AddNode(node13, node11);
            tree.AddNode(node14, node8);
            tree.AddNode(node15, node7);
            tree.AddNode(node16, node15);
            tree.AddNode(node17, node16);
            tree.AddNode(node18, node16);
            tree.AddNode(node19, node16);
            tree.AddNode(node20, node15);
            tree.AddNode(node21, root);
            tree.AddNode(node22, root);
            tree.AddNode(node23, root);
            tree.AddNode(node24, root);
            tree.AddNode(node25, root);
            tree.AddNode(node26, root);




            return tree;
        }

    }
}
