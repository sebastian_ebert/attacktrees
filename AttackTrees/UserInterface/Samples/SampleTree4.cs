﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.Trees;

namespace UserInterface.Samples
{
    public abstract class SampleTree4
    {
        public static Tree TreeSample()
        {
            Tree tree = new Tree();
            int level = 3;
            var root = new Node("Root");
            var parent = root;
            tree.AddNode(root, null);
            Random rand = new Random();
            List<Node> nodelist = new List<Node>();
            nodelist.Add(root);        

            for (int i = 0; i < level; i++)
            {
                nodelist = GenerateLevel(nodelist, tree,i);
                
            }   
            return tree;
        }
        public static List<Node> GenerateLevel(List<Node> parentList, Tree tree,int level)
        {
            List<Node> childList = new List<Node>();
            Random rand = new Random();
            for (int i = 0; i < parentList.Count(); i++)
            {

                Node currentNode = parentList[i];
                
                int nodeCount = rand.Next(0,5);
                for (int j = 0; j < nodeCount; j++)
                {
                    Node nodenew = new Node("node" + level + i + j);
                    tree.AddNode(nodenew, currentNode);
                   
                    childList.Add(nodenew);
                }
            }

            return childList;

        }

    }
}
