﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.Trees;

namespace UserInterface.Samples
{
	public abstract class SampleTree6
	{
		
		public static Tree TreeSample()
		{
			
			var tree = new Tree();
			
			var root = new Node("root node 0");
			Node node1 = new Node("node 1");
			Node node2 = new Node("node 2");
			Node node3 = new Node("node 3");
			Node node4 = new Node("node 4");
			Node node5 = new Node("Node 5");
			Node node6 = new Node("Node 6");
			Node node7 = new Node("Node 7");
			Node node8 = new Node("Node 8");
			Node node9 = new Node("Node 9");
			Node node10 = new Node("Node 10");
			Node node11 = new Node("Node 11");
			Node node12 = new Node("Node 12");
			Node node13 = new Node("Node 13");
			Node node14 = new Node("Node 14");
			Node node15 = new Node("Node 15");
			Node node16 = new Node("Node 16");
			Node node17 = new Node("Node 17");
			Node node18 = new Node("Node 18");
			Node node19 = new Node("Node 19");
			Node node20 = new Node("Node 20");
			Node node21 = new Node("Node 21");
			Node node22 = new Node("Node 22");
			Node node23 = new Node("Node 23");
			Node node24 = new Node("Node 24");
			

			// define edges
			tree.AddNode(root, null);
			tree.AddNode(node1, root);
			tree.AddNode(node2, root);
			tree.AddNode(node3, root);
			tree.AddNode(node4, root);
			tree.AddNode(node5, node2);
			tree.AddNode(node6, node2);
			tree.AddNode(node7, node2);
			tree.AddNode(node8, node2);
			tree.AddNode(node9, node3);
			tree.AddNode(node10, node3);
			tree.AddNode(node11, node3);
			tree.AddNode(node12, node3);
			tree.AddNode(node13, node3);
			tree.AddNode(node14, node6);
			tree.AddNode(node15, node6);
			tree.AddNode(node16, node6);
			tree.AddNode(node17, node15);
			tree.AddNode(node18, node15);
			tree.AddNode(node19, node15);
			tree.AddNode(node20, node12);
			tree.AddNode(node21, node12);
			tree.AddNode(node22, node21);
			tree.AddNode(node23, node21);
			tree.AddNode(node24, node21);


            var And1 = tree.GetNextAndGroup();
            var edge1 = tree.Edges.ElementAt(1);
            edge1.AndGroup = And1;
            var edge2 = tree.Edges.ElementAt(2);
            edge2.AndGroup = And1;

            var And2 = tree.GetNextAndGroup();
            var edge3 = tree.Edges.ElementAt(6);
            edge3.AndGroup = And2;
            var edge4 = tree.Edges.ElementAt(7);

            var And3 = tree.GetNextAndGroup();
            var edge5 = tree.Edges.ElementAt(14);
            edge5.AndGroup = And3;
            var edge6 = tree.Edges.ElementAt(15);
            edge6.AndGroup = And3;
            var edge7 = tree.Edges.ElementAt(16);
            edge7.AndGroup = And3;

            var And4 = tree.GetNextAndGroup();
            var edge8 = tree.Edges.ElementAt(10);
            edge8.AndGroup = And4;
            var edge9 = tree.Edges.ElementAt(11);
            edge9.AndGroup = And4;
            var edge10 = tree.Edges.ElementAt(12);
            edge10.AndGroup = And4;

            var And5 = tree.GetNextAndGroup();
            var edge11 = tree.Edges.ElementAt(22);
            edge11.AndGroup = And5;
            var edge12 = tree.Edges.ElementAt(23);
            edge12.AndGroup = And5;


           
			return tree;
		}
	}
}

