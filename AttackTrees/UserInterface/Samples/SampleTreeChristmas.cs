﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.Trees;

namespace UserInterface.Samples
{
    public abstract class SampleTreeChristmas
    {

        public static Tree TreeSample()
        {

            var tree = new Tree();


            var root = new Node("*");
            Node node1 = new Node("*");
            Node node2 = new Node("*");
            Node node3 = new Node("*");
            Node node4 = new Node("*");
            Node node5 = new Node("*");
            Node node6 = new Node("*");
            Node node7 = new Node("*");
            Node node8 = new Node("*");
            Node node9 = new Node("*");
            Node node10 = new Node("*");
            Node node11 = new Node("*");
            Node node12 = new Node("*");
            Node node13 = new Node("*");
            Node node14 = new Node("*");
            Node node15 = new Node("*");
            Node node16 = new Node("*");
            Node node17 = new Node("*");
            Node node18 = new Node("*");
            Node node19 = new Node("*");
            Node node20 = new Node("*");
            Node node21 = new Node("*");
            Node node22 = new Node("*");
            Node node23 = new Node("*");
            Node node24 = new Node("*");
            Node node25 = new Node("*");
            Node node26 = new Node("*");
            Node node27 = new Node("*");
            Node node28 = new Node("*");
            Node node29 = new Node("*");
            Node node30 = new Node("*");
            Node node31 = new Node("*");
            Node node32 = new Node("*");
            Node node33 = new Node("*");
            Node node34 = new Node("*");
            Node node35 = new Node("*");
            Node node36 = new Node("*");
            Node node37 = new Node("*");
            Node node38 = new Node("*");
            Node node39 = new Node("*");
            Node node40 = new Node("*");
            Node node41 = new Node("*");
            Node node42 = new Node("*");
            Node node43 = new Node("*");
            

            
            tree.AddNode(root, null);
            tree.AddNode(node1, root);
            tree.AddNode(node2, node1);
            tree.AddNode(node3, node1);
            tree.AddNode(node4, node1);
            tree.AddNode(node5, node2);
            tree.AddNode(node6, node3);
            tree.AddNode(node7, node4);
            tree.AddNode(node8, node5);
            tree.AddNode(node9, node5);
            tree.AddNode(node10, node6);
            tree.AddNode(node11, node7);
            tree.AddNode(node12, node7);
            tree.AddNode(node13, node8);
            tree.AddNode(node14, node8);
            tree.AddNode(node15, node10);
            tree.AddNode(node16, node12);
            tree.AddNode(node17, node12);
            tree.AddNode(node18, node13);
            tree.AddNode(node19, node13);
            tree.AddNode(node20, node14);
            tree.AddNode(node21, node15);
            tree.AddNode(node22, node16);
            tree.AddNode(node23, node17);
            tree.AddNode(node24, node17);
            tree.AddNode(node25, node21);
            tree.AddNode(node26, node25);
            tree.AddNode(node27, node18);
            tree.AddNode(node28, node18);
            tree.AddNode(node29, node24);
            tree.AddNode(node30, node24);
            tree.AddNode(node31, node26);
            tree.AddNode(node32, node22);
            tree.AddNode(node33, node20);
            tree.AddNode(node34, node31);
            tree.AddNode(node35, node27);
            tree.AddNode(node36, node27);
            tree.AddNode(node37, node28);
            tree.AddNode(node38, node32);
            tree.AddNode(node39, node33);
            tree.AddNode(node40, node29);
            tree.AddNode(node41, node30);
            tree.AddNode(node42, node30);
            tree.AddNode(node43, node34);
            
          

            return tree;
        }
    }
}

