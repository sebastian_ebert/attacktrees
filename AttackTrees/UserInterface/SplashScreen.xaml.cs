﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Persistence;


namespace UserInterface
{
    using GraphSharp.Controls;

    using Model;

    using UserInterface.ViewModels;
    using UserInterface.PopupWindows;

    /// <summary>
    /// Interaktionslogik für SplashScreen.xaml
    /// </summary>
    public partial class SplashScreen : Window
    {
        /// <summary>
        /// ModelView
        /// </summary>
        private ViewModels.ModelView modelView;
        
        /// <summary>
        /// Standard constructor
        /// </summary>
        public SplashScreen()
        {
            InitializeComponent();

            // intialize ModelView
            modelView = new ModelView();

            // initialize UserSession
            modelView.Session = new UserSession(UserRepository.Instance, TreeRepository.Instance, FunctionRepository.Instance);
        }



        /// <summary>
        /// UI EVENT | start application with SideMenuControl
        /// </summary>
        private void StartSideMenuControl()
        {
            var mainWindow = new MainWindow(Enumerations.UserInterfaceControlTypes.SideMenuControl);
                mainWindow.Show();
                mainWindow.ModelView = this.modelView;
                this.Close();
        }

        /// <summary>
        /// UI Event | shutdown application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonExitOnClick(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
            //this.StartSideMenuControl();
        }


        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
           // modelView.Session.CreateUser("admin","Bill","Jobs","admin");
            if (Login())
            {
                this.StartSideMenuControl();
            }
            else
            {
                this.LabelLoginMessage.Foreground = Brushes.White;
                this.LoginMessageGrid.Background = Brushes.OrangeRed;
                this.LabelLoginMessage.Content = "Login failed";
            }
            
        }


        private bool Login()
        {
            var username = TextboxUsername.Text;
            var password = PasswordBox.Password;

            User user = null;
            try
            {
                user = modelView.Session.Login(username, password);
                if (user != null)
                {
                    //User non existant if the Salt is set to "notExist"
                    if (user.Salt.Equals("notExist"))
                    {
                        return false;
                    }
                }
            }
            catch (Exception)
            {
                var message = new MessageWindow(WarningSigns.ErrorSign, "Unable to connect to database!", true, false);
                message.ShowDialog();
                Application.Current.Shutdown();
            }
            return user != null ;
        }

        private void TextboxUsername_OnGotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            TextboxUsername.Foreground = Brushes.Black;
            TextboxUsername.Text = "";
        }

        private void PasswordBox_OnGotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            PasswordBox.Foreground = Brushes.Black;
            PasswordBox.Password = "";
        }
    }
}
