﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UserInterface.VisualElements;
using GraphSharp.Controls;
using QuickGraph;
using Model.Trees;
using Model;
using System.ComponentModel;
using Model.Functions;
using UserInterface.Themes;

namespace UserInterface.ViewModels
{
    public struct NodeItem
    {

        public int Index;

        public Node Node;


        public NodeItem(int index, Node node)
        {
            this.Index = index;
            this.Node = node;
        }


        public override string ToString()
        {
            return Index + ". " + Node.Name;
        }

    }

    public struct TreeItem
    {

        public int Index;

        public Tree Tree;


        public TreeItem(int index, Tree tree)
        {
            this.Index = index;
            this.Tree = tree;
        }


        public override string ToString()
        {
            return Index + ". " + Tree.Name;
        }

    }

    public class UiGraphLayout : GraphLayout<Node, Edge, Tree> { }

    public class ModelView : INotifyPropertyChanged
    {
        #region Member

        private string layoutAlgorithmType;
        //private UIGraph graph;

        /// <summary>
        /// Representation of a model tree
        /// </summary>
        private Tree tree;

        private readonly List<String> layoutAlgorithmTypes = new List<string>();
        private int count;

        // Currently loaded and active function
        // TODO: Move this to the user session?
        private Function _function;

        private ColorTheme colorTheme;

        private List<int> maxNodeLevels;

        #endregion


        #region Property

        public List<String> LayoutAlgorithmTypes
        {
            get { return layoutAlgorithmTypes; }
        }


        public string LayoutAlgorithmType
        {
            get { return layoutAlgorithmType; }
            set
            {
                layoutAlgorithmType = value;
                NotifyPropertyChanged("LayoutAlgorithmType");
            }
        }

        public Tree Tree
        {
            get
            {
                return this.tree;
            }
            set
            {

                if (this.tree != value)
                {
                    this.tree = value;
                    Session.CurrentTree = this.tree;
                    this.OnPropertyChanged(new PropertyChangedEventArgs("Tree"));
                }
             
            }
        }

        public Function Function
        {
            get { return _function; }
            set
            {
                if (_function != value)
                {
                    _function = value;
                    HasFunction = (_function != null);
                    FunctionName = (_function != null) ? _function.Name : "<No function>";
                    OnPropertyChanged(new PropertyChangedEventArgs("Function"));
                    OnPropertyChanged(new PropertyChangedEventArgs("HasFunction"));
                    OnPropertyChanged(new PropertyChangedEventArgs("FunctionName"));
                }
            }
        }

        public string FunctionName { get; private set; }

        public List<string> DefaultFunctions { get; private set; }

        public bool HasFunction { get; private set; }

        public UserSession Session { set; get; }

        public List<NodeItem> NodeNames
        {
            get
            {
                var nodeNames = new List<NodeItem>(this.Tree.VertexCount);

                var e = this.Tree.Vertices.GetEnumerator();

                for (var i = 0; i < this.Tree.VertexCount; i++)
                {
                    e.MoveNext();
                    nodeNames.Add(new NodeItem(i + 1, e.Current));

                }
                return nodeNames;
            }
        }

        public List<TreeItem> TreeNames
        {
            get
            {
                var treeNames = new List<TreeItem>(this.Session.GetAllTrees().Count);

                var e = this.Session.GetAllTrees().GetEnumerator();

                for (var i = 0; i < this.Session.GetAllTrees().Count; i++)
                {
                    e.MoveNext();
                    treeNames.Add(new TreeItem(i + 1, e.Current));
                }
                return treeNames;
            }
        }

        public ColorTheme ColorTheme
        {
            set
            {
                this.colorTheme = value;
            }


            get
            {
                return this.colorTheme;
            }
        }

        public List<int> MaxNodeLevels
        {
            private set
            {
                this.maxNodeLevels = value;
            }
            get
            {
                this.SetMaxNodeLevels();
                return this.maxNodeLevels;
            }
        }

        #endregion

        
        public void NotifyTreeChanges()
        {
            
            this.Tree.UpdateAttributes();
            this.Tree.HasChanged = true;
            var nextTree = this.Tree;
            this.Tree = null;
            this.Tree = nextTree;
        }

        public ModelView()
        {
            this.tree = new Tree();
            var rootnode = new Node("Root");
            tree.AddNode(rootnode, null);

            this._function = null;
            this.HasFunction = false;
            this.FunctionName = "<No function>";
            this.InitializeModelView();

            this.DefaultFunctions = new List<string>()
            {
                Model.Functions.DefaultFunctions.LeastCost.Name,
                Model.Functions.DefaultFunctions.NoSpecialToolsRequired.Name,
                Model.Functions.DefaultFunctions.LeastCostAndNoSpecialToolsRequired.Name
            };

            this.SetMaxNodeLevels();
        }

        public void AddNode(Node node, Node parent)
        {
            this.tree.AddNode(node, parent);
            this.tree.ClearResults();
            this.NotifyTreeChanges();
        }

        public void RemoveNode(Node node)
        {
            this.tree.RemoveNode(node);
            this.tree.ClearResults();
            this.NotifyTreeChanges();
        }

        public void UpdateConnection(Node node, Node parent)
        {
            this.tree.UpdateConnection(node, parent);
            this.NotifyTreeChanges();
        }

        public void UpdateNode(Node node)
        {
            this.NotifyTreeChanges();            
        }

        public void UpdateFunction()
        {
            HasFunction = (_function != null);
            FunctionName = (_function != null) ? _function.Name : "<No function>";
            OnPropertyChanged(new PropertyChangedEventArgs("Function"));
            OnPropertyChanged(new PropertyChangedEventArgs("HasFunction"));
            OnPropertyChanged(new PropertyChangedEventArgs("FunctionName"));
        }


        public void LoadSampleTreeData(uint number)
        {
            switch (number)
            {
                case 1:
                    this.Tree = Samples.SampleTree1.TreeSample();
                    this.NotifyTreeChanges();
                    break;

                case 2:
                    this.Tree = Samples.SampleTree2.TreeSample();
                    this.NotifyTreeChanges();
                    break;

                case 3:
                    this.Tree = Samples.SampleTree2.TreeSample();
                    this.NotifyTreeChanges();
                    break;

                case 4:
                    this.Tree = Samples.SampleTree2.TreeSample();
                    this.NotifyTreeChanges();
                    break;

                case 5:
                    this.Tree = Samples.SampleTree2.TreeSample();
                    this.NotifyTreeChanges();
                    break;
            }

        }

        private void InitializeModelView()
        {
            //Add Layout Algorithm Types
            //layoutAlgorithmTypes.Add("BoundedFR");
            //layoutAlgorithmTypes.Add("Circular");
            //layoutAlgorithmTypes.Add("CompoundFDP");
            layoutAlgorithmTypes.Add("EfficientSugiyama");
            //layoutAlgorithmTypes.Add("FR");
            //layoutAlgorithmTypes.Add("ISOM");
            //layoutAlgorithmTypes.Add("KK");
            //layoutAlgorithmTypes.Add("LinLog");
            layoutAlgorithmTypes.Add("Tree");

            //Pick a default Layout Algorithm Type
            LayoutAlgorithmType = "Tree";
        }

        private void SetMaxNodeLevels()
        {
            var list = new List<int>();

            for (var i = 1; i <= this.Tree.GetMaxNodeLevel(); i++)
            {
                list.Add(i);
            }

            this.maxNodeLevels = list;
        }

        #region INotifyPropertyChanged Implementation

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        /// <summary>
        /// Invokes the PropertyChanged event.
        /// </summary>
        /// <param name="args">Event arguments.</param>
        protected virtual void OnPropertyChanged(PropertyChangedEventArgs args)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, args);
        }
        #endregion
    }
}
