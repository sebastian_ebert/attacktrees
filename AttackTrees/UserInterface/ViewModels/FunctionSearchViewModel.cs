﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Model.Functions;
using System.Collections.ObjectModel;
using Model.Trees;
using Model;

namespace UserInterface.ViewModels
{
    class FunctionSearchViewModel : INotifyPropertyChanged
    {
        #region Embedded Classes

        /// <summary>
        /// Attribute types for searching.
        /// </summary>
        public enum SearchType
        {
            /// <summary>
            /// Don't care value.
            /// </summary>
            Any,

            /// <summary>
            /// Same as <see cref="Types.Boolean"/>.
            /// </summary>
            Boolean,

            /// <summary>
            /// Same as <see cref="Types.Number"/>.
            /// </summary>
            Number
        }

        /// <summary>
        /// View model for attributes to search for.
        /// </summary>
        public class AttributeSearchModel : INotifyPropertyChanged
        {

            private string _name;
            private SearchType _type;

            public event PropertyChangedEventHandler PropertyChanged;

            /// <summary>
            /// Gets or sets attribute name.
            /// </summary>
            public string Name
            {
                get { return _name; }
                set
                {
                    if (_name != value)
                    {
                        _name = value;
                        OnPropertyChanged(new PropertyChangedEventArgs("Name"));
                    }
                }
            }

            /// <summary>
            /// Gets or sets attribute type.
            /// </summary>
            public SearchType Type
            {
                get { return _type; }
                set
                {
                    if (_type != value)
                    {
                        _type = value;
                        OnPropertyChanged(new PropertyChangedEventArgs("Type"));
                    }
                }
            }
            

            public AttributeSearchModel()
                : this("", SearchType.Any)
            {
            }

            public AttributeSearchModel(string name, SearchType type)
            {
                this._name = name;
                this._type = type;
            }


            protected virtual void OnPropertyChanged(PropertyChangedEventArgs args)
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, args);
            }

        }

        /// <summary>
        /// View model for search results.
        /// </summary>
        public class ResultModel : INotifyPropertyChanged
        {

            private string _name;
            
            public event PropertyChangedEventHandler PropertyChanged;

            public string Name
            {
                get { return _name; }
                set
                {
                    if (_name != value)
                    {
                        _name = value;
                        OnPropertyChanged(new PropertyChangedEventArgs("Name"));
                    }
                }
            }

            public Function Function { get; private set; }


            public ResultModel(Function function)
            {
                this._name = function.Name;
                this.Function = function;
            }


            protected virtual void OnPropertyChanged(PropertyChangedEventArgs args)
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, args);
            }

        }

        #endregion

        #region Fields

        private Tree _tree;
        private IFunctionRepository _repo;
        private string _name;
        private ResultModel _result;

        #endregion

        #region Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Properties

        public string Name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    OnPropertyChanged(new PropertyChangedEventArgs("Name"));
                    OnPropertyChanged(new PropertyChangedEventArgs("CanSearch"));
                }
            }
        }

        public ObservableCollection<AttributeSearchModel> Attributes { get; private set; }

        public ObservableCollection<ResultModel> Results { get; private set; }

        public ResultModel SelectedResult
        {
            get { return _result; }
            set
            {
                if (_result != value)
                {
                    _result = value;
                    OnPropertyChanged(new PropertyChangedEventArgs("SelectedResult"));
                    OnPropertyChanged(new PropertyChangedEventArgs("HasResult"));
                }
            }
        }

        public SearchType[] Types { get; private set; }

        public bool CanSearch
        {
            get { return Attributes.Count > 0 || !string.IsNullOrWhiteSpace(Name); }
        }

        public bool HasResult
        {
            get { return Results.Count > 0 && SelectedResult != null; }
        }

        #endregion


        public FunctionSearchViewModel(Tree tree, IFunctionRepository repo)
        {
            this._tree = tree;
            this._repo = repo;
            this.Types = (SearchType[])(Enum.GetValues(typeof(SearchType)));
            this.Attributes = new ObservableCollection<AttributeSearchModel>();
            this.Results = new ObservableCollection<ResultModel>();
        }


        public void AddTreeAttributes()
        {
            if (_tree != null && _tree.DefaultAttributes != null && _tree.DefaultAttributes.Count > 0)
            {
                foreach (var attrib in _tree.DefaultAttributes.Values)
                {
                    this.Attributes.Add(new AttributeSearchModel(attrib.Name, FunctionToSearchType(attrib.Type)));
                }
                this.OnPropertyChanged(new PropertyChangedEventArgs("Attributes"));
            }
        }


        private SearchType FunctionToSearchType(Model.Functions.Types type)
        {
            switch (type)
            {
                case Model.Functions.Types.Boolean:
                    return SearchType.Boolean;

                case Model.Functions.Types.Number:
                    return SearchType.Number;

                default:
                    return SearchType.Any;
            }
        }


        public bool Search()
        {
            var resultList = new List<Function>();
            if (!string.IsNullOrWhiteSpace(Name))
            {
                var query = Name;
                if (!query.StartsWith("%"))
                    query = "%" + query;
                if (!query.EndsWith("%"))
                    query = query + "%";
                var namedFuncs = _repo.FindAllByName(query);
                resultList.AddRange(namedFuncs);
            }
            if (Attributes.Count > 0)
            {
                var attribFuncs = _repo.FindByAttributes(Attributes.Select(attrib => attrib.Name));
                foreach (var func in attribFuncs)
                {
                    if (!resultList.Any(f => f.Id == func.Id))
                    {
                        resultList.Add(func);
                    }
                }
            }

            if (resultList.Count <= 0)
                return false;

            this.Results.Clear();
            foreach (var func in resultList)
            {
                this.Results.Add(new ResultModel(func));
            }
            return true;
        }


        protected virtual void OnPropertyChanged(PropertyChangedEventArgs args)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, args);
        }

    }
}
