﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Model.Functions;
using Model.Functions.Operators;
using System.Collections.ObjectModel;
using Model.Trees;
using System.Windows.Media.Imaging;

namespace UserInterface.ViewModels
{
    public class FunctionViewModel : INotifyPropertyChanged
    {

        #region Constants

        public static readonly AttributeInfo[] DefaultAttributes =
        {
            new AttributeInfo("Cost", Types.Number),
            new AttributeInfo("Time Needed", Types.Number),
            new AttributeInfo("Special Tools Required?", Types.Boolean)
        };

        public static readonly BitmapImage IconError;
        public static readonly BitmapImage IconWarning;
        public static readonly BitmapImage IconOk;


        static FunctionViewModel()
        {
            IconError = new BitmapImage(new Uri("/UserInterface;component/Images/error_sign.png", UriKind.Relative));
            IconWarning = new BitmapImage(new Uri("/UserInterface;component/Images/warning_sign.png", UriKind.Relative));
            IconOk = new BitmapImage(new Uri("/UserInterface;component/Images/oksign.png", UriKind.Relative));
        }

        #endregion

        #region Embedded classes

        public enum ValidateResult
        {
            Ok,
            Warning,
            Error
        }

        public class FunctionRowElement : INotifyPropertyChanged
        {

            private int _index;

            private AttributeInfo _attribute;

            private AttributeOperators _andOp;

            private GroupOperators _combineOp;

            private SelectOperators _selectOp;

            private bool _isLast;


            public FunctionRowElement(int index, AttributeInfo[] attribs)
            {
                this._index = index;
                this.Attributes = attribs;

                if (attribs.Length > 0)
                    _attribute = attribs[0];

                this.AndOps = (AttributeOperators[])Enum.GetValues(typeof(AttributeOperators));
                this.CombineOps = (GroupOperators[])Enum.GetValues(typeof(GroupOperators));
                this.SelectOps = (SelectOperators[])Enum.GetValues(typeof(SelectOperators));

                _andOp = AttributeOperators.Add;
                _combineOp = GroupOperators.Undefined;
                _selectOp = SelectOperators.Undefined;
            }


            public AttributeInfo[] Attributes { get; private set; }

            public AttributeOperators[] AndOps { get; private set; }

            public GroupOperators[] CombineOps { get; private set; }

            public SelectOperators[] SelectOps { get; private set; }


            public int Index
            {
                get { return _index; }
                set
                {
                    if (_index != value)
                    {
                        _index = value;
                        OnPropertyChanged(new PropertyChangedEventArgs("Index"));
                    }
                }
            }

            public AttributeInfo Attribute
            {
                get { return _attribute; }
                set
                {
                    if (_attribute != value)
                    {
                        _attribute = value;
                        OnPropertyChanged(new PropertyChangedEventArgs("Attribute"));
                    }
                }
            }

            public AttributeOperators AndOperator
            {
                get { return _andOp; }
                set
                {
                    if (_andOp != value)
                    {
                        _andOp = value;
                        OnPropertyChanged(new PropertyChangedEventArgs("AndOperator"));
                    }
                }
            }

            public GroupOperators CombineOperator
            {
                get { return _combineOp; }
                set
                {
                    if (_combineOp != value)
                    {
                        _combineOp = value;
                        OnPropertyChanged(new PropertyChangedEventArgs("CombineOperator"));
                    }
                }
            }

            public SelectOperators SelectOperator
            {
                get { return _selectOp; }
                set
                {
                    if (_selectOp != value)
                    {
                        _selectOp = value;
                        OnPropertyChanged(new PropertyChangedEventArgs("SelectOperator"));
                    }
                }
            }

            public bool IsLast
            {
                get { return _isLast; }
                set
                {
                    if (_isLast != value)
                    {
                        _isLast = value;
                        OnPropertyChanged(new PropertyChangedEventArgs("IsLast"));
                    }

                }
            }


            public event PropertyChangedEventHandler PropertyChanged;

            protected virtual void OnPropertyChanged(PropertyChangedEventArgs args)
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, args);
            }

        }

        public struct AttributeInfo
        {

            public string Name;

            public Types Type;


            public AttributeInfo(string name, Types type)
            {
                this.Name = name;
                this.Type = type;
            }

            public override string ToString()
            {
                return Name + " [" + Type + "]";
            }

            public static bool operator ==(AttributeInfo left, AttributeInfo right)
            {
                return (left.Name == right.Name && left.Type == right.Type);
            }

            public static bool operator !=(AttributeInfo left, AttributeInfo right)
            {
                return !(left == right);
            }

            public override bool Equals(object obj)
            {
                if (obj == null)
                    return false;
                if (!(obj is AttributeInfo))
                    return false;
                return (this == (AttributeInfo)obj);
            }

            public override int GetHashCode()
            {
                return Name.GetHashCode() ^ Type.GetHashCode();
            }

        }

        #endregion

        #region Fields

        private BitmapImage _messageIcon = null;

        private ValidateResult _validateResult = ValidateResult.Ok;

        private string _lastMessage = "";

        private string _functionName = "";

        #endregion

        #region PropertyChanged Events

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(PropertyChangedEventArgs args)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, args);
        }

        #endregion

        #region Properties

        public AttributeInfo[] Attributes { get; private set; }

        public ValidateResult Result
        {
            get { return _validateResult; }
            set
            {
                if (_validateResult != value)
                {
                    _validateResult = value;
                    switch (value)
                    {
                        case FunctionViewModel.ValidateResult.Error:
                            MessageIcon = IconError;
                            break;
                        case FunctionViewModel.ValidateResult.Warning:
                            MessageIcon = IconWarning;
                            break;
                        default:
                            MessageIcon = IconOk;
                            break;
                    }
                }
            }
        }

        public ObservableCollection<FunctionRowElement> Elements { get; private set; }

        public BitmapImage MessageIcon
        {
            get { return _messageIcon; }
            set
            {
                if (_messageIcon != value)
                {
                    _messageIcon = value;
                    OnPropertyChanged(new PropertyChangedEventArgs("MessageIcon"));
                }
            }
        }

        public string LastMessage
        {
            get { return _lastMessage; }
            set
            {
                if (_lastMessage != value)
                {
                    _lastMessage = (value == null) ? "" : value;
                    OnPropertyChanged(new PropertyChangedEventArgs("LastMessage"));
                }
            }
        }

        public string FunctionName
        {
            get { return _functionName; }
            set
            {
                if (_functionName != value)
                {
                    _functionName = (value == null) ? "" : value;
                    OnPropertyChanged(new PropertyChangedEventArgs("FunctionName"));
                }
            }
        }

        #endregion


        public FunctionViewModel(Tree tree)
        {
            // Get all attributes used by tree
            var attribs = new Dictionary<string, AttributeInfo>();
            foreach (var attrib in DefaultAttributes)
            {
                if (!attribs.ContainsKey(attrib.Name))
                    attribs.Add(attrib.Name, attrib);
            }
            foreach (var leaf in tree.Leafs)
            {
                foreach (var attrib in leaf.Attributes)
                {
                    if (!attribs.ContainsKey(attrib.Key))
                        attribs.Add(attrib.Key, new AttributeInfo(attrib.Key, attrib.Value.Type));
                }
            }
            this.Attributes = attribs.Values.ToArray();

            // Initialize elements list
            this.Elements = new ObservableCollection<FunctionRowElement>();
            this.Elements.Add(new FunctionRowElement(1, Attributes));
            UpdateOrder();

            this.MessageIcon = IconOk;
            this.CheckFunction();
        }


        #region Methods

        public void AddNewElement()
        {
            var element = new FunctionRowElement(Elements.Count + 1, Attributes);
            Elements.Add(element);
            UpdateOrder();
            OnPropertyChanged(new PropertyChangedEventArgs("Elements"));
            CheckFunction();
            element.PropertyChanged += (sender, args) => {
                CheckFunction();
            };
        }

        public void RemoveElement(int index)
        {
            Elements.RemoveAt(index);
            if (Elements.Count == 0)
                AddNewElement();
            UpdateOrder();
            OnPropertyChanged(new PropertyChangedEventArgs("Elements"));
            CheckFunction();
        }

        public bool IsLastElement(int index)
        {
            return (index == Elements.Count - 1);
        }

        public void MoveElementUp(int index)
        {
            if (index > 0)
            {
                var element = Elements[index];
                Elements.RemoveAt(index);
                Elements.Insert(index - 1, element);
                UpdateOrder();
                OnPropertyChanged(new PropertyChangedEventArgs("Elements"));
                CheckFunction();
            }
        }

        public void MoveElementDown(int index)
        {
            if (!IsLastElement(index))
            {
                var element = Elements[index];
                Elements.RemoveAt(index);
                Elements.Insert(index + 1, element);
                UpdateOrder();
                OnPropertyChanged(new PropertyChangedEventArgs("Elements"));
                CheckFunction();
            }
        }

        private void UpdateOrder()
        {
            for (var i = 0; i < Elements.Count; ++i)
            {
                Elements[i].Index = i + 1;
                Elements[i].IsLast = false;
            }
            if (Elements.Count > 0)
                Elements.Last().IsLast = true;
        }


        public ValidateResult CheckFunction()
        {
            // Clear previous result
            this.LastMessage = "";
            this.Result = ValidateResult.Ok;
            FunctionRowElement warningElement = null;

            // First check every element on it's own
            foreach (var element in Elements)
            {
                var result = CheckSingleElement(element);
                if (result.Item1 == ValidateResult.Error)
                {
                    // Bail out on first error
                    this.Result = result.Item1;
                    this.LastMessage = "[#" + element.Index + "] Error: " + result.Item2;
                    return this.Result;
                }
                else if (this.Result == ValidateResult.Ok &&
                          result.Item1 == ValidateResult.Warning)
                {
                    // Store first warning
                    this.Result = result.Item1;
                    this.LastMessage = "[#" + element.Index + "] Warning : " + result.Item2;
                    warningElement = element;
                }
            }

            // Bail out if state is on error
            if (Result == ValidateResult.Error)
                return Result;

            // Check element combinations
            // 1. Duplicate property names
            var nameCounts = new Dictionary<string, int>(Elements.Count);
            foreach (var element in Elements)
            {
                if (nameCounts.ContainsKey(element.Attribute.Name))
                {
                    this.Result = ValidateResult.Error;
                    this.LastMessage = "[#" + element.Index + "] Error: Duplicate attribute \"" + element.Attribute.Name + "\"! Can only use each attribute once per function.";
                    return ValidateResult.Error;
                }
                else
                {
                    nameCounts.Add(element.Attribute.Name, 1);
                }
            }

            return ValidateResult.Ok;
        }

        public Tuple<ValidateResult, string> CheckSingleElement(FunctionRowElement element)
        {
            // Type checking - errors first
            var elementType = element.Attribute.Type;
            if (elementType == Types.Boolean)
            {
                if (element.AndOperator != AttributeOperators.And &&
                    element.AndOperator != AttributeOperators.Or &&
                    element.AndOperator != AttributeOperators.XOr)
                {
                    return new Tuple<ValidateResult, string>(ValidateResult.Error, "Can't use AND-operator " + element.AndOperator + " with attribute of type " + elementType);
                }
                if (element.CombineOperator != GroupOperators.Undefined)
                {
                    return new Tuple<ValidateResult, string>(ValidateResult.Error, "Can't use combine operator " + element.CombineOperator + " with attribute of type " + elementType);
                }
                if (element.SelectOperator != SelectOperators.IsFalse &&
                    element.SelectOperator != SelectOperators.IsTrue &&
                    element.SelectOperator != SelectOperators.Undefined)
                {
                    return new Tuple<ValidateResult, string>(ValidateResult.Error, "Can't use select/filter operator " + element.SelectOperator + " with attribute of type " + elementType);
                }
            }
            else if (elementType == Types.Number)
            {
                if (element.AndOperator == AttributeOperators.And ||
                    element.AndOperator == AttributeOperators.Or ||
                    element.AndOperator == AttributeOperators.XOr)
                {
                    return new Tuple<ValidateResult, string>(ValidateResult.Error, "Can't use AND-operator " + element.AndOperator + " with attribute of type " + elementType);
                }
                if (element.SelectOperator == SelectOperators.IsFalse ||
                    element.SelectOperator == SelectOperators.IsTrue)
                {
                    return new Tuple<ValidateResult, string>(ValidateResult.Error, "Can't use combine operator " + element.SelectOperator + " with attribute of type " + elementType);
                }
            }

            // Type checking - warnings
            if (elementType == Types.Boolean)
            {
                if (element.SelectOperator == SelectOperators.Undefined)
                {
                    return new Tuple<ValidateResult, string>(ValidateResult.Warning, "Select/filter operator for boolean attribute is Undefined. This attribute will not take part in selecting the function result.");
                }
            }
            else if (elementType == Types.Number)
            {
                if (element.CombineOperator == GroupOperators.Undefined &&
                    element.SelectOperator == SelectOperators.Undefined)
                {
                    return new Tuple<ValidateResult, string>(ValidateResult.Warning, "Both combine and Select/filter operators are Undefined. This attribute will not take part in selecting the function result.");
                }
            }

            // No errors or warnings found
            return new Tuple<ValidateResult, string>(ValidateResult.Ok, "");
        }

        public Function CreateFunction(string name)
        {
            var function = new Function(name);
            foreach (var element in Elements)
            {
                function.Add(element.Attribute.Name, element.AndOperator, element.CombineOperator, element.SelectOperator);
            }
            return function;
        }

        #endregion

    }
}
