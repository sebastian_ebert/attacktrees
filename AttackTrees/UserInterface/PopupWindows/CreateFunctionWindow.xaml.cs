﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using UserInterface.ViewModels;
using System.ComponentModel;
using Model.Functions;
using System.Diagnostics;

namespace UserInterface.PopupWindows
{

    /// <summary>
    /// Interaktionslogik für CreateFunctionWindow.xaml
    /// </summary>
    public partial class CreateFunctionWindow : Window
    {
        private ModelView _modelView;

        private FunctionViewModel _funcViewModel;
                

        public CreateFunctionWindow(ModelView modelView)
        {
            this._modelView = modelView;
            this._funcViewModel = new FunctionViewModel(modelView.Tree);

            InitializeComponent();

            this.DataContext = _funcViewModel;
            this.FunctionDataGrid.DataContext = _funcViewModel.Elements;
        }

        private void HelpButton_OnClick(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show("Create new functions by adding steps to take per attribute and specify how to combine them." +
            //                "Order is important when results of multiple attributes are to be combined!", "Help", MessageBoxButton.OK,
            //                MessageBoxImage.Information);
            
            // ToDo: Create "real" help window here
            var message = new MessageWindow(
                WarningSigns.InformationSign,
                "Create new functions by adding steps to take per attribute and specify how to combine them."
                + "Order is important when results of multiple attributes are to be combined!",
                true,
                false);
            message.ShowDialog();
        }

        private void SaveButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (this._funcViewModel.Result == FunctionViewModel.ValidateResult.Error)
            {
                //MessageBox.Show("Can't store functions with errors!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                var message = new MessageWindow(
                    WarningSigns.ErrorSign, "Can't store functions with errors!", true, false);
                message.ShowDialog();

                return;
            }
            while (true)
            {
                var dialog = new SelectNameWindow();
                var result = dialog.ShowDialog();
                if (result != null && result == true)
                {
                    if (_modelView.Session.CurrentUser != null)
                    {
                        try
                        {
                            // Check if tree with the selected name already exists.
                            _modelView.Session.FuncRepository.FindByName(dialog.SelectedName);
                            var message = new MessageWindow(
                            WarningSigns.ErrorSign, "A function with name \"" + dialog.SelectedName + "\" already exists!", true, false);
                            message.ShowDialog();
                        }
                        catch (Exception)
                        {
                            this._funcViewModel.FunctionName = dialog.SelectedName;
                            this.DialogResult = true;
                            this.Close();
                            return;
                        }
                    }
                    else
                    {
                        this._funcViewModel.FunctionName = dialog.SelectedName;
                        this.DialogResult = true;
                        this.Close();
                        return;
                    }
                }
                else if (result == null || result == false)
                {
                    return;
                }
            }
        }

        private void CancelButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }



        private void SaveElementButton_Click(object sender, RoutedEventArgs e)
        {
            var element = (FunctionViewModel.FunctionRowElement)((Button)sender).DataContext;
            _funcViewModel.AddNewElement();
        }

        private void DeleteElementButton_Click(object sender, RoutedEventArgs e)
        {
            var element = (FunctionViewModel.FunctionRowElement)((Button)sender).DataContext;
            _funcViewModel.RemoveElement(element.Index - 1);
        }

        private void MoveUpElementButton_Click(object sender, RoutedEventArgs e)
        {
            var element = (FunctionViewModel.FunctionRowElement)((Button)sender).DataContext;
            _funcViewModel.MoveElementUp(element.Index - 1);
        }

        private void MoveDownElementButton_Click(object sender, RoutedEventArgs e)
        {
            var element = (FunctionViewModel.FunctionRowElement)((Button)sender).DataContext;
            _funcViewModel.MoveElementDown(element.Index - 1);
        }

    }

}
