﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UserInterface.PopupWindows
{

    public enum WarningSigns 
    {
        InformationSign,
        WarningSign,
        ErrorSign
    }
    
    
    /// <summary>
    /// Interaction logic for MessageWindow.xaml
    /// </summary>
    public partial class MessageWindow : Window
    {
        public string Message { set; get; }
        public string MessageHeaderText { set; get; }
        public SolidColorBrush MessageHeaderBackgroundBrush { set; get; }
        public SolidColorBrush LabelMessageHeaderForegroundBrush { set; get; }
        public bool ShowOkayButton { set; get; }
        public bool ShowCancelButton { set; get; }

        public MessageWindow(WarningSigns signEnum, string message, bool showOkayButton, bool showCancelButton) 
        {
            //Uri UriSource = null;
            //var img = new BitmapImage();


            this.Message = message;
            this.DataContext = this;
            this.ShowCancelButton = showCancelButton;
            this.ShowOkayButton = showOkayButton;

            switch (signEnum)
            {
                case WarningSigns.InformationSign:

                    this.MessageHeaderBackgroundBrush = Brushes.DodgerBlue;
                    this.MessageHeaderText = "INFORMATION";
                    this.LabelMessageHeaderForegroundBrush = Brushes.White;

                    break;
                case WarningSigns.WarningSign:

                    this.MessageHeaderBackgroundBrush = Brushes.Yellow;
                    this.MessageHeaderText = "WARNING";
                    this.LabelMessageHeaderForegroundBrush = Brushes.Black;

                    //img.BeginInit();
                    ////img.UriSource = new Uri("/UserInterface;component/Images/MainLogo512.png");
                    //img.EndInit();
                    //sign.Source = img;

                    break;
                case WarningSigns.ErrorSign:

                    this.MessageHeaderBackgroundBrush = Brushes.OrangeRed;
                    this.MessageHeaderText = "ERROR";
                    this.LabelMessageHeaderForegroundBrush = Brushes.White;
                    //img.BeginInit();
                    //img.UriSource = new Uri("/UserInterface;component/Images/MainLogo512.png");
                    //img.EndInit();
                    //sign.Source = img
                    break;
                default:
                    break;
            }
            InitializeComponent();
        }


        private void ButtonOkayOnClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }


        private void ButtonCancelOnClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }
    }
}
