﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using UserInterface.ViewModels;
using System.Windows.Controls.Primitives;
using Model.Trees;
using System.Collections.ObjectModel;

namespace UserInterface.PopupWindows
{
    /// <summary>
    /// Interaktionslogik für LoadTreeFromDatabaseWindow.xaml
    /// </summary>
    public partial class LoadTreeFromDatabaseWindow : Window
    {
        private ModelView modelView;

        private SideMenuControl sideMenu;

        private ObservableCollection<Tree> allTrees;

        public LoadTreeFromDatabaseWindow(ModelView modelView, SideMenuControl sideMenu)
        {
            this.modelView = modelView;
            this.sideMenu = sideMenu;
            //this.allTrees = (trees == null) ? this.modelView.Session.GetAllTrees() : trees;
            this.allTrees = new ObservableCollection<Tree>();
            InitializeComponent();
            this.LoadTreeGrid.DataContext = allTrees;
            TextBoxTreeName.Focus();
        }

        private void Load_Click(object sender, RoutedEventArgs e)
        {
            if (LoadTreeGrid.SelectedIndex >= 0 &&
                LoadTreeGrid.SelectedIndex < allTrees.Count)
            {
                Tree tempTree = allTrees.ElementAt(LoadTreeGrid.SelectedIndex);
                this.modelView.Tree = this.modelView.Session.GetById(tempTree.Id);
                this.modelView.NotifyTreeChanges();
                // update comboboxes in sidemenu 
                this.sideMenu.UpdateComboBoxValues();
                if (this.modelView.ColorTheme != null)
                {
                    this.modelView.ColorTheme.ClearColorDictionary();
                }

                this.Close();
            }
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            // just close
            this.Close();
        }

        private void ButtonSearch_Click(object sender, RoutedEventArgs e)
        {
            allTrees.Clear();

            if (string.IsNullOrWhiteSpace(TextBoxTreeName.Text))
            {
                var trees = this.modelView.Session.GetAllTrees();
                foreach (var tree in trees)
                    allTrees.Add(tree);
            }
            else
            {
                var query = TextBoxTreeName.Text;
                if (!query.StartsWith("%"))
                    query = "%" + query;
                if (!query.EndsWith("%"))
                    query = query + "%";

                var whatToSearch = ComboBoxDBSearch.SelectedIndex;
                List<Tree> resultTrees = null;

                switch (whatToSearch)
                {
                    case 0: // 'Find trees by name'
                        resultTrees = modelView.Session.TreeRepository.FindAllByName(query);
                        break;

                    case 1: // 'Find trees by attribute present'
                        resultTrees = modelView.Session.TreeRepository.FindAllByAttributeName(query);
                        break;

                    case 2: // 'Find trees by node name'
                        resultTrees = modelView.Session.TreeRepository.FindAllByNodeName(query);
                        break;
                }
                if (resultTrees != null)
                {
                    resultTrees = modelView.Session.GetViewableTrees(resultTrees);
                }

                if (resultTrees == null || resultTrees.Count == 0)
                {
                    //MessageBox.Show("No matching trees found!", "No match", MessageBoxButton.OK, MessageBoxImage.Information);

                    var message = new MessageWindow(
                        WarningSigns.InformationSign, "No matching trees found!", true, false);
                    message.ShowDialog();
                    return;
                }
                else
                {
                    foreach (var tree in resultTrees)
                        allTrees.Add(tree);
                }
            }
        }
    }
}
