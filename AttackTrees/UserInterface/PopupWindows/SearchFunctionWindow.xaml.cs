﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using UserInterface.ViewModels;
using Model;
using Model.Functions;

namespace UserInterface.PopupWindows
{
    /// <summary>
    /// Interaction logic for SearchFunctionWindow.xaml
    /// </summary>
    public partial class SearchFunctionWindow : Window
    {

        private UserSession session;
        private FunctionSearchViewModel viewModel;


        public SearchFunctionWindow(UserSession session)
        {
            this.session = session;
            this.viewModel = new FunctionSearchViewModel(session.CurrentTree, session.FuncRepository);

            this.DataContext = viewModel;

            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            TextBoxName.Focus();
        }

        private void ButtonAddTreeAttribs_Click(object sender, RoutedEventArgs e)
        {
            viewModel.AddTreeAttributes();
        }

        private void ButtonSearch_Click(object sender, RoutedEventArgs e)
        {
            if (!viewModel.CanSearch)
            {
                //MessageBox.Show("Please enter a name or add some attributes to search for.", "Info", MessageBoxButton.OK, MessageBoxImage.Information);

                var message = new MessageWindow(
                    WarningSigns.InformationSign,
                    "Please enter a name or add some attributes to search for.",
                    true,
                    false);
                message.ShowDialog();

                return;
            }

            if (!viewModel.Search())
            {
                //MessageBox.Show("Nothing found!", "No results", MessageBoxButton.OK, MessageBoxImage.Information);

                var message = new MessageWindow(WarningSigns.InformationSign, "Nothing found!", true, false);
                message.ShowDialog();


            }
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void ButtonOk_Click(object sender, RoutedEventArgs e)
        {
            if (!viewModel.HasResult)
            {
                MessageBox.Show("Nothing selected!", "No selection", MessageBoxButton.OK, MessageBoxImage.Warning);

                var message = new MessageWindow(WarningSigns.WarningSign, "Nothing selected!", true, false);
                message.ShowDialog();
                return;
            }
            this.DialogResult = true;
        }
    }
}
