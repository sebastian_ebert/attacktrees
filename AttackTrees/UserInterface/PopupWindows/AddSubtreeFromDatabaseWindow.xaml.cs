﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Model.Trees;


namespace UserInterface.PopupWindows
{
    using UserInterface.ViewModels;

    /// <summary>
    /// Interaktionslogik für AddSubtreeFromDatabaseWindow.xaml
    /// </summary>
    public partial class AddSubtreeFromDatabaseWindow : Window
    {
        private readonly ModelView modelView;

        private readonly ModelView newModelView;

        private readonly Node parentNode;

        public AddSubtreeFromDatabaseWindow(ModelView modelView, Node parentNode)
        {
            this.modelView = modelView;
            this.parentNode = parentNode;
            InitializeComponent();

            // initialize empty GraphSharp
            this.newModelView = new ModelView
            {
                Session = this.modelView.Session, 
            };

            this.GraphSharpControl.DataContext = newModelView;
            this.newModelView.NotifyTreeChanges();
            
            this.ComboBoxTreeItems.DataContext = this.modelView;
            //this.SelectedNode.DataContext = this.GraphSharpControl;
            //this.SelectedNode.Content = "select node ...";

            this.GraphSharpControl.ViewingMode = true;
        }

        private void CancelClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void AppendClick(object sender, RoutedEventArgs e)
        {
            if (this.GraphSharpControl.CurrentSelectedNode != null)
            {
                var nodeitem = this.newModelView.NodeNames.ElementAt(ComboBoxNodeItems.SelectedIndex);
                
                this.modelView.Tree.CopySubTree(this.newModelView.Tree, nodeitem.Node, this.parentNode);
                this.modelView.NotifyTreeChanges();

                if (this.modelView.Tree.VertexCount > 2000)
                {
                    var message = new MessageWindow(
                        WarningSigns.WarningSign, "You reached 2000+ nodes now", true, false);
                    message.ShowDialog();
                }

                if (this.modelView.Tree.VertexCount > 4000)
                {
                    var message = new MessageWindow(
                        WarningSigns.WarningSign, "You reached 4000+ nodes now. Application might be slow.", true, false);
                    message.ShowDialog();
                }

                this.Close(); 
            }
        }

        private void ComboBoxTreeItems_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.GridMessage.Background = Brushes.OrangeRed;
            this.LabelMessage.Content = "please wait ... loading tree data from database";
            
            // ToDo: Update UI element to see message, code below not working
            this.GridMessage.InvalidateVisual();
            this.LabelMessage.InvalidateVisual();

            var item = this.modelView.TreeNames.ElementAt(this.ComboBoxTreeItems.SelectedIndex);

            try
            {
                this.newModelView.Tree = this.modelView.Session.GetById(item.Tree.Id);
                this.newModelView.NotifyTreeChanges();

                this.GridMessage.Background = Brushes.DimGray;
                this.LabelMessage.Content = "loading successful";
            }
            catch (Exception)
            {
                this.GridMessage.Background = Brushes.DimGray;
                this.LabelMessage.Content = "loading failed";
            }

            this.ComboBoxNodeItems.ItemsSource = this.newModelView.NodeNames;
        }

        // ToDo: Find other solution - shouldnt need a manual update
        // Better solution: NotifyPropertyChanged in GraphSharpControl.CurrentSelectedNode 
        // and update on the selectedNode label
        //private void Update_OnClick(object sender, RoutedEventArgs e)
        //{
        //    this.SelectedNode.Content = this.GraphSharpControl.CurrentSelectedNode.Name;
        //}
    }
}
