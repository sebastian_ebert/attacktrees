﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using UserInterface.ViewModels;
using Model.Trees;

namespace UserInterface.PopupWindows
{
    /// <summary>
    /// Interaction logic for DeleteNode.xaml
    /// </summary>
    /// 
    
    public partial class DeleteNode : Window
    {
        private ModelView modelview;

        private readonly SideMenuControl sideMenu;


        public DeleteNode(ModelView modelview, SideMenuControl sideMenu)
        {

            this.modelview = modelview;
            this.sideMenu = sideMenu;

            InitializeComponent();

            this.DeleteNodeBox.DataContext = this.modelview;
        }


        private void ButtonDeleteNode_Click(object sender, RoutedEventArgs e)
        {

                      
            var deletenodeItem = this.modelview.NodeNames[DeleteNodeBox.SelectedIndex];

            var deletenode = deletenodeItem.Node;

            var rootnode = this.modelview.Tree.Vertices.First();

            if (rootnode != deletenode)
            {
                this.modelview.RemoveNode(deletenode);
            }
            else
            {
                // MessageBox.Show("Root node can't be deleted!", "Not able to delete", MessageBoxButton.OK, MessageBoxImage.Warning);

                var message = new MessageWindow(WarningSigns.WarningSign, "Root node can't be deleted!", true, false);
                message.ShowDialog();
            }

            this.sideMenu.UpdateComboBoxValues();

            this.Close();
      
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
