﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using UserInterface.ViewModels;
using System.Diagnostics;
using Model.Trees;

namespace UserInterface.PopupWindows
{

    /// <summary>
    /// Interaction logic for AddNewNodeWindow.xaml
    /// </summary>
    public partial class AddNewNodeWindow : Window
    {

        private ModelView modelview;

        private SideMenuControl sideMenu;

        public AddNewNodeWindow(ModelView modelview, SideMenuControl sideMenu)
        {
            this.modelview = modelview;
            this.sideMenu = sideMenu;

            InitializeComponent();

            this.comboBox1.DataContext = this.modelview;
        }

        private void ButtonAddNode_Click(object sender, RoutedEventArgs e)
        {

         var nodetext = RequiredNodeName.Text;

         var node = new Node(RequiredNodeName.Text);

         var nodeparentItem = this.modelview.NodeNames[comboBox1.SelectedIndex];

         var nodeparent = nodeparentItem.Node;
            
            //this.modelview.tree.AddNode{actualnodename, parentnode}

         if (!String.IsNullOrWhiteSpace(nodetext))
         {
             this.modelview.AddNode(node,nodeparent);

             this.sideMenu.UpdateComboBoxValues();

             if (this.modelview.Tree.VertexCount > 2000)
             {
                 var message = new MessageWindow(
                     WarningSigns.WarningSign, "You reached 2000+ nodes now", true, false);
                 message.ShowDialog();
             }

             if (this.modelview.Tree.VertexCount > 4000)
             {
                 var message = new MessageWindow(
                     WarningSigns.WarningSign, "You reached 4000+ nodes now. Application might be slow.", true, false);
                 message.ShowDialog();
             }


             this.Close();
         }

         else
         {
             //MessageBox.Show("You srewed up, try again without whitespaces!", "Screwer!", MessageBoxButton.OK,MessageBoxImage.Warning);
             var message = new MessageWindow(WarningSigns.WarningSign, "Please try again without whitespaces", true, false);
             message.ShowDialog();
         }

        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
