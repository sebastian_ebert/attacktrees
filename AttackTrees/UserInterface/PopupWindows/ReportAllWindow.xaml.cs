﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using UserInterface.ViewModels;
using System.Diagnostics;
using Model.Trees;

namespace UserInterface.PopupWindows
{
    /// <summary>
    /// Interaction logic for ReportAllWindow.xaml
    /// </summary>
    /// 
    

    public partial class ReportAllWindow : Window
    {
        private ModelView modelview;
        
        public ReportAllWindow(ModelView modelview)
        {

            this.modelview = modelview;
            InitializeComponent();
        }

        private void ButtonOK_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
