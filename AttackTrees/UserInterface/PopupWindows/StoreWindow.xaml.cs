﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UserInterface.PopupWindows
{
    /// <summary>
    /// Interaction logic for SaveWindow.xaml
    /// </summary>
    public partial class StoreWindow : Window
    {
        private ViewModels.ModelView modelView;
        private SideMenuControl sideMenu;

        public StoreWindow(ViewModels.ModelView modelView, SideMenuControl sideMenu)
        {
            this.modelView = modelView;
            this.sideMenu = sideMenu;
            InitializeComponent();
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void ButtonStore_Click(object sender, RoutedEventArgs e)
        {
            this.modelView.Session.CurrentTree.Name = NameBox.Text;
            this.modelView.Session.CurrentTree.Description = DescriptionBox.Text;

            this.modelView.Tree.Name = this.modelView.Session.CurrentTree.Name;
            this.modelView.Tree.Description = this.modelView.Session.CurrentTree.Description;

            bool success = this.modelView.Session.SaveOrUpdateTree(this.modelView.Session.CurrentTree);
            string userPermission = this.modelView.Session.CurrentUser.Rights[0];
            
            //Adding user rights to the tree
            if (!(this.modelView.Session.TreeRepository.HasPermission(this.modelView.Session.CurrentTree,userPermission))) 
            {
                this.modelView.Session.TreeRepository.AddPermission(this.modelView.Session.CurrentTree, userPermission);
            }
            if (success)
            {
                //MessageBox.Show("Tree Stored");

                var message = new MessageWindow(WarningSigns.InformationSign, "Tree stored", false, false);
                message.Show();
                message.ContentRendered += new EventHandler(TreeStoredMessage);


            }
            else
            {
                //MessageBox.Show("Tree could not be stored");

                var message = new MessageWindow(WarningSigns.InformationSign, "Tree could not be stored", true, false);
                message.ShowDialog();
            }
            this.Close();
        }

        private void TreeStoredMessage(object sender, EventArgs eventArgs)
        {
            var message = sender as MessageWindow;

            if (message != null)
            {
                System.Threading.Thread.Sleep(3000);
                message.Close();
            }
        }
    }
}
