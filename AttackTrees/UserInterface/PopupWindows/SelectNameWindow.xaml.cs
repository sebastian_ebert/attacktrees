﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;

namespace UserInterface.PopupWindows
{
    /// <summary>
    /// Interaction logic for SelectNameWindow.xaml
    /// </summary>
    public partial class SelectNameWindow : Window
    {

        #region Embedded classes - View model

        public class NameViewModel : INotifyPropertyChanged
        {

            private string _name;

            public string Name
            {
                get { return _name; }
                set
                {
                    if (_name != value)
                    {
                        _name = value;
                        OnPropertyChanged(new PropertyChangedEventArgs("Name"));
                        var isValidValue = !string.IsNullOrWhiteSpace(_name);
                        if (isValidValue != IsValid)
                        {
                            IsValid = isValidValue;
                            OnPropertyChanged(new PropertyChangedEventArgs("IsValid"));
                        }
                    }
                }
            }

            public bool IsValid { get; private set; }

            public NameViewModel()
            {
                this._name = "";
                this.IsValid = false;
            }

            public event PropertyChangedEventHandler PropertyChanged;

            protected virtual void OnPropertyChanged(PropertyChangedEventArgs args)
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, args);
            }
        }

        #endregion

        #region Fields

        private NameViewModel _viewModel;

        #endregion

        public string SelectedName { get { return _viewModel.Name; } }


        public SelectNameWindow()
        {
            this._viewModel = new NameViewModel();
            this.DataContext = _viewModel;

            InitializeComponent();

            this.DataContext = null;
            this.DataContext = _viewModel;
            this.TextBoxName.Focus();
        }

        private void ButtonOk_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

    }
}
