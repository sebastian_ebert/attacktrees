﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Windows.Media;

namespace UserInterface.Themes
{
    

    public class ColorTheme
    {

        #region Enumeration

        public enum ColorThemeType
        {
            LightTheme,
            DarkTheme,
            ColorExplosion
        }

        #endregion

        #region Members

        private ColorThemeType currentColorThemeType;

        private Dictionary<Guid, Brush> andEdgesColorDictionary;

        #endregion


        #region Properties
        
        public ColorThemeType CurrentColorThemeType
        {
            set
            {
                this.currentColorThemeType = value;
                this.SwitchColorTheme(value);
            }
            get
            {
                return currentColorThemeType;
            }
        }

        #endregion

        #region ColorProperties



        #endregion

        public ColorTheme(ColorThemeType themeType, Dictionary<Guid, Brush> andEdgesColorDictionary)
        {
            this.CurrentColorThemeType = themeType;
            this.andEdgesColorDictionary = andEdgesColorDictionary;
            this.SetUpAndEdgesColorDictionary();
        }

        private void SetUpAndEdgesColorDictionary()
        {
            
        }

        private void SetColorsLightTheme()
        {
            
        }

        private void SetColorsDarkTheme()
        {
            
        }

        private void SetColorsColorExplosion()
        {
            
        }

        public void SwitchColorTheme(ColorThemeType themeType)
        {
            switch (themeType)
            {
                case ColorThemeType.LightTheme:
                    this.SetColorsLightTheme();
                    break;
                case ColorThemeType.DarkTheme:
                    this.SetColorsDarkTheme();
                    break;
                case ColorThemeType.ColorExplosion:
                    this.SetColorsColorExplosion();
                    break;
                default:
                    this.SetColorsLightTheme();
                    break;
            }
        }

        public Brush FindBrushByGuid(Guid uniqueId)
        {
            Brush brush;
            
            if (andEdgesColorDictionary.TryGetValue(uniqueId, out brush))
            {
                return brush;        
            }
            else
            {
                this.SetAndEdgeColor(uniqueId);
                return andEdgesColorDictionary[uniqueId];  
            }
        }


        public void SetAndEdgeColor(Guid uniqueId)
        {
            var brushInfo = typeof(Brushes).GetProperties();
            var brushList = new Brush[brushInfo.Length];
            
            for (var i = 0; i < brushInfo.Length; i++)
            {
                brushList[i] = (Brush)brushInfo[i].GetValue(null, null);
            }

            var randomNumber = new Random(DateTime.Now.Millisecond);
            
            var brush = brushList[randomNumber.Next(1, brushList.Length)];

            andEdgesColorDictionary.Add(uniqueId, brush);

        }

        public static SolidColorBrush GetRandomBrush()
        {
            var randonGen = new Random();
            var tmpColor = System.Drawing.Color.FromArgb(randonGen.Next(255), randonGen.Next(255), randonGen.Next(255));
            var randomColor = Color.FromArgb(255, tmpColor.R, tmpColor.G, tmpColor.B);

            return new SolidColorBrush(randomColor);
        }

        public void ClearColorDictionary()
        {
            this.andEdgesColorDictionary.Clear();
        }
    }
}
