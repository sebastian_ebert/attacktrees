﻿using System.Windows;
using Persistence;
using System;
using UserInterface.ViewModels;
using UserInterface.PopupWindows;
using Model;

namespace UserInterface {


    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        #region Member

        /// <summary>
        /// Model View
        /// </summary>
        private ViewModels.ModelView modelView;
        private GraphSharpControl graphSharpControl;

        #endregion

        #region Constructor & Finalizers


        /// <summary>
        /// constructor with pre-defined control visibity
        /// </summary>
        /// <param name="type">
        /// Enumerations.UserInterfaceControlTypes.StubControl,
        /// Enumerations.UserInterfaceControlTypes.SideMenuControl,
        /// Enumerations.UserInterfaceControlTypes.RibbonControl:
        /// </param>
        public MainWindow(Enumerations.UserInterfaceControlTypes type)
        {
            // initialize user interface elements
            InitializeComponent();
            
            // sets visibilty for controls
            switch (type)
            {
                case Enumerations.UserInterfaceControlTypes.StubControl:
                    
                    break;
                case Enumerations.UserInterfaceControlTypes.SideMenuControl:
                    sideMenuControl.Visibility = Visibility.Visible;
                    break;
                case Enumerations.UserInterfaceControlTypes.RibbonControl:
                    
                    break;
                default:
                    break;
            }
        }


        #endregion

        #region Properties

        public ModelView ModelView 
        { 
            set
            {
                this.modelView = value;
                sideMenuControl.ModelView = value;
                sideMenuControl.GraphSharpControl.ModelView = value;
                sideMenuControl.UpdateComboBoxValues();
            } 
            get
            {
                return this.modelView;
            } 
        }
        #endregion


        #region Methods

        public void InitializeUserSession(UserSession session)
        {
        }
       

        #endregion

        private void ButtonExit_Click(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (this.ModelView.Tree.HasChanged)
            {
                var message = new MessageWindow(WarningSigns.WarningSign, "Your tree has unsaved changes, save tree?", true, true);
                message.ShowDialog();

                //MessageBoxResult dialogResult = MessageBox.Show("Your tree has unsaved changes, save tree?", "Exit ZenTree", MessageBoxButton.YesNo);
                if (message.DialogResult == true)
                {
                    var storewindow = new StoreWindow(this.sideMenuControl.ModelView, sideMenuControl);
                    storewindow.Show();
                }
                else if (message.DialogResult == false)
                {
                    Application.Current.Shutdown();
                }
            }
            else
            {
                Application.Current.Shutdown();
            }
        }

    }
}
