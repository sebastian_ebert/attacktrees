﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UserInterface
{
    public class Enumerations
    {
        /// <summary>
        /// Enumeration for user interface type definition 
        /// </summary>
        public enum UserInterfaceControlTypes
        {
            StubControl,
            SideMenuControl,
            RibbonControl
        }
    }
}
