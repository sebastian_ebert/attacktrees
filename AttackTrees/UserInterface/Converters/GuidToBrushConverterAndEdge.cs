﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows;
using System.Windows.Media;

namespace UserInterface.Converters
{

	class GuidToBrushConverterAndEdge : IValueConverter
	{

        private Dictionary<Guid, SolidColorBrush> brushes;
        private Random rng;

        public GuidToBrushConverterAndEdge()
        {
            rng = new Random(this.GetHashCode());
            brushes = new Dictionary<Guid, SolidColorBrush>();
            brushes[Guid.Empty] = new SolidColorBrush(Colors.DarkGray);
        }

		#region IValueConverter Members

		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			var id = (Guid) value;
            SolidColorBrush brush;
            if (!brushes.TryGetValue(id, out brush))
            {
                var r = rng.Next(256);
                var g = rng.Next(256);
                var b = rng.Next(256);
                brush = new SolidColorBrush(Color.FromRgb((byte)r, (byte)g, (byte)b));
                brushes.Add(id, brush);
            }
            return brush;
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			throw new System.NotImplementedException();
		}

		#endregion
	}
}