using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Model;
using Model.Trees;
using GraphSharp.Controls;
using UserInterface.ViewModels;
using System.Globalization;
using System.ComponentModel;
using UserInterface.PopupWindows;


namespace UserInterface
{
    using GraphSharp.Algorithms.Layout.Simple.Tree;

    /// <summary>
    /// Interaktionslogik f�r GraphSharpControl.xaml
    /// </summary>
    public partial class GraphSharpControl : UserControl
    {
        #region Members

        private ModelView modelView;

        private Node currentSelectedNode;

        private Node currentCopiedNode;

        private bool viewingMode;

        private bool activateNodesHighlighting = true;

        #endregion

        #region Constructors & Finalizers

        /// <summary>
        /// Constructor
        /// </summary>
        public GraphSharpControl()
        {
            InitializeComponent();


            var a = new SimpleTreeLayoutParameters();
            a.LayerGap = 30;
            a.VertexGap = 30;
            this.graphLayout.LayoutParameters = a;
            //this.graphLayout.OverlapRemovalConstraint = AlgorithmConstraints.Must;
            this.graphLayout.Relayout();

        }

       
        #endregion

        #region Properties

        public ModelView ModelView
        {
            set
            {
                this.modelView = value;
                if (this.modelView != null)
                {
                    try
                    {
                        this.DataContext = this.modelView;
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            get
            {
                return this.modelView;
            }
        }

        public SideMenuControl ParentSideMenu { private get; set; }

        public Node CurrentSelectedNode
        {
            get
            {
                return this.currentSelectedNode;
            }

            set
            {
                this.currentSelectedNode = value;
            }
        }

        public Node CurrentCopiedNode
        {
            get
            {
                return this.currentCopiedNode;
            }

            set
            {
                this.currentCopiedNode = value;
            }
        }

        public bool ViewingMode
        {
            set
            {
                this.viewingMode = value;
            }

            get
            {
                return viewingMode;
            }
        }

        public bool ActivateNodesHighlighting
        {
            set
            {
                this.activateNodesHighlighting = value;
            }

            get
            {
                return this.activateNodesHighlighting;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// sets up sample tree data for the stub
        /// </summary>
        /// 
       
        public void SetupTreeSampleData()
        {
            try
            {
                this.DataContext = modelView;
            }
            catch (Exception)
            {
                throw;
            }

            //var tree = new Tree();

            //var root = new Node("Open Safe");

            //var node2 = new Node("Pick Lock (I)");
            //node2.AddAttribute("Impossible", true);
            //node2.AddAttribute("Possible", false);

            //var node3 = new Node("Learn Combo");

            //var node4 = new Node("Cut Open Safe (P)");
            //node4.AddAttribute("Possible", true);

            //var node5 = new Node("Install Improperly (I)");
            //node5.AddAttribute("Impossible", true);

            //var node6 = new Node("Find Written Combo (I)");
            //node6.AddAttribute("Impossible", true);

            //var node7 = new Node("Get Combo From Target");

            //var node8 = new Node("Threaten (I)");
            //node8.AddAttribute("Impossible", true);

            //var node9 = new Node("Blackmail (I)");
            //node9.AddAttribute("Impossible", true);

            //var node10 = new Node("Eavesdrop");

            //var node11 = new Node("Bribe (P)");
            //node11.AddAttribute("Possible", true);

            //var node12 = new Node("Listen to Conversation (P)");
            //node12.AddAttribute("Possible", true);

            //var node13 = new Node("Get Target to State Combo");

            //tree.AddNode(root, null);
            //tree.AddNode(node2, root);
            //tree.AddNode(node3, root);
            //tree.AddNode(node4, root);
            //tree.AddNode(node5, root);
            //tree.AddNode(node6, node3);
            //tree.AddNode(node7, node3);
            //tree.AddNode(node8, node7);
            //tree.AddNode(node9, node7);
            //tree.AddNode(node10, node7);
            //tree.AddNode(node11, node7);
            //tree.AddNode(node12, node10);
            //tree.AddNode(node13, node10);

            //modelView.Tree = tree;

            //modelView.Tree = Samples.SampleTree1.TreeSample();
            modelView.Tree = Samples.SampleTree6.TreeSample();
        }

        #endregion


        private void OnDataPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "Tree":
                    this.modelView.Tree = this.modelView.Tree;
                    break;
                    
            }

        }

        private void SubNodeExpanderOnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!this.ViewingMode)
            {
                var textblock = sender as TextBlock;

                if (textblock != null && textblock.Text.Any())
                {
                    var node = textblock.DataContext as Node;

                    if (node != null)
                    {
                        var childNodes = this.modelView.Tree.FindChildren(node);

                        var enumerable = childNodes as Node[] ?? childNodes.ToArray();

                        if (enumerable.Any())
                        {
                            var visibility = !enumerable.ElementAt(0).IsExpanded;

                            foreach (var childnode in enumerable)
                            {
                                ExpandOrCollapseSubNodes(visibility, childnode);
                            }
                        }
                    }
                    // �ber alle nodes und verstecken

                    textblock.Text = textblock.Text.Equals("+") ? "-" : "+";

                    this.ModelView.NotifyTreeChanges();
                }
            }
        }

        private void AttributtesExpanderOnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!ViewingMode)
            {
                var textblock = sender as TextBlock;

                if (textblock != null && textblock.Text.Any())
                {
                    var node = textblock.DataContext as Node;

                    if (node != null)
                    {
                        node.ShowsAttributes = !node.ShowsAttributes;
                    }

                    textblock.Text = textblock.Text.Equals("+") ? "-" : "+";

                    this.ModelView.NotifyTreeChanges();

                }
            }
        }


        private void ExpandOrCollapseSubNodes(bool visibility, Node currentRoot)
        {
            currentRoot.IsExpanded = visibility;

            if (this.ModelView.Tree.FindChildren(currentRoot).Any())
            {
                foreach (var childnode in this.ModelView.Tree.FindChildren(currentRoot))
                {
                    this.ExpandOrCollapseSubNodes(visibility, childnode);
                }
            }
        }



        private void TextBoxBase_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (!this.ViewingMode)
            {
                var element = sender as TextBox;

                if (element != null && element.Text.Any())
                {
                    var node = element.DataContext as Node;

                    if (node != null)
                    {
                        node.Name = element.Text;
                        this.ParentSideMenu.UpdateComboBoxValues();
                    }
                } 
            }
            else
            {
                var element = sender as TextBox;

                if (element != null && element.Text.Any())
                {
                    var node = element.DataContext as Node;

                    if (node != null)
                    {
                        element.Text = node.Name;
                    }
                } 
            }
        }


        private void AddSubnode_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!this.ViewingMode)
            {
                var grid = sender as Grid;

                if (grid != null)
                {
                    var node = grid.DataContext as Node;

                    if (node != null)
                    {
                        var subNode = new Node("Change name ...");
                        this.modelView.AddNode(subNode, node);
                        this.ParentSideMenu.UpdateComboBoxValues();

                        if (this.modelView.Tree.VertexCount > 2000)
                        {
                            var message = new MessageWindow(
                                WarningSigns.WarningSign, "You reached 2000+ nodes now", true, false);
                            message.ShowDialog();
                        }

                        if (this.modelView.Tree.VertexCount > 4000)
                        {
                            var message = new MessageWindow(
                                WarningSigns.WarningSign, "You reached 4000+ nodes now. Application might be slow.", true, false);
                            message.ShowDialog();
                        }
                    }
                } 
            }
        }


        private void NodeAttributeValueOnKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                var element = sender as TextBox;

                if (element != null && element.Text.Any())
                {
                    var node = element.DataContext as NodeAttribute;

                    if (node != null)
                    {
                        this.UpdateNodeAttributeValue(node, element.Text);
                        
                    }
                }
            }
        }

        private void NodeAttributeValueOnLostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            var element = sender as TextBox;

            if (element != null && element.Text.Any())
            {
                var node = element.DataContext as NodeAttribute;

                if (node != null)
                {
                    this.UpdateNodeAttributeValue(node, element.Text);
                    
                }
            }
        }

        private void UpdateNodeAttributeValue(NodeAttribute node, string value)
        {
            if (!this.ViewingMode)
            {
                if (node.Type == Model.Functions.Types.Number)
                {
                    node.SetValue(decimal.Parse(value));
                }
                else
                {
                    node.SetValue(bool.Parse(value));
                }
                this.modelView.NotifyTreeChanges();
            }
        }


        private void Relayout()
        {
            this.ZoomControl.Width = this.Width;
            this.ZoomControl.Height = this.Height;
        }

        private void FrameworkElement_OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            this.ZoomControl.Width = this.Width;
            this.ZoomControl.Height = this.Height;
            
        }

        private void UIElement_OnMouseEnter(object sender, MouseEventArgs e)
        {
            var element = sender as Border;

            if (element != null)
            {
                var node = element.DataContext as Node;

                this.currentSelectedNode = node;

                if (node != null)
                {
                    if (activateNodesHighlighting) HighlightNodesOnPath(node, true);
                }
            }
        }

        private void HighlightNodesOnPath(Node node, bool isSelected)
        {
            node.IsSelected = isSelected;

            if (this.ModelView != null && node != this.ModelView.Tree.RootNode)
            {
                this.HighlightNodesOnPath(this.modelView.Tree.FindParent(node), isSelected);
            }
        }

        private void UIElement_OnMouseLeave(object sender, MouseEventArgs e)
        {
            var element = sender as Border;

            if (element != null)
            {
                var node = element.DataContext as Node;

                if (node != null)
                {
                    HighlightNodesOnPath(node, false);
                }
            }
        }

        private void MenuItemAddSubtreeFromDatabaseClick(object sender, RoutedEventArgs e)
        {
            if (!this.ViewingMode)
            {
                var grid = sender as MenuItem;

                if (grid != null)
                {
                    var node = grid.DataContext as Node;

                    if (node != null)
                    {
                        var window = new AddSubtreeFromDatabaseWindow(this.modelView, node);
                        window.ShowDialog();

                        this.ZoomControl.ZoomToFill();
                    }
                } 
            }
        }

        private void MenuItemCopyNodeClick(object sender, RoutedEventArgs e)
        {
            if (!this.ViewingMode)
            {
                this.CurrentCopiedNode = this.CurrentSelectedNode;
            }
        }

        private void MenuItemAppendNodeClick(object sender, RoutedEventArgs e)
        {
            if (!this.ViewingMode)
            {
                if (this.CurrentCopiedNode != null)
                {
                    var copy = modelView.Tree.CopySubTreeAsNewTree(this.CurrentCopiedNode);

                    this.modelView.Tree.CopySubTree(
                        copy, copy.RootNode, this.CurrentSelectedNode);

                    if (this.modelView.Tree.VertexCount > 2000)
                    {
                        var message = new MessageWindow(
                            WarningSigns.WarningSign, "You reached 2000+ nodes now", true, false);
                        message.ShowDialog();
                    }

                    if (this.modelView.Tree.VertexCount > 4000)
                    {
                        var message = new MessageWindow(
                            WarningSigns.WarningSign, "You reached 4000+ nodes now. Application might be slow.", true, false);
                        message.ShowDialog();
                    }

                    this.modelView.Tree.ClearResults();
                    this.modelView.NotifyTreeChanges();
                    this.ParentSideMenu.UpdateComboBoxValues();
                }
            }
        }

        private void MenuItemDeleteNodeClick(object sender, RoutedEventArgs e)
        {
            if (!this.ViewingMode)
            {
                if (this.CurrentSelectedNode != this.modelView.Tree.RootNode)
                {
                    this.modelView.RemoveNode(this.CurrentSelectedNode);
                    this.ParentSideMenu.UpdateComboBoxValues();
                }
                else
                {
                    var message = new MessageWindow(WarningSigns.WarningSign, "Root node cannot be deleted", true, false);
                    message.Show();
                }
            }
        }

        private void TextBoxNodeHeaderOnLostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            if (!this.ViewingMode)
            {
                var element = sender as TextBox;

                if (element != null && element.Text.Any())
                {
                    var node = element.DataContext as Node;

                    if (node != null)
                    {
                        node.Name = element.Text;
                        this.ParentSideMenu.UpdateComboBoxValues();
                    }
                }
            }
            else
            {
                var element = sender as TextBox;

                if (element != null && element.Text.Any())
                {
                    var node = element.DataContext as Node;

                    if (node != null)
                    {
                        element.Text = node.Name;

                    }
                }
            }
        }
    }
}
