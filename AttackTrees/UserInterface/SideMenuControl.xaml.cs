using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;
using Microsoft.Win32;
using UserInterface.PopupWindows;
using System.ComponentModel;
using UserInterface.ViewModels;
using System.Diagnostics;
using Model.Trees;
using Model.Functions.Evaluation.Local;
using Model.Functions;
using System.Text.RegularExpressions;
using UserInterface.Themes;

namespace UserInterface
{
    using System.Drawing;

    /// <summary>
	/// Interaktionslogik f�r SideMenuControl.xaml
	/// </summary>
	public partial class SideMenuControl : UserControl
	{

		#region Members

		private ModelView viewModel;
   
		private bool menuVisible = false;

		#endregion

		#region Constructors & Finalizers


		/// <summary>
		/// Standard constructor
		/// </summary>
		public SideMenuControl()
		{
			
			InitializeComponent();

			this.SelectNode1.DataContext = this.viewModel;
			this.SelectAndNode1.DataContext = this.viewModel;
			this.SelectAndNode2.DataContext = this.viewModel;
			this.SelectOrNode.DataContext = this.viewModel;
			this.GraphSharpControl.ParentSideMenu = this;
			this.GraphSharpControl.ViewingMode = false;

		    this.ComboBoxExpandNodesLowerLevel.DataContext = this.viewModel;
		    this.ComboBoxCollapeNodesLowerLevel.DataContext = this.viewModel;
		}




		#endregion

		#region Properties

		/// <summary>
		/// ModelView - contains Tree and UserSession
		/// </summary>

		public ModelView ModelView
		{
			set
			{
				this.viewModel = value;

				if (this.viewModel != null)
				{
					this.TextBoxFunctionName.Text = this.viewModel.FunctionName;
					this.ButtonRunFunction.IsEnabled = this.viewModel.HasFunction;
				}
			}
			get
			{
				return this.viewModel;
			}
		}

		#endregion

		#region Methods

		
		public void UpdateComboBoxValues()
		{
			this.SelectNode1.DataContext = null;
			this.SelectAndNode1.DataContext = null;
			this.SelectAndNode2.DataContext = null;
			this.SelectOrNode.DataContext = null;
			this.SelectNode1.DataContext = this.viewModel;
			this.SelectAndNode1.DataContext = this.viewModel;
			this.SelectAndNode2.DataContext = this.viewModel;
			this.SelectOrNode.DataContext = this.viewModel;

            // Expand and Collapse Comboboxes
		    this.ComboBoxExpandNodesLowerLevel.DataContext = null;
            this.ComboBoxExpandNodesLowerLevel.DataContext = this.viewModel;
		    this.ComboBoxCollapeNodesLowerLevel.DataContext = null;
		    this.ComboBoxCollapeNodesLowerLevel.DataContext = this.viewModel;
            
			

            this.AlgorithmType.DataContext = this.viewModel;

			if (!this.SelectNode1.Items.IsEmpty)
			{
				this.SelectNode1.SelectedItem = this.SelectNode1.Items[0];
				this.SelectAndNode1.SelectedItem = this.SelectAndNode1.Items[0];
				this.SelectAndNode2.SelectedItem = this.SelectAndNode2.Items[0];
				this.SelectOrNode.SelectedItem = this.SelectOrNode.Items[0];
				this.AttributeAddName.Text = "Enter name....";
				this.AttributeAddDefaultType.SelectedIndex = 0;
				this.AttributeAddDefaultNumber.Text = "<Default value>";
				this.AttributeEditName.DataContext = this.ModelView.Tree.GetNodeAttributesAsList();
				this.AttributeEditName.SelectedIndex = -1;
				this.AttributeEditType.SelectedIndex = 0;
				this.AttributeEditDefaultNumber.Text = "<Default value>";
				this.AttributeRemove.DataContext = this.ModelView.Tree.GetNodeAttributesAsList();
				this.AttributeRemove.SelectedIndex = -1;

			}

			this.ComboDefaultFunctions.DataContext = null;
			this.ComboDefaultFunctions.DataContext = this.viewModel;

		}
		
		public void SetGraphSharpControl(GraphSharpControl control)
		{
			this.GraphSharpControl = control;
		}

	   
		private void Expander_Expanded(object sender, RoutedEventArgs e)
		{

		}


		private void ButtonStore_Click(object sender, RoutedEventArgs e)
		{
			if (this.ModelView.Session.TreeExists(this.ModelView.Tree.Id))
			{
				bool success = this.ModelView.Session.SaveOrUpdateTree(this.ModelView.Tree);
				if (success)
				{
                    //MessageBox.Show("Tree Updated");
				    var message = new MessageWindow(WarningSigns.InformationSign, "Tree updated", true, false);
				    message.ShowDialog();
				}
				else
				{
                    //MessageBox.Show("Tree could not be updated");
                    var message = new MessageWindow(WarningSigns.InformationSign, "Tree could not be updated", true, false);
                    message.ShowDialog();
				}
			}
			else {
				StoreWindow storewindow = new StoreWindow(this.viewModel, this);
				storewindow.Show();
			}
		}

		private void ButtonSaveAs_Click(object sender, RoutedEventArgs e)
		{
			var saveasDialog = new SaveFileDialog();
			saveasDialog.ShowDialog();
		}

		private void ButtonOpenFromFile_Click(object sender, RoutedEventArgs e)
		{
            var messagebox = new MessageWindow(WarningSigns.InformationSign, "Connecting database \nplease wait ...", false, false);
            messagebox.Show();
            messagebox.ContentRendered += new EventHandler(messagebox_ContentRendered);
        }

        void messagebox_ContentRendered(object sender, EventArgs e)
        {
            var loadTreeFromDatabaseWindow = new LoadTreeFromDatabaseWindow(this.ModelView, this);

            var messageBox = sender as MessageWindow;

            loadTreeFromDatabaseWindow.Show();
            messageBox.Close();
        }

		private void ButtonAbout_Click(object sender, RoutedEventArgs e)
		{

			AboutBox1 box = new AboutBox1();
			box.ShowDialog();

		}

		private void ButtonRecent_Click(object sender, RoutedEventArgs e)
		{

			//if (this.recentfilesvisible)
			//{
			//    RecentFiles.Visibility = System.Windows.Visibility.Collapsed;
			//    this.recentfilesvisible = false;
			//}

			//else
			//{
			//    RecentFiles.Visibility = System.Windows.Visibility.Visible;
			//    this.recentfilesvisible = true;
			//}


		}

		private void ButtonHelp_Click_1(object sender, RoutedEventArgs e)
		{
			var helpwindow = new HelpWindow();
			helpwindow.Show();
		}

		private void ButtonSettings_Click(object sender, RoutedEventArgs e)
		{
		    var settingsWindows = new SettingsWindow();
            settingsWindows.Show();
		}

		private void ButtonExit_Click(object sender, RoutedEventArgs e)
		{
			if (this.ModelView.Tree.HasChanged)
			{
			    var message = new MessageWindow(WarningSigns.WarningSign, "Your tree has unsaved changes, save tree?", true, true);
			    message.ShowDialog();

                //MessageBoxResult dialogResult = MessageBox.Show("Your tree has unsaved changes, save tree?", "Exit ZenTree", MessageBoxButton.YesNo);
				if (message.DialogResult == true)
				{
					var storewindow = new StoreWindow(this.viewModel, this);
					storewindow.Show();
				}
                else if (message.DialogResult == false)
				{
					Application.Current.Shutdown();
				}
			}
			else {
				Application.Current.Shutdown();
			}
			
		}

	
		private void ButtonFitToSrceen_Click(object sender, RoutedEventArgs e)
		{
            this.GraphSharpControl.ZoomControl.ZoomToFill();
		}

		private void ButtonExpandAll_Click(object sender, RoutedEventArgs e)
		{
            // if it means expand all subnodes use this
            //this.ModelView.Tree.ExpandLevel(1);
            //this.ModelView.NotifyTreeChanges();
            //this.GraphSharpControl.ZoomControl.ZoomToFill();

            // if it means expand all attributes use this
		    foreach (var node in this.ModelView.Tree.Vertices)
		    {
		        node.ShowsAttributes = true;
                this.ModelView.NotifyTreeChanges();
		    }
		}

		private void ButtonHighlight_Click(object sender, RoutedEventArgs e)
		{
		    this.GraphSharpControl.ActivateNodesHighlighting = !this.GraphSharpControl.ActivateNodesHighlighting;
		}

		private void ButtonAddNode_Click(object sender, RoutedEventArgs e)
		{
			var ann = new AddNewNodeWindow(this.ModelView, this);
			ann.ShowDialog();
			this.DataContext = ann.DataContext;
		}

		private void ButtonRemoveNode_Click(object sender, RoutedEventArgs e)
		{
			var dn = new DeleteNode(this.ModelView, this);
			dn.ShowDialog();
			this.DataContext = dn.DataContext;
		}

		private void ButtonAddAttribute_Click(object sender, RoutedEventArgs e)
		{
			NodeAttribute nodeAttribute = null;
			bool ready = true;
			//getting the values
			string newName = null;
			Types newType = (Types) Enum.Parse(typeof(Types), AttributeAddType.Text, true);
			
			//Check the name of the attribute
			if (AttributeAddName.Text.Equals("") || AttributeAddName.Text.Equals("Enter name...."))
			{
				ready = false;
				//MessageBox.Show("Please fill the attribute's name.", "Add Attribute");

			    var message = new MessageWindow(WarningSigns.WarningSign, "Please fill the attribute's name.", false, false);
			    message.Show();
                message.ContentRendered += new EventHandler(this.MessageWindowShowTimeEvent);
                return;
			}
			else 
			{
				newName = AttributeAddName.Text;
			}

            if (ModelView.Tree.DefaultAttributes.ContainsKey(newName))
            {
                var message = new MessageWindow(WarningSigns.WarningSign, "Can't add attribute \"" + newName + "\" because an attribute with that name already exists.", false, false);
                message.Show();
                message.ContentRendered += new EventHandler(this.MessageWindowShowTimeEvent);
                return;
            }

			switch (newType)
			{
				case Types.Boolean:
					//get the value
					nodeAttribute = new NodeAttribute(newName,Types.Boolean, AttributeAddDefaultType.Text);
					break;
				case Types.Number:
					//get the value
					nodeAttribute = new NodeAttribute(newName, Types.Number, AttributeAddDefaultNumber.Text);
					break;
				default:
					break;
			}

			 if (ready) 
			 {
				 ModelView.Tree.AddDefaultAttribute(nodeAttribute);
			 }
			 this.ModelView.NotifyTreeChanges();
			 UpdateComboBoxValues();
			
		}

        private void ButtonEditAttribute_Click(object sender, RoutedEventArgs e)
		{
			//Get the different Attribute Names
			if (AttributeEditType.SelectedIndex == 0)
			{
				//MessageBox.Show("Please select a type for the attribute.", "Edit Attribute");

                var message = new MessageWindow(WarningSigns.WarningSign, "Please select a type for the attribute.", false, false);
                message.Show();
                message.ContentRendered += new EventHandler(this.MessageWindowShowTimeEvent);
			}
			else
			{
				List<string> attributesList = this.ModelView.Tree.GetNodeAttributesAsList();
				int index = this.AttributeEditName.SelectedIndex;
				string attrKey = attributesList.ElementAt(index);
				Types newType = (Types)Enum.Parse(typeof(Types), AttributeEditType.Text, true);
				
				string defaultValue = null;
				switch (newType) 
				{
					case Types.Boolean:
						defaultValue = AttributeEditDefaultType.Text;
						break;
					case Types.Number:
						defaultValue = AttributeEditDefaultNumber.Text;
						break;
					default:
						break;
				}
				NodeAttribute editAttribute = new NodeAttribute(AttributeEditName.Text, newType, defaultValue);

				foreach (Node node in this.ModelView.Tree.Leafs)
				{
					if (node.Attributes.ContainsKey(attrKey))
					{
						node.Attributes.Remove(attrKey);
						node.Attributes.Add(editAttribute.Name, editAttribute);
					}

				}
			}
			this.ModelView.NotifyTreeChanges();
			UpdateComboBoxValues();
		}

        private void ButtonRemoveAttribute_Click(object sender, RoutedEventArgs e)
		{
			string attrName = (string) AttributeRemove.SelectedItem;
			this.ModelView.Tree.RemoveDefaultAttribute(attrName);
			this.ModelView.NotifyTreeChanges();
			UpdateComboBoxValues();
		}

		private void ButtonCreateAndEdge_Click(object sender, RoutedEventArgs e)
		{
			NodeItem node1 = this.ModelView.NodeNames.ElementAt(SelectAndNode1.SelectedIndex);
			NodeItem node2 = this.ModelView.NodeNames.ElementAt(SelectAndNode2.SelectedIndex);
		  
			Node parent1 = this.ModelView.Tree.FindParent(node1.Node);
			Node parent2 = this.ModelView.Tree.FindParent(node2.Node);
		   
			//check if both nodes have the same parent
			//Same parent
			if (parent1 != null && parent2 != null) 
			{
				if (parent1.Id.Equals(parent2.Id))
				{
					//Check if nodes already have an AND Group
					IEnumerable<Edge> edgesNode1 = this.ModelView.Tree.InEdges(node1.Node);
					IEnumerable<Edge> edgesNode2 = this.ModelView.Tree.InEdges(node2.Node);
					bool isAndNode1 = edgesNode1.ElementAt(0).IsAnd;
					bool isAndNode2 = edgesNode2.ElementAt(0).IsAnd;
                    Guid andGroup1 = edgesNode1.ElementAt(0).AndGroup;
                    Guid andGroup2 = edgesNode2.ElementAt(0).AndGroup;
					
					//if both are AND groups
					if (isAndNode1 && isAndNode2) 
					{
					  // Check if nodes are already connected
                        if (andGroup1 == andGroup2)
                        {
                            var message1 = new MessageWindow(WarningSigns.InformationSign, "The Selected nodes are already AND connected.", false, false);
                            message1.Show();
                            message1.ContentRendered += new EventHandler(this.MessageWindowShowTimeEvent);
                        }
                        // Else ask user what to do
                        else
                        {
                            var result = MessageBox.Show("Join AND groups?", "AND groups", MessageBoxButton.YesNo, MessageBoxImage.Information);
                            if (result == MessageBoxResult.Yes)
                            {
                                foreach (var node in viewModel.Tree.FindAndGroupNodes(andGroup2))
                                    viewModel.Tree.InEdges(node).First().AndGroup = andGroup1;
                            }
                            else
                            {
                                var newGroup = viewModel.Tree.GetNextAndGroup();
                                edgesNode1.First().AndGroup = newGroup;
                                edgesNode2.First().AndGroup = newGroup;

                                // Check if any old group is empty now
                                var group1 = viewModel.Tree.FindAndGroupNodes(andGroup1);
                                var group2 = viewModel.Tree.FindAndGroupNodes(andGroup2);
                                if (group1.Count() <= 1)
                                {
                                    foreach (var node in group1)
                                        viewModel.Tree.InEdges(node).First().AndGroup = Guid.Empty;
                                }
                                if (group1.Count() <= 2)
                                {
                                    foreach (var node in group2)
                                        viewModel.Tree.InEdges(node).First().AndGroup = Guid.Empty;
                                }
                            }
                        }
					}
					//if first is in AND group
					else if (isAndNode1)
					{
					  //Group the non AND to the first AND gruop
						edgesNode2.ElementAt(0).AndGroup = edgesNode1.ElementAt(0).AndGroup;
						//MessageBox.Show("The Selected nodes are now AND connected.", "AND Group");

                        //var message = new MessageWindow(WarningSigns.WarningSign, "The Selected nodes are now AND connected.", false, false);
                        //message.Show();
                        //message.ContentRendered += new EventHandler(this.MessageWindowShowTimeEvent);
					}
					//if second is in AND group
					else if (isAndNode2)
					{
					  //Group the non AND to the second AND gruop
						edgesNode1.ElementAt(0).AndGroup = edgesNode2.ElementAt(0).AndGroup;
						//MessageBox.Show("The Selected nodes are now AND connected.", "AND Group");

                        //var message = new MessageWindow(WarningSigns.WarningSign, "The Selected nodes are now AND connected.", false, false);
                        //message.Show();
                        //message.ContentRendered += new EventHandler(this.MessageWindowShowTimeEvent);
					}
					//if none is in AND group
					else
					{ 
						//Create a new GUID for the AND group
						Guid groupGuid = ModelView.Tree.GetNextAndGroup();

                        //this.ModelView.ColorTheme.SetAndEdgeColor(groupGuid);
                        //var edgeColor = ColorTheme.GetRandomBrush();

					    //var edgeColor = new SolidColorBrush(Colors.DodgerBlue);

						edgesNode1.ElementAt(0).AndGroup = groupGuid;
						edgesNode2.ElementAt(0).AndGroup = groupGuid;
						//MessageBox.Show("The Selected nodes are now AND connected.", "AND Group");

                        //var message = new MessageWindow(WarningSigns.WarningSign, "The Selected nodes are now AND connected.", false, false);
                        //message.Show();
                        //message.ContentRendered += new EventHandler(this.MessageWindowShowTimeEvent);
					}
				}
				//Trigger error box 
				else
				{
					//MessageBox.Show("The Selected nodes doesn't have the same parent node.", "Error");

                    var message = new MessageWindow(WarningSigns.ErrorSign, "The selected nodes doesn't have the same parent node.", false, false);
                    message.Show();
                    message.ContentRendered += new EventHandler(this.MessageWindowShowTimeEvent);
				}
			}
			else
			{
				//MessageBox.Show("Could not connect the nodes as an AND group.", "Error");

                var message = new MessageWindow(WarningSigns.ErrorSign, "Could not connect the nodes as an AND group.", false, false);
                message.Show();
                message.ContentRendered += new EventHandler(this.MessageWindowShowTimeEvent);
			}
			this.ModelView.NotifyTreeChanges();
			UpdateComboBoxValues();
		}



        private void MessageWindowShowTimeEvent(object sender, EventArgs eventArgs)
        {
            var message = sender as MessageWindow;

            if (message != null)
            {
                System.Threading.Thread.Sleep(3000);
                message.Close();
            }
        }

        //private void ButtonReportAll_Click(object sender, RoutedEventArgs e)
        //{

        //    var reportall = new ReportAllWindow(this.ModelView);
        //    reportall.ShowDialog();

        //}

		private void ButtonCreateFunction_Click(object sender, RoutedEventArgs e)
		{
			var cfw = new CreateFunctionWindow(this.ModelView);
			var result = cfw.ShowDialog();
			if (result != null && result == true)
			{
				var dataContext = cfw.DataContext as FunctionViewModel;
				this.viewModel.Function = dataContext.CreateFunction(dataContext.FunctionName);

                if (viewModel.Session.CurrentUser != null)
                {
                    if (!this.viewModel.Session.FuncRepository.SaveOrUpdate(viewModel.Function))
                    {
                        //MessageBox.Show("Wasn't able to store function in Database!", "DB Error", MessageBoxButton.OK, MessageBoxImage.Error);

                        var message = new MessageWindow(WarningSigns.ErrorSign, "Wasn't able to store function in Database!", true, false);
                        message.ShowDialog();
                    }
                    else
                    {
                        // MessageBox.Show("Function stored in the Database.", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
                        var message = new MessageWindow(WarningSigns.InformationSign, "Function stored in the Database.", true, false);
                        message.ShowDialog();
                    }
                }

				this.ButtonRunFunction.IsEnabled = viewModel.HasFunction;
				this.TextBoxFunctionName.Text = viewModel.FunctionName;
				this.TextBoxFunctionName.InvalidateVisual();
			}
		}



		private void ButtonRGB_Click(object sender, RoutedEventArgs e)
		{

		}

		private void ButtonBlackAndWhite_Click(object sender, RoutedEventArgs e)
		{

		}

		private void ButtonColor1_Click(object sender, RoutedEventArgs e)
		{

		}

		private void ButtonColor2_Click(object sender, RoutedEventArgs e)
		{

		}

		private void ComboBoxItem_Selected_Example1(object sender, RoutedEventArgs e)
		{

			viewModel.Tree = Samples.SampleTree1.TreeSample();
			this.UpdateComboBoxValues();
		}

		private void ComboBoxItem_Selected_Example2(object sender, RoutedEventArgs e)
		{
			viewModel.Tree = Samples.SampleTree2.TreeSample();
			this.UpdateComboBoxValues();
 
		}

		private void ComboBoxItem_Selected_Example3(object sender, RoutedEventArgs e)
		{
			viewModel.Tree = Samples.SampleTree3.TreeSample();
			this.UpdateComboBoxValues();
		}

		private void ComboBoxItem_Selected_Example4(object sender, RoutedEventArgs e)
		{
			viewModel.Tree = Samples.SampleTree4.TreeSample();
			this.UpdateComboBoxValues();
		}

		private void ComboBoxItem_Selected_Example5(object sender, RoutedEventArgs e)
		{
			viewModel.Tree = Samples.SampleTree6.TreeSample();
			this.UpdateComboBoxValues();
		}

		private void ComboBoxItem_Selected_Example6(object sender, RoutedEventArgs e)
		{
			viewModel.Tree = Samples.SampleTreeChristmas.TreeSample();
			this.UpdateComboBoxValues();
		}

		#endregion

		private void ButtonCreateOrEdge_Click(object sender, RoutedEventArgs args)
		{
			NodeItem nodeItem = this.ModelView.NodeNames.ElementAt(SelectOrNode.SelectedIndex);
			var andGroup = ModelView.Tree.GetAndGroup(nodeItem.Node);
			if (andGroup != Guid.Empty)
            {
                var andGroupNodes = ModelView.Tree.FindAndGroupNodes(andGroup);
                if (andGroupNodes.Count() == 2)
                {
                    foreach (var edge in ModelView.Tree.Edges.Where(e => e.AndGroup == andGroup))
                    {
                        edge.AndGroup = Guid.Empty;
                    }
                }
                else
                {
                    var result = MessageBox.Show("Remove whole AND group?", "AND group", MessageBoxButton.YesNo, MessageBoxImage.Information);
                    if (result == MessageBoxResult.Yes)
                    {
                        foreach (var edge in ModelView.Tree.Edges.Where(e => e.AndGroup == andGroup))
                        {
                            edge.AndGroup = Guid.Empty;
                        }
                    }
                    else
                    {
                        ModelView.Tree.InEdges(nodeItem.Node).First().AndGroup = Guid.Empty;
                    }
                }
				//MessageBox.Show("AND connection removed.", "Remove AND Connetion");

                //var message = new MessageWindow(WarningSigns.InformationSign, "AND connection removed.", false, false);
                //message.Show();
                //message.ContentRendered += new EventHandler(this.MessageWindowShowTimeEvent);
			}
			else
			{
				//MessageBox.Show("The Node has no AND connection.", "Remove AND Connetion");

                var message = new MessageWindow(WarningSigns.InformationSign, "The Node has no AND connection.", false, false);
                message.Show();
                message.ContentRendered += new EventHandler(this.MessageWindowShowTimeEvent);
			}
			this.ModelView.NotifyTreeChanges();
			UpdateComboBoxValues();
		}

		private void ButtonRunFunction_Click(object sender, RoutedEventArgs e)
		{
            // Check if tree has all attributes required by function
            var missingAttribs = new List<string>();
            foreach (var attrib in this.viewModel.Function.AttributeFunctions)
            {
                if (!viewModel.Tree.DefaultAttributes.ContainsKey(attrib.AttributeName))
                    missingAttribs.Add(attrib.AttributeName);
            }
            if (missingAttribs.Count > 0)
            {
                // ToDo: create bigger MessageWindow here
                MessageBox.Show("The tree is missing the following attribute(s) required by the function:" + Environment.NewLine +
                    string.Join(Environment.NewLine, missingAttribs), "Missing attributes", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            // Check attribute types
            var typeErrors = new List<string>();
            foreach (var attrib in this.viewModel.Function.AttributeFunctions)
            {
                var treeAttrib = viewModel.Tree.DefaultAttributes[attrib.AttributeName];
                if (attrib.ReturnType != treeAttrib.Type)
                {
                    typeErrors.Add("\"" + attrib.AttributeName + "\" is of type " + treeAttrib.Type + ", function expectes type " + attrib.ReturnType);
                }
            }
            if (typeErrors.Count > 0)
            {
                // ToDo: create bigger message window here
                MessageBox.Show("The trees attribute types don't match the following attribute(s) required by the function:" + Environment.NewLine +
                    string.Join(Environment.NewLine, typeErrors), "Invalid attributes", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

			this.viewModel.Tree.ClearResults();

			var evaluator = new Evaluator(this.viewModel.Function);
			try
			{
				if (!evaluator.Evaluate(this.viewModel.Tree))
				{
					//MessageBox.Show("Function completed with errors!", "Function Error", MessageBoxButton.OK, MessageBoxImage.Error);
				    var message = new MessageWindow(WarningSigns.ErrorSign, "Function completed with errors!", true, false);
				    message.ShowDialog();

                    viewModel.Tree.ClearResults();
				}
			}
			catch (KeyNotFoundException)
			{
				//MessageBox.Show("Nodes are missing a required attribute!", "Function Error", MessageBoxButton.OK, MessageBoxImage.Error);

                var message = new MessageWindow(WarningSigns.ErrorSign, "Nodes are missing a required attribute!", true, false);
                message.ShowDialog();
                
                viewModel.Tree.ClearResults();
			}
			catch (Exception)
			{
				//MessageBox.Show("Function can't be evaluated! Does the function contain errors/warnings?", "Function Error", MessageBoxButton.OK, MessageBoxImage.Error);

                var message = new MessageWindow(WarningSigns.ErrorSign, "Function can't be evaluated! Does the function contain errors/warnings?", true, false);
                message.ShowDialog();


                viewModel.Tree.ClearResults();
			}
			this.viewModel.Tree.UpdateAttributes();
			this.viewModel.NotifyTreeChanges();
		}

		private void ButtonChooseDefaultFunction_Click(object sender, RoutedEventArgs e)
		{
			var selectedFunction = ComboDefaultFunctions.SelectedIndex;
			switch (selectedFunction)
			{
				case 0:
					SetCurrentFunction(DefaultFunctions.LeastCost);
					break;

				case 1:
					SetCurrentFunction(DefaultFunctions.NoSpecialToolsRequired);
					break;

				case 2:
					SetCurrentFunction(DefaultFunctions.LeastCostAndNoSpecialToolsRequired);
					break;
			}
		}

		private void SetCurrentFunction(Function function)
		{
			this.viewModel.Function = function;
			this.ButtonRunFunction.IsEnabled = viewModel.HasFunction;
			this.TextBoxFunctionName.Text = viewModel.FunctionName;
			this.TextBoxFunctionName.InvalidateVisual();
		}

		private void AttributeAddName_GotFocus(object sender, RoutedEventArgs e)
		{
			if (AttributeAddName.Text.Equals("Enter name...."))
			{
				AttributeAddName.Text = "";
			}
		   
		}

		private void BooleanAttribute_GotFocus(object sender, RoutedEventArgs e)
		{
			AttributeAddDefaultNumber.Visibility = Visibility.Collapsed;
			AttributeAddDefaultType.Visibility = Visibility.Visible;
			
		}

		private void NumberAttribute_GotFocus(object sender, RoutedEventArgs e)
		{
			AttributeAddDefaultType.Visibility = Visibility.Collapsed;
			AttributeAddDefaultNumber.Visibility = Visibility.Visible;
		}

		private void AttributeAddDefaultNumber_GotFocus(object sender, RoutedEventArgs e)
		{
			AttributeAddDefaultNumber.Text = "";
		}
		private static bool IsTextAllowed(string text)
		{
			var regex = new Regex("[^0-9.]+"); //regex that matches disallowed text
			return !regex.IsMatch(text);
		}

		// Use the PreviewTextInputHandler to respond to key presses 
		private void PreviewTextInputHandler(Object sender, TextCompositionEventArgs e)
		{
			e.Handled = !IsTextAllowed(e.Text);
		}

		// Use the DataObject.Pasting Handler  
		private void PastingHandler(object sender, DataObjectPastingEventArgs e)
		{
			if (e.DataObject.GetDataPresent(typeof(String)))
			{
				String text = (String)e.DataObject.GetData(typeof(String));
				if (!IsTextAllowed(text)) e.CancelCommand();
			}
			else e.CancelCommand();
		}
		
		private void AttributeEditDefaultNumber_GotFocus(object sender, RoutedEventArgs e) 
		{
			AttributeEditDefaultNumber.Text = "";
		}

		private void NumericEditAttribute_GotFocus(object sender, RoutedEventArgs e)
		{
			AttributeEditDefaultType.Visibility = Visibility.Collapsed;
			AttributeEditDefaultNumber.Visibility = Visibility.Visible;
		}

		private void BooleanEditAttribute_GotFocus(object sender, RoutedEventArgs e)
		{
			AttributeEditDefaultNumber.Visibility = Visibility.Collapsed;
			AttributeEditDefaultType.Visibility = Visibility.Visible;
		}

		private void AttributeAddType_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (AttributeAddDefaultNumber != null && AttributeAddDefaultType!=null)
			{
				string text = ((sender as ComboBox).SelectedItem as ComboBoxItem).Name;
				if (text.Equals("BooleanAttribute"))
				{
					AttributeAddDefaultNumber.Visibility = Visibility.Collapsed;
					AttributeAddDefaultType.Visibility = Visibility.Visible;
				}
				else
				{
					AttributeAddDefaultType.Visibility = Visibility.Collapsed;
					AttributeAddDefaultNumber.Visibility = Visibility.Visible;
				}
			}
		  
		}

		private void AttributeEditType_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (AttributeEditDefaultNumber != null && AttributeEditDefaultType != null)
			{
				string text = ((sender as ComboBox).SelectedItem as ComboBoxItem).Name;
				if (text.Equals("BooleanEditAttribute"))
				{
					AttributeEditDefaultNumber.Visibility = Visibility.Collapsed;
					AttributeEditDefaultType.Visibility = Visibility.Visible;
				}
				else if (text.Equals("NumericEditAttribute"))
				{
					AttributeEditDefaultType.Visibility = Visibility.Collapsed;
					AttributeEditDefaultNumber.Visibility = Visibility.Visible;
				}
			}
		}

		private void AttributeEditName_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			Dictionary<string, NodeAttribute> attrDictionary = this.ModelView.Tree.GetNodeAttributes();
			string attrName = ((sender as ComboBox).SelectedItem as string);
			//using node as placeHolder 
			Types type = Types.Node;
			if (attrName != null) {
				if (attrDictionary.ContainsKey(attrName))
				{
					type = attrDictionary.FirstOrDefault(x => x.Key == attrName).Value.Type;
				}
				switch (type)
				{
					case Types.Boolean:
						AttributeEditType.SelectedIndex = 1;
						break;
					case Types.Number:
						AttributeEditType.SelectedIndex = 2;
						break;
					default:
						AttributeEditType.SelectedIndex = 0;
						break;

				}
			}
		}

		private void ExpanderOkayClick(object sender, RoutedEventArgs e)
		{

			if (this.menuVisible)
			{
				this.SideMenu.Width = 0;
				this.menuVisible = false;
				this.ExpanderImage.Source = new BitmapImage(new Uri("/UserInterface;component/Images/open_sign1.png", UriKind.Relative));
			}

			else
			{
				this.SideMenu.Width = 200;
				menuVisible = true;
				this.ExpanderImage.Source = new BitmapImage(new Uri("/UserInterface;component/Images/collapse_sign.png", UriKind.Relative));

			}


		}

		private void ButtonSearchFunction_Click(object sender, RoutedEventArgs e)
		{
            if (viewModel.Session.CurrentUser != null)
            {
                var window = new SearchFunctionWindow(this.viewModel.Session);
                var result = window.ShowDialog();
                if (result != null && result == true)
                {
                    this.viewModel.Function = ((FunctionSearchViewModel)window.DataContext).SelectedResult.Function;
                    this.ButtonRunFunction.IsEnabled = viewModel.HasFunction;
                    this.TextBoxFunctionName.Text = viewModel.FunctionName;
                    this.TextBoxFunctionName.InvalidateVisual();
                }
            }
            else
            {
                var message = new MessageWindow(WarningSigns.WarningSign, "Can't access database when not logged in.", false, false);
                message.Show();
                message.ContentRendered += new EventHandler(this.MessageWindowShowTimeEvent);
            }

		} 

		private void ButtonNewTree_Click(object sender, RoutedEventArgs e)
		{
            //if (this.ModelView.Tree.HasChanged)
            //{
                //var dialogResult = MessageBox.Show("Your tree has unsaved changes, loose them?", "Create empty tree", MessageBoxButton.YesNo);

		    var message = new MessageWindow(
		        WarningSigns.WarningSign, "Your tree has unsaved changes, loose them?", true, true);
		    message.ShowDialog();

                if (message.DialogResult == true)
                {
                    this.ModelView.Tree = new Tree();
                    var rootnode = new Node("Root");
                    this.ModelView.Tree.AddNode(rootnode, null);
                    this.ModelView.NotifyTreeChanges();
                    this.GraphSharpControl.ZoomControl.ZoomToFill();

                    //this.ModelView.ColorTheme.ClearColorDictionary();
                }
            //}
		}

        private void ButtonCollapseAll_OnClick(object sender, RoutedEventArgs e)
        {
            // if it means collapse all nodes use this
            //this.ModelView.Tree.CollapseLevel(1);
            //this.ModelView.NotifyTreeChanges();

            // if it means collapse all attributes use this
            foreach (var node in this.ModelView.Tree.Vertices)
            {
                node.ShowsAttributes = false;
                this.ModelView.NotifyTreeChanges();
            }
        }

        private void ComboBoxCollapeNodesLowerLevel_OnDropDownClosed(object sender, EventArgs e)
        {
            if (this.ComboBoxCollapeNodesLowerLevel.SelectedItem != null)
            {
                var value = (int)this.ComboBoxCollapeNodesLowerLevel.SelectedItem;
                this.ModelView.Tree.CollapseLevel(value);
            }
        }

        private void ComboBoxExpandNodesLowerLevel_OnDropDownClosed(object sender, EventArgs e)
        {
            if (this.ComboBoxCollapeNodesLowerLevel.SelectedItem != null)
            {
                var value = (int)this.ComboBoxExpandNodesLowerLevel.SelectedItem;
                this.ModelView.Tree.ExpandLevel(value);
            }
        }

        private void ButtonRelayoutTree_OnClick(object sender, RoutedEventArgs e)
        {
            this.GraphSharpControl.graphLayout.Relayout();
            //this.ModelView.NotifyTreeChanges();
        }
	}
}
