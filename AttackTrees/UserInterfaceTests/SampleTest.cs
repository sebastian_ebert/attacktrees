﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace UserInterfaceTests {

    [TestFixture]
    public class SampleTest {

        [TestFixtureSetUp]
        public void SetUp() {
            // TODO: Set up test class here
        }

        [TestFixtureTearDown]
        public void TearDown() {
            // TODO: Tear down test class here (freee resources allocated in SetUp())
        }

        [Test]
        public void Test() {
            // Simple example test method
            var x = 1 + 2;
            Assert.AreEqual(3, x);
        }

    }

}
