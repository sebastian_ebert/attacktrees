﻿// -----------------------------------------------------------------------
// <copyright file="UserRepositoryTest.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace PersistenceTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using NUnit.Framework;
    using Model;
    using Persistence;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    [TestFixture]
    public class UserRepositoryTest
    {
        [TestFixtureSetUp]
        public void init()
        {
            DatabaseService.Instance.Reconfigure(@".\Config\db-properties.xml");
        }

        User _user;
        [SetUp]
        public void SetUp()
        {
            _user = new User();
            _user.Username = "username";
            _user.FirstName = "firstName";
            _user.LastName = "lastName";
            _user.Password = "password";
            _user.Salt = "s4lz1g";

            try
            {
                UserRepository.Instance.CreateUpdateUser(_user);
            }
            catch (Exception e)
            {
                //Dont give a F*, just init
            }
        }

        [TearDown]
        public void TearDown()
        {
            try{
                UserRepository.Instance.DeleteUser(_user);
            }
            catch(Exception e) {
                //Dont give a F*, just clean up
            }
        }
    
        [Test]        
        public void CanInsertExistingUser()
        {
            UserRepository.Instance.CreateUpdateUser(_user);
        }

        [Test]
        public void CanUpdateExistingUser()
        {
            _user.LastName = "Werner";
            UserRepository.Instance.CreateUpdateUser(_user);
            Assert.AreEqual("Werner", UserRepository.Instance.ReadUser(_user.Username).LastName);
        }

        [Test]
        public void CanDeleteUser()
        {
            Assert.IsTrue(UserRepository.Instance.DeleteUser(_user));            
        }

        [Test]
        [ExpectedException(typeof(NoSuchUserException))]
        public void CannotDeleteNonExistingUser()
        {
            Assert.IsTrue(UserRepository.Instance.DeleteUser(new User()));
        }

        [Test]
        public void CanLoadUser()
        {
            User dbUser = UserRepository.Instance.ReadUser(_user.Username);
            Assert.IsTrue(_user.Username == dbUser.Username);
            Assert.IsTrue(_user.FirstName == dbUser.FirstName);
            Assert.IsTrue(_user.LastName == dbUser.LastName);
            Assert.IsTrue(_user.Password == dbUser.Password);
            Assert.IsTrue(_user.Salt == dbUser.Salt);
 
        }

        [Test]
        public void CanAddNewPermission()
        {
            Assert.IsTrue(UserRepository.Instance.AddPermission(_user, "readTree"));
         }

        [Test]
        [ExpectedException(typeof(DuplicatePermissionException))]
        public void CannotAddDuplicatePermission()
        {   
            UserRepository.Instance.AddPermission(_user, "readTree");
            UserRepository.Instance.AddPermission(_user, "readTree");        
        }

        [Test]
        public void CanDeletePermission() 
        {
            UserRepository.Instance.AddPermission(_user, "readTree");
            UserRepository.Instance.DeletePermission(_user, "readTree");
        }

        [Test]
        [ExpectedException(typeof(NoSuchPermissionException))]
        public void CannotDeleteNonExistantPermission()
        {           
            UserRepository.Instance.DeletePermission(_user, "hurpDurpPermission");
        }

        [Test]
        public void CanGetPermissionList()
        {
            UserRepository.Instance.AddPermission(_user, "hurpTree");
            UserRepository.Instance.AddPermission(_user, "durpTree");
            UserRepository.Instance.AddPermission(_user, "hugTree");
            Assert.AreEqual(3, UserRepository.Instance.GetPermissions(_user).Count);
        }

        [Test]
        public void CannotGetPermissionListForUserWithoutPermission()
        {           
            Assert.AreEqual(0, UserRepository.Instance.GetPermissions(_user).Count);            
        }

        [Test]
        public void CannotGetPermissionListForNewUser()
        {
             Assert.AreEqual(0, UserRepository.Instance.GetPermissions(new User()).Count);
        }
    }
}
