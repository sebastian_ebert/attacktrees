﻿using System;
using Model.Trees;
using NUnit.Framework;
using Persistence;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Model.Functions;

namespace PersistenceTests
{
    [TestFixture]
    public class TreeRepositoryTest
    {
        [TestFixtureSetUp]
        public void init()
        {
            DatabaseService.Instance.Reconfigure(@".\Config\db-properties.xml");
        }

        private Tree _tree;
        private Node _node;
        [SetUp]
        public void setUp()
        {
            /// Build Bruce Schneiers sample tree
            ///
            _tree = new Tree();
            _tree.Name = "Bruce's tree";
            Node openSafe = new Node("Open Safe");
            Node pickLock = new Node("Pick Lock");
            Node learnCombo = new Node("Learn Combo");
            Node cutOpenSafe = new Node("Cut Open Safe");
            Node installImproperly = new Node ("Install Improperly");
            Node findWrittenCombo = new Node ("Find Written Combo");
            Node getComboFromTarget = new Node ("Get Combo from Target");
            Node threaten = new Node ("Threaten");
            Node eavesdrop = new Node("Eavesdrop");
            Node blackmail = new Node ("Blackmail");
            Node bribe = new Node ("Bribe");
            Node listenToConversation = new Node("Listen to Conversation");
            Node getTargetToStateCombo = new Node("Get Target to State Combo");

            NodeAttribute testAttribute = new NodeAttribute("specialEquip", false);
            NodeAttribute testAttribute2 = new NodeAttribute("cost", 200);
            getTargetToStateCombo.AddAttribute(testAttribute);
            getTargetToStateCombo.AddAttribute(testAttribute2);

            _tree.AddNode(openSafe, null);
            _tree.AddNode(pickLock,openSafe);
            _tree.AddNode(learnCombo,openSafe);
            _tree.AddNode(cutOpenSafe, openSafe);
            _tree.AddNode(installImproperly, openSafe);
            _tree.AddNode(findWrittenCombo, learnCombo);
            _tree.AddNode(getComboFromTarget, learnCombo);
            _tree.AddNode(threaten, getComboFromTarget);
            _tree.AddNode(eavesdrop, getComboFromTarget);
            _tree.AddNode(blackmail, getComboFromTarget);
            _tree.AddNode(bribe, getComboFromTarget);
            _tree.AddNode(listenToConversation,eavesdrop);
            _tree.AddNode(getTargetToStateCombo,eavesdrop);

            /// Add and connection
            /// 
            ///
            Edge andEdgeOne = null, andEdgeTwo = null;
            Guid andGroupId = Guid.NewGuid();
            _tree.TryGetEdge(eavesdrop, listenToConversation, out andEdgeOne);
            _tree.TryGetEdge(eavesdrop, getTargetToStateCombo, out andEdgeTwo);
            andEdgeOne.AndGroup = andGroupId;
            andEdgeTwo.AndGroup = andGroupId;
            _node = new Node("TestNode");
        }

        [Test]
        public void CanFindAllTreesByAttributeName()
        {
            TreeRepository.Instance.SaveOrUpdate(_tree);
            List<Tree> trees = TreeRepository.Instance.FindAllByAttributeName("%ost%");
            Assert.IsTrue(trees.Count > 0);
        }

        [Test]
        public void CannotFindAllTreesByNonExistingAttributeName()
        {
            TreeRepository.Instance.SaveOrUpdate(_tree);
            List<Tree> trees = TreeRepository.Instance.FindAllByAttributeName("%deadbeef%");
            Assert.IsTrue(trees.Count == 0);
        }

        [Test]
        public void CanFindAllTreesByNodeName()
        {
            TreeRepository.Instance.SaveOrUpdate(_tree);
            List<Tree> trees = TreeRepository.Instance.FindAllByNodeName("%written%");
            Assert.IsTrue(trees.Count > 0);
        }

        [Test]
        public void CannotFindAllTreesByNonExistingNodeName()
        {
            TreeRepository.Instance.SaveOrUpdate(_tree);
            List<Tree> trees = TreeRepository.Instance.FindAllByNodeName("%deadbeef%");
            Assert.IsTrue(trees.Count == 0);
        }

        [Test]
        public void CanFindTreeByName()
        {
            Assert.IsNotNull(TreeRepository.Instance.FindByName("Bruce's tree"));
        }

        [Test]
        public void CanFindAllTreesByName()
        {
            TreeRepository.Instance.SaveOrUpdate(_tree);
            List<Tree> trees = TreeRepository.Instance.FindAllByName("%bruce%");
            Assert.IsTrue(trees.Count > 0);
        }

        [Test]
        [ExpectedException(typeof(NoSuchTreeException))]
        public void CannotFindNonExistingTreeByName()
        {
            Assert.IsNull(TreeRepository.Instance.FindByName("Derptree"));
        }

        [Test]
        public void CanAddAttribute()
        {
            NodeAttribute na = new NodeAttribute("testAttribute", Types.Number, "123");
            Assert.IsNotNull(TreeRepository.Instance.SaveOrUpdateAttribute(na));
        }

        [Test]
        public void CanAttachAttributeToNode()
        {
            
            NodeAttribute na = new NodeAttribute("testAttribute2", Types.Number, "123");
            Assert.IsNotNull(TreeRepository.Instance.SaveOrUpdateAttribute(na));
            TreeRepository.Instance.SaveOrUpdateNode(_node);
            Assert.IsTrue(TreeRepository.Instance.AttachAttributeToNode(_node, na));
            Assert.IsTrue(TreeRepository.Instance.NodeHasAttribute(_node, na));
        }

        [Test]
        public void CanLoadTreeWithAttributes()
        {
            TreeRepository.Instance.SaveOrUpdate(_tree);
            Tree t = TreeRepository.Instance.GetById(_tree.Id);
            int attrCount = 0;
            foreach (Node n in t.Leafs)
            {
                attrCount += n.Attributes.Count;
            }
            Console.WriteLine("Attribute Count: " + attrCount);
            Assert.IsTrue(attrCount > 0);
            TreeRepository.Instance.Delete(_tree);
        }

        [Test]
        public void CanPopulateDefaultAttributesOnTreeLoad()
        {
            TreeRepository.Instance.SaveOrUpdate(_tree);
            Tree t = TreeRepository.Instance.GetById(_tree.Id);
            Assert.IsTrue(t.DefaultAttributes.Count > 0);
            TreeRepository.Instance.Delete(_tree);
        }

        [Test]
        public void CanInitializeRepository()
        {
            Assert.IsNotNull(TreeRepository.Instance);
        }
        [Test]
        public void CanSaveOrUpdateNode()
        {
            Assert.IsNotNull(TreeRepository.Instance.SaveOrUpdateNode(_node));
        }
        [Test]
        public void CanLoadNode()
        {
            TreeRepository.Instance.SaveOrUpdateNode(_node);
            Assert.AreEqual(_node,TreeRepository.Instance.GetNodeById(_node.Id));
        }

        [Test]
        public void CanStoreTree()
        {
            bool success = TreeRepository.Instance.SaveOrUpdate(_tree);
            Assert.IsTrue(success);
        }

        [Test]
        public void CanLoadTree()
        {
            bool success = TreeRepository.Instance.SaveOrUpdate(_tree);
            Assert.IsTrue(success);

            /// Load it again
            /// 
            Tree persistedTree = TreeRepository.Instance.GetById(_tree.Id);
            Assert.AreEqual(_tree.VertexCount, persistedTree.VertexCount);
            Assert.AreEqual(_tree.EdgeCount, persistedTree.EdgeCount);
            
        }

        [Test]
        public void CannotFindMissingTree()
        {
            bool found = TreeRepository.Instance.TreeExists(Guid.Empty);
            Assert.IsFalse(found);
        }

        [Test]
        public void CanFindExistingTree()
        {
            TreeRepository.Instance.SaveOrUpdate(_tree);                
            bool found = TreeRepository.Instance.TreeExists(_tree.Id);
            Assert.IsTrue(found);
        }

        [Test]
        public void CanDeleteExistingTree()
        {
            TreeRepository.Instance.SaveOrUpdate(_tree);
            bool success = TreeRepository.Instance.Delete(_tree);
            Assert.IsTrue(success);
            bool found = TreeRepository.Instance.TreeExists(_tree.Id);
            Assert.IsFalse(found);
        }

        [Test]
        public void CannotDeleteNonExistingTree()
        {
            bool success = TreeRepository.Instance.Delete(new Tree());
            Assert.IsFalse(success);
            bool found = TreeRepository.Instance.TreeExists(_tree.Id);
            Assert.IsFalse(found);
        }

        [Test]
        public void CanUpdateExistingTree()
        {
            TreeRepository.Instance.SaveOrUpdate(_tree);
            Tree t = TreeRepository.Instance.GetById(_tree.Id);
            Assert.IsNotNull(t);
            IEnumerator<Node> enumerator = t.Leafs.GetEnumerator();
            enumerator.MoveNext();
            t.RemoveNode(enumerator.Current);
            bool success = TreeRepository.Instance.SaveOrUpdate(t);
            Assert.IsTrue(success);
        }


        [Test]
        public void CanAddNewPermission()
        {
            bool success = TreeRepository.Instance.SaveOrUpdate(_tree);
            Assert.IsTrue(TreeRepository.Instance.AddPermission(_tree, "readTree"));
            TreeRepository.Instance.Delete(_tree);
        }

        [Test]
        [ExpectedException(typeof(DuplicatePermissionException))]
        public void CannotAddDuplicatePermission()
        {
            bool success = TreeRepository.Instance.SaveOrUpdate(_tree);
            TreeRepository.Instance.AddPermission(_tree, "readTree");
            TreeRepository.Instance.AddPermission(_tree, "readTree");
            TreeRepository.Instance.Delete(_tree);
        }

        [Test]
        public void CanDeletePermission()
        {
            bool success = TreeRepository.Instance.SaveOrUpdate(_tree);
            TreeRepository.Instance.AddPermission(_tree, "readTree");
            TreeRepository.Instance.DeletePermission(_tree, "readTree");
            TreeRepository.Instance.Delete(_tree);
        }

        [Test]
        [ExpectedException(typeof(NoSuchPermissionException))]
        public void CannotDeleteNonExistantPermission()
        {
            bool success = TreeRepository.Instance.SaveOrUpdate(_tree);
            TreeRepository.Instance.DeletePermission(_tree, "hurpDurpPermission");
            TreeRepository.Instance.Delete(_tree);
        }

        [Test]
        public void CanGetPermissionList()
        {
            bool success = TreeRepository.Instance.SaveOrUpdate(_tree);
            TreeRepository.Instance.AddPermission(_tree, "hurpTree");
            TreeRepository.Instance.AddPermission(_tree, "durpTree");
            TreeRepository.Instance.AddPermission(_tree, "hugTree");
            Assert.AreEqual(3, TreeRepository.Instance.GetPermissions(_tree).Count);
            TreeRepository.Instance.Delete(_tree);
        }

        [Test]
        public void CannotGetPermissionListForTreeWithoutPermission()
        {
            Assert.AreEqual(0, TreeRepository.Instance.GetPermissions(_tree).Count);
        }

        [Test]
        public void CannotGetPermissionListForNewTree()
        {
            Assert.AreEqual(0, TreeRepository.Instance.GetPermissions(new Tree()).Count);
        }

        [Test]
        public void CanLoadAllTrees()
        {
            bool success = TreeRepository.Instance.SaveOrUpdate(_tree);
            Assert.IsTrue(TreeRepository.Instance.GetAllTrees().Count > 0);
            TreeRepository.Instance.Delete(_tree);
        }
    }

}
