﻿using System;
using Model.Trees;
using NUnit.Framework;
using Persistence;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Model.Functions;

namespace PersistenceTests
{
    [TestFixture]
    public class FunctionRepositoryTest
    {

        [TestFixtureSetUp]
        public void init()
        {
            DatabaseService.Instance.Reconfigure(@".\Config\db-properties.xml"); 
        }

        private Tree _tree;
        private Node _node;
        NodeAttribute _testAttribute, _testAttribute2;
        Function _function;
        [SetUp]
        public void setUp()
        {
            /// Build Bruce Schneiers sample tree
            ///
            _tree = new Tree();
            _tree.Name = "Bruce's tree";
            Node openSafe = new Node("Open Safe");
            Node pickLock = new Node("Pick Lock");
            Node learnCombo = new Node("Learn Combo");
            Node cutOpenSafe = new Node("Cut Open Safe");
            Node installImproperly = new Node ("Install Improperly");
            Node findWrittenCombo = new Node ("Find Written Combo");
            Node getComboFromTarget = new Node ("Get Combo from Target");
            Node threaten = new Node ("Threaten");
            Node eavesdrop = new Node("Eavesdrop");
            Node blackmail = new Node ("Blackmail");
            Node bribe = new Node ("Bribe");
            Node listenToConversation = new Node("Listen to Conversation");
            Node getTargetToStateCombo = new Node("Get Target to State Combo");

            _testAttribute = new NodeAttribute("specialEquip", false);
            _testAttribute2 = new NodeAttribute("cost", 200);
            getTargetToStateCombo.AddAttribute(_testAttribute);
            getTargetToStateCombo.AddAttribute(_testAttribute2);
            getTargetToStateCombo.AddAttribute(new NodeAttribute("cost2", 200));

            _function = new Function("TestFunction");
            _function.Add("cost", Model.Functions.Operators.AttributeOperators.Add, Model.Functions.Operators.GroupOperators.Add, Model.Functions.Operators.SelectOperators.Minimum);
            _function.Add("specialEquip", Model.Functions.Operators.AttributeOperators.Or, Model.Functions.Operators.GroupOperators.Undefined, Model.Functions.Operators.SelectOperators.IsTrue);

            _tree.AddNode(openSafe, null);
            _tree.AddNode(pickLock,openSafe);
            _tree.AddNode(learnCombo,openSafe);
            _tree.AddNode(cutOpenSafe, openSafe);
            _tree.AddNode(installImproperly, openSafe);
            _tree.AddNode(findWrittenCombo, learnCombo);
            _tree.AddNode(getComboFromTarget, learnCombo);
            _tree.AddNode(threaten, getComboFromTarget);
            _tree.AddNode(eavesdrop, getComboFromTarget);
            _tree.AddNode(blackmail, getComboFromTarget);
            _tree.AddNode(bribe, getComboFromTarget);
            _tree.AddNode(listenToConversation,eavesdrop);
            _tree.AddNode(getTargetToStateCombo,eavesdrop);

            /// Add and connection
            /// 
            ///
            Edge andEdgeOne = null, andEdgeTwo = null;
            Guid andGroupId = Guid.NewGuid();
            _tree.TryGetEdge(eavesdrop, listenToConversation, out andEdgeOne);
            _tree.TryGetEdge(eavesdrop, getTargetToStateCombo, out andEdgeTwo);
            andEdgeOne.AndGroup = andGroupId;
            andEdgeTwo.AndGroup = andGroupId;
            _node = new Node("TestNode");
        }

        [TearDown]
        public void tearDown()
        {
            FunctionRepository.Instance.Delete(_function);
        }

        [Test]
        public void CanFindAllFunctionsByName()
        {
            Assert.IsTrue(FunctionRepository.Instance.SaveOrUpdate(_function));
            List<Function> functions =  FunctionRepository.Instance.FindAllByName("Test%");
            Assert.IsTrue(functions.Count > 0);
        }

        [Test]
        public void CannotFindAllFunctionsByNameThatDontExist()
        {
            Assert.IsTrue(FunctionRepository.Instance.SaveOrUpdate(_function));
            List<Function> functions = FunctionRepository.Instance.FindAllByName("%DERP%");
            Assert.IsTrue(functions.Count == 0);
        }

        [Test]
        public void CanFindFunctionByName()
        {
            Assert.IsTrue(FunctionRepository.Instance.SaveOrUpdate(_function));
            Assert.IsNotNull(FunctionRepository.Instance.FindByName("TestFunction"));
        }

        [Test]
        [ExpectedException(typeof(NoSuchFunctionException))]
        public void CannotFindNonExistingFunctionByName()
        {
            Assert.IsTrue(FunctionRepository.Instance.SaveOrUpdate(_function));
            Assert.IsNotNull(FunctionRepository.Instance.FindByName("Derpfunction"));
        }

        [Test]
        public void CanInsertFunction()
        {
            TreeRepository.Instance.SaveOrUpdate(_tree);
            Assert.IsTrue(FunctionRepository.Instance.SaveOrUpdate(_function));
            TreeRepository.Instance.Delete(_tree);
        }

        [Test]
        public void CanUpdateFunction()
        {
            TreeRepository.Instance.SaveOrUpdate(_tree);
            Assert.IsTrue(FunctionRepository.Instance.SaveOrUpdate(_function));
            _function.Add("cost2", Model.Functions.Operators.AttributeOperators.Add, Model.Functions.Operators.GroupOperators.Multiply, Model.Functions.Operators.SelectOperators.Minimum);
            Assert.IsTrue(FunctionRepository.Instance.SaveOrUpdate(_function));
            TreeRepository.Instance.Delete(_tree);
        }

        [Test]
        public void CanDeleteFunction()
        {
            TreeRepository.Instance.SaveOrUpdate(_tree);
            Assert.IsTrue(FunctionRepository.Instance.SaveOrUpdate(_function));
            Assert.IsTrue(FunctionRepository.Instance.Delete(_function));
            TreeRepository.Instance.Delete(_tree);
        }

        [Test]
        public void CanLoadFunction()
        {
            TreeRepository.Instance.SaveOrUpdate(_tree);
            Assert.IsTrue(FunctionRepository.Instance.SaveOrUpdate(_function));
            Function f = FunctionRepository.Instance.GetById(_function.Id);
            Assert.IsNotNull(f);
            Assert.IsTrue(f.AttributeFunctions.Count == 2);
            Assert.IsNotNullOrEmpty(f.Name);
            TreeRepository.Instance.Delete(_tree);
        }

        [Test]
        [ExpectedException(typeof(NoSuchFunctionException))]
        public void CannotLoadNonExistingFunction()
        {
            TreeRepository.Instance.SaveOrUpdate(_tree);
            Assert.IsTrue(FunctionRepository.Instance.SaveOrUpdate(_function));
            Function f = FunctionRepository.Instance.GetById(Guid.Empty);
            Assert.IsNull(f);
            
            TreeRepository.Instance.Delete(_tree);
        }

        [Test]
        [ExpectedException(typeof(NoSuchFunctionException))]
        public void CannotLoadNonExistingFunctionByName()
        {
            TreeRepository.Instance.SaveOrUpdate(_tree);
            Assert.IsTrue(FunctionRepository.Instance.SaveOrUpdate(_function));
            Function f = FunctionRepository.Instance.FindByName("NonExistingName");
            Assert.IsNull(f);

            TreeRepository.Instance.Delete(_tree);
        }

        [Test]        
        public void CanLoadFunctionByName()
        {
            TreeRepository.Instance.SaveOrUpdate(_tree);
            Assert.IsTrue(FunctionRepository.Instance.SaveOrUpdate(_function));
            Function f = FunctionRepository.Instance.FindByName(_function.Name);
            Assert.IsNotNull(f);

            TreeRepository.Instance.Delete(_tree);
        }

        [Test]
        public void CanLoadFunctionByAttributeName()
        {
            TreeRepository.Instance.SaveOrUpdate(_tree);
            Assert.IsTrue(FunctionRepository.Instance.SaveOrUpdate(_function));
            List<String> attributeNames = new List<String>();
            attributeNames.Add(_testAttribute.Name);
            List<Function> functions = (List<Function>)FunctionRepository.Instance.FindByAttributes(attributeNames);
            Assert.IsNotNull(functions);
            Assert.IsTrue(functions.Count == 1);

            TreeRepository.Instance.Delete(_tree);
        }

        [Test]
        public void CanLoadUniqueFunctionByAttributeNames()
        {
            TreeRepository.Instance.SaveOrUpdate(_tree);
            Assert.IsTrue(FunctionRepository.Instance.SaveOrUpdate(_function));
            List<String> attributeNames = new List<String>();
            attributeNames.Add(_testAttribute.Name);
            attributeNames.Add(_testAttribute2.Name);
            List<Function> functions = (List<Function>)FunctionRepository.Instance.FindByAttributes(attributeNames);
            Assert.IsNotNull(functions);
            Assert.IsTrue(functions.Count == 1);

            TreeRepository.Instance.Delete(_tree);
        }

        [Test]
        public void CannotLoadFunctionsByInvalidAttributeName()
        {
            TreeRepository.Instance.SaveOrUpdate(_tree);
            Assert.IsTrue(FunctionRepository.Instance.SaveOrUpdate(_function));
            List<String> attributeNames = new List<String>();
            attributeNames.Add("xyzinvalid");
            List<Function> functions = (List<Function>)FunctionRepository.Instance.FindByAttributes(attributeNames);
            Assert.IsEmpty(functions);
            TreeRepository.Instance.Delete(_tree);
        }
      
    }

}
