﻿// -----------------------------------------------------------------------
// <copyright file="DatabaseServiceTest.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace PersistenceTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
using NUnit.Framework;
    using Persistence;
    using Model.Functions.Operators;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class DatabaseServiceTest
    {
        [Test]
        public void CanReconfigureDBConfig()
        {
            DatabaseService.Instance.Reconfigure(@".\Config\db-properties.xml");
        }

        [Test]
        public void CanGetEnumValues()
        {
            Console.WriteLine("AttributeOperators");
            foreach (string name in Enum.GetNames(typeof(AttributeOperators)))
            {

                var attribute_operator = (AttributeOperators)Enum.Parse(typeof(AttributeOperators), name);
                Console.WriteLine(name + ": " + (int)attribute_operator);
            }

            Console.WriteLine("GroupOperators");
            foreach (string name in Enum.GetNames(typeof(GroupOperators)))
            {

                var attribute_operator = (GroupOperators)Enum.Parse(typeof(GroupOperators), name);
                Console.WriteLine(name + ": " + (int)attribute_operator);
            }

            Console.WriteLine("SelectOperators");
            foreach (string name in Enum.GetNames(typeof(SelectOperators)))
            {

                var attribute_operator = (SelectOperators)Enum.Parse(typeof(SelectOperators), name);
                Console.WriteLine(name + ": " + (int)attribute_operator);
            }
        }
    }
}
