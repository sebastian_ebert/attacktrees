﻿// -----------------------------------------------------------------------
// <copyright file="INodeStorage.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Persistence
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Interface which provices Storage of nodes
    /// </summary>
    public interface INodeStorage
    {
        Model.Trees.Node SaveOrUpdateNode(Model.Trees.Node n);

        Model.Trees.Node GetNodeById(Guid id);
    }
}
