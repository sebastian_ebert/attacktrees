﻿// -----------------------------------------------------------------------
// <copyright file="PersistenceConfig.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Persistence
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Diagnostics;
    using System.Xml;
    using MySql.Data.MySqlClient;
    using System.IO;
    using System.Data;

    /// <summary>
    /// Provides general database functions 
    /// </summary>
    public sealed class DatabaseService
    {
        /// <summary>
        /// MySQL connection string
        /// </summary>
        private String _connectionString = "";
        /// <summary>
        /// MySqlDBConnection
        /// </summary>
        private MySqlConnection _connection;
        /// <summary>
        /// Stores the queries inside a map
        /// </summary>
        private Dictionary<String, String> _queries = new Dictionary<string, string>();

        /// <summary>
        /// Singleton Instance
        /// </summary>
        private static readonly DatabaseService _instance = new DatabaseService();

        /// <summary>
        /// Reconfigure the DatabaseService with a different configuration
        /// </summary>
        /// <param name="configPath"></param>
        public void Reconfigure(String configPath) 
        {
            _queries.Clear();
            Initialize(configPath);
        }

        /// <summary>
        /// Initialize the DatabaseService Instance
        /// </summary>
        /// <param name="configPath">The path to the config file</param>
        private void Initialize(String configPath)
        {
            XmlDocument dbConfig = new XmlDocument();
            XmlDocument sqlQueries = new XmlDocument();
            String servername;
            String database;
            String username;
            String password;
            String port;
            String applySchemaTmp;
            String insertDataTmp;
            Boolean applySchema = false;
            Boolean insertData = false;

            dbConfig.Load(configPath);

            if (String.IsNullOrEmpty(servername = dbConfig.SelectNodes("/db-properties/server")[0].InnerText))
            {
                throw new InvalidDatabaseConfigException("server property missing.");
            }
            if (String.IsNullOrEmpty(database = dbConfig.SelectNodes("/db-properties/database")[0].InnerText))
            {
                throw new InvalidDatabaseConfigException("database property missing.");
            }
            if (String.IsNullOrEmpty(username = dbConfig.SelectNodes("/db-properties/username")[0].InnerText))
            {
                throw new InvalidDatabaseConfigException("username property missing.");
            }

            //Port may be null

            port = dbConfig.SelectNodes("/db-properties/port")[0].InnerText;

            //Password may be null
            password = dbConfig.SelectNodes("/db-properties/password")[0].InnerText;

            if ((applySchemaTmp = dbConfig.SelectNodes("/db-properties/apply_schema")[0].InnerText) == null)
            {
                throw new InvalidDatabaseConfigException("apply_schema property missing.");
            }
            if ((insertDataTmp = dbConfig.SelectNodes("/db-properties/insert_data")[0].InnerText) == null)
            {
                throw new InvalidDatabaseConfigException("insert_data property missing.");
            }

            Boolean.TryParse(applySchemaTmp, out applySchema);
            Boolean.TryParse(insertDataTmp, out insertData);

            //Load the queries
            sqlQueries.Load(@".\Config\queries.xml");
            XmlNodeList queryNodes = sqlQueries.SelectNodes("/sql-queries/query");

            foreach (XmlNode queryNode in queryNodes)
            {
                //Store in our map
                _queries.Add(queryNode.Attributes["name"].InnerText, queryNode.InnerText);
            }

            StringBuilder connectionStringBuilder = new StringBuilder();
            connectionStringBuilder.Append("SERVER=").Append(servername).Append(";")
                .Append("PORT=").Append(port).Append(";")
                .Append("DATABASE=").Append(database).Append(";")
                .Append("UID=").Append(username).Append(";")
                .Append("PWD=").Append(password).Append(";");

            this._connectionString = connectionStringBuilder.ToString();

            //Initialize Database Connection
            try
            {
                this._connection = new MySqlConnection(this._connectionString);
                this._connection.Open();
                //Check if the schema should be applied to the database
                if (applySchema)
                {
                    MySqlScript schemaScript = new MySqlScript(this._connection, File.ReadAllText(@".\Schema\tables.sql"));
                    int result = schemaScript.Execute();
                }
                //Check if data should be inserted
                if (insertData)
                {
                    MySqlScript dataScript = new MySqlScript(this._connection, File.ReadAllText(@".\Schema\data.sql"));
                    int result = dataScript.Execute();
                }
            }
            catch (MySqlException me)
            {
                Debug.WriteLine(me.ErrorCode + ": " + me.Message);
                Debug.WriteLine(me.StackTrace);
            }
        }

        /// <summary>
        /// Prevents a default instance of the TreeRepository
        /// </summary>
        private DatabaseService()
        {
            Initialize(@".\Config\db-properties.xml");
        }
        /// <summary>
        /// Gets the initialized and connected TreeRepository Instance
        /// </summary>
        /// <exception>Can throw an InvalidDatabaseConfigException when the TreeRepository is initialized but the config file is missing / invalid.</exception>
        public static DatabaseService Instance
        {
            get
            {
                return _instance;
            }
        }

        /// <summary>
        /// Get the open MySqlConnection object
        /// </summary>
        public MySqlConnection Connection
        {
            get
            {
                return _instance._connection;
            }
        }

        /// <summary>
        /// Get a MySqlCommand Object for the specified query
        /// </summary>
        /// <param name="query"></param>
        /// <returns>A MySqlCommand object which is already prepared to accept statement parameters</returns>
        public MySqlCommand GetCommandForQuery(String queryTemplateName)
        {
            MySqlCommand cmd = Connection.CreateCommand();
            string queryTemplate;
            _instance._queries.TryGetValue(queryTemplateName, out queryTemplate);
            if (String.IsNullOrEmpty(queryTemplate))
            {
                throw new Exception("Invalid SQL query template!");
            }
            cmd.CommandText = queryTemplate;
            cmd.Prepare();
            return cmd;
        }
    }
    
}
