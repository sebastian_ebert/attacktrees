﻿// -----------------------------------------------------------------------
// <copyright file="UserRepository.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Persistence
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Model;
    using MySql.Data.MySqlClient;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public sealed class UserRepository : IUserRepository, IPermissionStorage<User>
    {
        /// <summary>
        /// Singleton instance
        /// </summary>
        private static readonly UserRepository _instance = new UserRepository();

        /// <summary>
        /// Gets the initialized and connected UserRepository Instance
        /// </summary>
        /// <exception>Can throw an InvalidDatabaseConfigException when the UserRepository is initialized but the config file is missing / invalid.</exception>
        public static UserRepository Instance
        {
            get { return _instance; }
        }

        /// <summary>
        /// Inserts a new user into the database.
        /// </summary>
        /// <param name="user">The User Object to insert.</param>
        /// <returns>True on success, false otherwise</returns>
        /// <exception cref="UserAlreadyExistsException"></exception>
        public bool CreateUpdateUser(User user)
        {
            MySqlTransaction transaction = DatabaseService.Instance.Connection.BeginTransaction();
            if (UserExists(user.Username) == true)
            {
                try
                {
                    if (DeleteUser(user) == false)
                    {
                        transaction.Rollback();
                        return false;
                    }
                }
                catch (NoSuchUserException nsue)
                {
                    transaction.Rollback();
                    throw nsue;
                }
            }
            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("insert_user");
            cmd.Parameters.Add("@id", MySqlDbType.VarChar).Value = user.Id.ToString();
            cmd.Parameters.Add("@username", MySqlDbType.VarChar).Value = user.Username;
            cmd.Parameters.Add("@first_name", MySqlDbType.VarChar).Value = user.FirstName;
            cmd.Parameters.Add("@last_name", MySqlDbType.VarChar).Value = user.LastName;
            cmd.Parameters.Add("@password", MySqlDbType.VarChar).Value = user.Password;
            
            cmd.Parameters.Add("@salt", MySqlDbType.VarChar).Value = user.Salt;
            if (cmd.ExecuteNonQuery() == 1)
            {
                try
                {
                    //Insert permission
                    foreach (string permission in user.Rights)
                    {
                        AddPermission(user, permission);
                    }
                }
                catch (NoSuchUserException nsue)
                {
                    transaction.Rollback();
                    throw nsue;
                }
                catch (DuplicatePermissionException dpe)
                {
                    transaction.Rollback();
                    throw dpe;
                }
                transaction.Commit();
                return true;
            }
            transaction.Rollback();
            return false;
        }

        /// <summary>
        /// Checks if a user already exists in the database
        /// </summary>
        /// <param name="user"></param>
        /// <returns>True if the user exists, false otherwise</returns>
        public bool UserExists(string username)
        {
            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("select_user_by_name");
            cmd.Parameters.Add("@username", MySqlDbType.VarChar).Value = username;
            bool result = false;
            MySqlDataReader reader =  cmd.ExecuteReader();
            if (reader.Read())
            {
                result = true;
            }
            reader.Close();
            return result;
        }

        /// <summary>
        /// Delete a user from the DB
        /// </summary>
        /// <param name="user">The User Object which should be deleted</param>
        /// <returns>True on success, false otherwise</returns>
        /// <exception cref="NoSuchUserException"></exception>
        public bool DeleteUser(User user)
        {
            if (UserExists(user.Username) == false)
            {
                throw new NoSuchUserException("User " + user.Username + " does not exists!");
            }

            //Delete Tag references
            DeletePermissions(user);

            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("delete_user");
            cmd.Parameters.Add("@username", MySqlDbType.VarChar).Value = user.Username;
            if (cmd.ExecuteNonQuery() == 1)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Load a User object from the database
        /// </summary>
        /// <param name="userName">Username string</param>
        /// <returns>The loaded user or null if none could be found</returns>
        public User ReadUser(string userName)
        {
            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("select_user_by_name");
            cmd.Parameters.Add("@username", MySqlDbType.VarChar).Value = userName;
            MySqlDataReader reader = cmd.ExecuteReader();
            User dbUser = null;
            if (reader.Read())
            {
                dbUser = new User(reader.GetGuid("id"));                
                dbUser.Username = reader.GetString("username");
                dbUser.FirstName = reader.GetString("first_name");
                dbUser.LastName = reader.GetString("last_name");
                dbUser.Password = reader.GetString("password");
                dbUser.Salt = reader.GetString("salt");
                
            }
            reader.Close();               
            return dbUser;
        }

        /// <summary>
        /// Find the Guid of a permission tag in the database
        /// </summary>
        /// <param name="permission">The string to look up in the "tags" table</param>
        /// <returns>The Guid of the permission on success or Guid.Empty if no entry was found</returns>
        public Guid GetPermissionId(string permission)
        {
            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("select_tag");
            cmd.Parameters.Add("@name", MySqlDbType.VarChar).Value = permission;
            MySqlDataReader reader = cmd.ExecuteReader();
            Guid permissionGuid = Guid.Empty;
            if (reader.Read())
            {
                permissionGuid = reader.GetGuid("id");
            }
            reader.Close();
            return permissionGuid;
        }

        /// <summary>
        /// Check permissions for the user
        /// </summary>
        /// <param name="user">The user which should be checked for the specified permission</param>
        /// <param name="permission">The permission as a string</param>
        /// <returns>True if the user has the permission, false if not</returns>
        public bool HasPermission(User user, string permission)
        {
            Guid permissionGuid = GetPermissionId(permission);           
            if (permissionGuid == Guid.Empty)
            {
                return false;
            }
            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("select_user_tag_by_user_and_tag_id");
            cmd.Parameters.Add("@user_id", MySqlDbType.VarChar).Value = user.Id.ToString();
            cmd.Parameters.Add("@tag_id", MySqlDbType.VarChar).Value = permissionGuid.ToString();
            MySqlDataReader reader = cmd.ExecuteReader();
            bool result = false;
            if (reader.Read())
            {
                result = true;
            }
            reader.Close();
            return result;
        }
        /// <summary>
        /// Deleting permission from the user
        /// </summary>
        /// <param name="user">The user for which the permission should be removed</param>
        /// <param name="permission">The permission tag to be deleted</param>
        /// <returns>True on success, false on failure</returns>
        /// <exception cref="NoSuchPermissionException">thrown if the permission does not exist in the database</exception>
        public bool DeletePermission(User user, string permission)
        {
            Guid permissionGuid = GetPermissionId(permission);
            if(permissionGuid == Guid.Empty) 
            {
                throw new NoSuchPermissionException("Permission " + permission + " does not exist!");
            }

            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("delete_user_tag_by_user_and_tag_id");
            cmd.Parameters.Add("@user_id", MySqlDbType.VarChar).Value = user.Id.ToString();
            cmd.Parameters.Add("@tag_id", MySqlDbType.VarChar).Value = permissionGuid.ToString();
            if(cmd.ExecuteNonQuery() > 0)
            {
                return true;
            }
            return false;
           
        }

        /// <summary>
        /// Load a list of all permissions the user has
        /// </summary>
        /// <param name="entity">The user for which the permissions should be read from the db</param>
        /// <returns>A list of permission names, can be empty</returns>
        public List<string> GetPermissions(User entity)
        {
            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("select_tags_for_user_by_id");
            cmd.Parameters.Add("@user_id", MySqlDbType.VarChar).Value = entity.Id.ToString();
            MySqlDataReader reader = cmd.ExecuteReader();
            List<string> permissionList = new List<string>();
            while (reader.Read())
            {
                permissionList.Add(reader.GetString("tag_name"));
            }
            reader.Close();
            return permissionList;
            
        }
        /// <summary>
        /// Add a new permission for an user
        /// </summary>
        /// <param name="user">The user to which the permission should be added</param>
        /// <param name="permission">The permission tag name</param>
        /// <returns>True on success, false on error</returns>
        /// <exception cref="NoSuchUserException">thrown if the User does not exist in the database</exception>
        /// <exception cref="DuplicatePermissionException">thrown if the user already has the permission</exception>
        public bool AddPermission(User user, string permission)
        {
            MySqlCommand cmd = null;
            Guid permissionGuid = GetPermissionId(permission);           
            if (permissionGuid == Guid.Empty)
            {
                permissionGuid = Guid.NewGuid();
                cmd = DatabaseService.Instance.GetCommandForQuery("insert_tag");
                cmd.Parameters.Add("@id", MySqlDbType.VarChar).Value = permissionGuid.ToString();
                cmd.Parameters.Add("@name", MySqlDbType.VarChar).Value = permission;
                cmd.ExecuteNonQuery();               
            }            

            if (UserExists(user.Username) == false)
            {
                throw new NoSuchUserException("User " + user.Username + " doesn't exists!");
            }

            if (HasPermission(user, permission) == false)
            {                
                cmd = DatabaseService.Instance.GetCommandForQuery("insert_user_tag");
                cmd.Parameters.Add("@user_id", MySqlDbType.VarChar).Value = user.Id.ToString();
                cmd.Parameters.Add("@tag_id", MySqlDbType.VarChar).Value = permissionGuid.ToString();
                cmd.ExecuteNonQuery();
                return true;
            }
            else
            {
                throw new DuplicatePermissionException("User " + user.Username + " already has permission " + permission + "!");
            }
            
        }

        /// <summary>
        /// Remove all permissions from a user
        /// </summary>
        /// <param name="entity">The user which should be stripped of all permissions</param>
        public void DeletePermissions(User entity)
        {
            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("delete_user_tags_by_user_id");
            cmd.Parameters.Add("@user_id", MySqlDbType.VarChar).Value = entity.Id.ToString();
            cmd.ExecuteNonQuery(); 
        }
    }
}
