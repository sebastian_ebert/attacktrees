﻿// -----------------------------------------------------------------------
// <copypermission file="INodeStorage.cs" company="">
// TODO: Update copypermission text.
// </copypermission>
// -----------------------------------------------------------------------

namespace Persistence
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Model;
    using Model.Trees;

    /// <summary>
    /// Interface which provices functions to store and load permissions
    /// </summary>
    public interface IPermissionStorage<E>
    {
        bool HasPermission(E entity, string permission);

        bool DeletePermission(E entity, string permission);

        List<String> GetPermissions(E entity);

        bool AddPermission(E entity, string permission);

        void DeletePermissions(E entity);
    }
}
