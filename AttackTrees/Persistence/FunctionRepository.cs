﻿// -----------------------------------------------------------------------
// <copyright file="Class1.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Persistence
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Model;
    using MySql.Data.MySqlClient;
    using Model.Functions;
    using Model.Functions.Operators;

    /// <summary>
    /// FunctionRepository handles DB access.
    /// </summary>
    public class FunctionRepository : IFunctionRepository
    {
        /// <summary>
        /// Singleton instance
        /// </summary>
        private static readonly FunctionRepository _instance = new FunctionRepository();

        /// <summary>
        /// Gets the initialized and connected FunctionRepository Instance
        /// </summary>
        /// <exception>Can throw an InvalidDatabaseConfigException when the FunctionRepository is initialized but the config file is missing / invalid.</exception>
        public static FunctionRepository Instance
        {
            get
            {
                return _instance;
            }
        }

        /// <summary>
        /// Check if a function already exists in the database
        /// </summary>
        /// <param name="function">The function whose existence should be checked for</param>
        /// <returns>True if the function exists in the database, false if not</returns>
        public bool FunctionExists(Model.Functions.Function function)
        {
            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("select_function");
            cmd.Parameters.AddWithValue("@id", function.Id);
            MySqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            bool result = reader.HasRows;
            reader.Close();
            return result;
        }

        /// <summary>
        /// Save or update the specified function object in the database
        /// </summary>
        /// <param name="function">The function which should be inserted / updated</param>
        /// <returns>True on success or false if an error occurs</returns>
        /// <exception cref="NoSuchAttributeException">thrown if on of the functions referenced attributes does not exist in the database</exception>
        public bool SaveOrUpdate(Model.Functions.Function function)
        {
            MySqlTransaction transaction = DatabaseService.Instance.Connection.BeginTransaction();

            //Delete the function if it already exists
            if (FunctionExists(function) == true)
            {
                if (Delete(function) == false)
                {
                    transaction.Rollback();
                    return false;
                }
            }

            //Insert function
            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("insert_function");
            cmd.Parameters.AddWithValue("@id", function.Id);
            //cmd.Parameters.AddWithValue("@tree_id", Guid.Empty);
            cmd.Parameters.AddWithValue("@name", function.Name);
            if (cmd.ExecuteNonQuery() != 1)
            {
                transaction.Rollback();
                return false;
            }

            int order = 0;
            //Insert expressions
            foreach (Model.Functions.AttributeFunction expression in function.AttributeFunctions)
            {
                cmd = DatabaseService.Instance.GetCommandForQuery("insert_function_expression");
                cmd.Parameters.AddWithValue("@function_id", function.Id);

                cmd.Parameters.AddWithValue("@attribute_name", expression.AttributeName);
                cmd.Parameters.AddWithValue("@group_operator", expression.GroupOp);
                cmd.Parameters.AddWithValue("@attribute_operator", expression.AndOp);
                cmd.Parameters.AddWithValue("@select_operator", expression.SelectOp);
                cmd.Parameters.AddWithValue("@return_type", expression.ReturnType);
                cmd.Parameters.AddWithValue("@order_num", order++);

                if (cmd.ExecuteNonQuery() != 1)
                {
                    transaction.Rollback();
                    return false;
                }
            }
            //success
            transaction.Commit();
            return true;
        }

        /// <summary>
        /// Delete the function from the database also cascades to all expressions of which the function is composed
        /// </summary>
        /// <param name="function">The function to be deleted</param>
        /// <returns>True on success, false on error</returns>
        public bool Delete(Model.Functions.Function function)
        {
            //Delete expressions
            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("delete_function_expressions");
            cmd.Parameters.AddWithValue("@function_id", function.Id);            
            if (cmd.ExecuteNonQuery() == 0)
            {
                return false;
            }

            //Delete function
            cmd = DatabaseService.Instance.GetCommandForQuery("delete_function");
            cmd.Parameters.AddWithValue("@id", function.Id);
            if (cmd.ExecuteNonQuery() == 0)
            {
                return false;
            }

            //success
            return true;
        }

        /// <summary>
        /// Load the Function which has the specified Guid
        /// </summary>
        /// <param name="id">The Guid of the function to be loaded</param>
        /// <returns>The loaded function</returns>
        /// <exception cref="NoSuchFunctionException">thrown if there is no such function in the database</exception>
        public Model.Functions.Function GetById(Guid id)
        {
            //Load basic function information
            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("select_function");
            cmd.Parameters.AddWithValue("@id", id);
            MySqlDataReader reader = cmd.ExecuteReader();
            Model.Functions.Function function = null;
            if (reader.Read() == true)
            {
                function = new Model.Functions.Function(reader.GetString("name"));
                function.Id = reader.GetGuid("id");
            }
            reader.Close();
            if (function == null)
            {
                throw new NoSuchFunctionException("Function with Id " + id.ToString() + " does not exists in the database.");
            }

            //Load expressions
            function.AttributeFunctions.AddRange(GetExpressionsForFunction(function));
             

            return function;


        }

        /// <summary>
        /// Load all expressions which are attached to the supplied function
        /// </summary>
        /// <param name="function">The function whose subexpressions should be loaded</param>
        /// <returns>A list of all AttributeFunctions of which the function is composed, can be empty</returns>
        private IEnumerable<Model.Functions.AttributeFunction> GetExpressionsForFunction(Function function)
        {
            List<AttributeFunction> expressions = new List<AttributeFunction>();
            //Load expressions
            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("select_function_expressions");
            cmd.Parameters.AddWithValue("@function_id", function.Id);
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var attribute_operator = (AttributeOperators)Enum.Parse(typeof(AttributeOperators), reader.GetString("attribute_operator"));
                var select_operator = (SelectOperators)Enum.Parse(typeof(SelectOperators), reader.GetString("select_operator"));
                var group_operator = (GroupOperators)Enum.Parse(typeof(GroupOperators), reader.GetString("group_operator"));
                var return_type = (Types)Enum.Parse(typeof(Types), reader.GetString("return_type"));
                expressions.Add(new AttributeFunction(reader.GetString("attribute_name"), attribute_operator, group_operator, select_operator));
            }
            reader.Close();

            return expressions;

        }

        /// <summary>
        /// Find a function by it's unique name
        /// </summary>
        /// <param name="name">The function's name</param>
        /// <returns>The function object</returns>
        /// <exception cref="NoSuchFunctionException">thrown if no such function exists in the database</exception>
        public Model.Functions.Function FindByName(string name)
        {
            //Load basic function information
            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("select_function_by_name");
            cmd.Parameters.AddWithValue("@name", name);
            MySqlDataReader reader = cmd.ExecuteReader();
            Model.Functions.Function function = null;
            if (reader.Read() == true)
            {
                function = new Model.Functions.Function(reader.GetString("name"));
                function.Id = reader.GetGuid("id");
            }
            reader.Close();
            if (function == null)
            {
                throw new NoSuchFunctionException("Function with Name " + name + " does not exists in the database.");
            }

            //Load expressions
            function.AttributeFunctions.AddRange(GetExpressionsForFunction(function));


            return function;
        }

        /// <summary>
        /// Find all functions which use the specified attributes
        /// </summary>
        /// <param name="attributeNames">A list of attribute names</param>
        /// <returns>A list of Functions, can be empty</returns>
        public IEnumerable<Model.Functions.Function> FindByAttributes(IEnumerable<string> attributeNames)
        {
            Dictionary<Guid, Function> functions = new Dictionary<Guid, Function>();
            foreach (string attributeName in attributeNames)
            {
                MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("select_function_by_attribute_name");
                cmd.Parameters.AddWithValue("@name", attributeName);
                MySqlDataReader reader = cmd.ExecuteReader();
                Model.Functions.Function function = null;
                if (reader.Read() == true)
                {
                    function = new Model.Functions.Function(reader.GetString("name"));
                    function.Id = reader.GetGuid("function_id");
                }
                reader.Close();
                if (function == null)
                {
                    continue;
                }
                function.AttributeFunctions.AddRange(GetExpressionsForFunction(function));
                //Only add unique functions
                if (!functions.ContainsKey(function.Id))
                {
                    functions.Add(function.Id, function);
                }
                
            }

            return functions.Values.ToList();
        }

        /// <summary>
        /// Finds functions whose names match the supplied pattern. Use % wildcards.
        /// </summary>
        /// <param name="pattern">String pattern with % as a wildcard character</param>
        /// <returns>A possibly empty list of functions</returns>
        public List<Function> FindAllByName(string pattern)
        {
            //Load basic function information
            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("select_function_by_name_like");
            cmd.Parameters.AddWithValue("@name", pattern);
            MySqlDataReader reader = cmd.ExecuteReader();
          
            List<Function> functionList = new List<Function>();
            while(reader.Read())            
            {
                Model.Functions.Function  function = new Model.Functions.Function(reader.GetString("name"));
                function.Id = reader.GetGuid("id");
                functionList.Add(function);
            }
            reader.Close();

            foreach (Function function in functionList)
            {
                //Load expressions
                function.AttributeFunctions.AddRange(GetExpressionsForFunction(function));
            }
            return functionList;
        }
    }
}
