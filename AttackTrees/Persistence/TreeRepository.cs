﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Diagnostics;
using MySql.Data.MySqlClient;
using System.Data;
using System.IO;
using Model;
using Model.Trees;
using Model.Functions;




namespace Persistence
{
    /// <summary>
    /// TreeRepository handles DB access.
    /// </summary>
    public sealed class TreeRepository : ITreeRepository, INodeStorage, IEdgeStorage, IPermissionStorage<Tree>
    {
        /// <summary>
        /// Singleton instance
        /// </summary>
        private static readonly TreeRepository _instance = new TreeRepository();



        /// <summary>
        /// Gets the initialized and connected TreeRepository Instance
        /// </summary>
        /// <exception>Can throw an InvalidDatabaseConfigException when the TreeRepository is initialized but the config file is missing / invalid.</exception>
        public static TreeRepository Instance
        {
            get
            {
                return _instance;
            }
        }
        /// <summary>
        /// Inserts or updates a node in the database
        /// </summary>
        /// <param name="node"></param>
        /// <returns>Node on success and NULL on error</returns>
        public Model.Trees.Node SaveOrUpdateNode(Model.Trees.Node node)
        {
            Model.Trees.Node existingNode = GetNodeById(node.Id);
            int result;

            if (existingNode != null)
            {
                //Update
                MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("update_node");
                cmd.Parameters.AddWithValue("@name", node.Name);
                result = cmd.ExecuteNonQuery();
                if (result != 1)
                {
                    //Updated failed
                    return null;
                }

                //Detach all attributes
                DeleteAllAttributesFromNode(node);

                //Attach all attributes
                foreach (NodeAttribute attribute in node.Attributes.Values)
                {
                    if (AttributeExists(attribute) == false)
                    {
                        SaveOrUpdateAttribute(attribute);
                    }
                    AttachAttributeToNode(node, attribute);
                }




                return node;
            }
            else
            {
                MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("insert_node");
                //set new Guid if node doesnt have one
                if (node.Id == null)
                {
                    node.Id = Guid.NewGuid();
                }
                cmd.Parameters.AddWithValue("@id", node.Id.ToString());
                cmd.Parameters.AddWithValue("@name", node.Name);
                result = cmd.ExecuteNonQuery();
                if (result != 1)
                {
                    //Updated successfully
                    return null;
                }

                //Attach all attributes
                foreach (NodeAttribute attribute in node.Attributes.Values)
                {
                    NodeAttribute storedAttribute = ConstructAttributeByName(attribute.Name, attribute.Value);
                    if (storedAttribute == null)
                    {
                        storedAttribute = SaveOrUpdateAttribute(attribute);
                    }
                    AttachAttributeToNode(node, storedAttribute);
                }

                return node;

            }

        }

        /// <summary>
        /// Searches the "attributes" table for an attribute with the specified name string and returns it's GUID
        /// </summary>
        /// <param name="name">Name of the attribute</param>
        /// <returns>The Guid of the attribute or null if none could be found</returns>
        public Guid GetAttributeGuidByName(String name)
        {
            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("select_attribute_by_name");
            cmd.Parameters.AddWithValue("@name", name);
            MySqlDataReader reader = cmd.ExecuteReader();
            var id = Guid.Empty;
            if (reader.Read())
            {
                id = reader.GetGuid("id");
            }
            reader.Close();
            return id;
        }

        /// <summary>
        /// Searches the "attributes" table for an attribute with the specified GUID and returns it's name
        /// </summary>
        /// <param name="id">The Guid of the attribute</param>
        /// <returns>The name of the attribute or null if none has been found</returns>
        public string GetAttributeName(Guid id)
        {
            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("select_attribute_by_id");
            cmd.Parameters.AddWithValue("@id", id);
            MySqlDataReader reader = cmd.ExecuteReader();
            string name = null;
            if (reader.Read())
            {
                name = reader.GetString("name");
            }
            reader.Close();
            return name;
        }

        /// <summary>
        /// Generates a NodeAttribute object from an attribute name and a string value. Only works for attributes which already exist in den database.
        /// </summary>
        /// <param name="name">Name of the attribute as a string</param>
        /// <param name="value">Value of the attribute as a string</param>
        /// <returns>Returns the created NodeAttribute object</returns>
        private NodeAttribute ConstructAttributeByName(String name, String value)
        {
            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("select_attribute_by_name");
            cmd.Parameters.AddWithValue("@name", name);
            NodeAttribute attribute = null;
            MySqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                var typeName = reader.GetString("type");
                var type = (Types)Enum.Parse(typeof(Types), typeName);
                attribute = new NodeAttribute(reader.GetGuid("id"), reader.GetString("name"), type, value);
            }
            reader.Close();
            return attribute;
        }

        /// <summary>
        /// Removes all attributes from the specified Node
        /// </summary>
        /// <param name="node">Node which should be stripped of it's attributes</param>
        private void DeleteAllAttributesFromNode(Node node)
        {
            //Proxy the call
            DeleteAllAttributesFromNode(node.Id);
        }

        /// <summary>
        /// Removes all attributes from the node with the specified GUID
        /// </summary>
        /// <param name="node">Guid of the node which should be stripped of all it's attributes</param>
        private void DeleteAllAttributesFromNode(Guid nodeId)
        {
            //Detach all attributes
            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("delete_all_attributes_from_node");
            cmd.Parameters.AddWithValue("@node_id", nodeId);
            cmd.ExecuteNonQuery();
        }

        /// <summary>
        /// Check if an attribute already exists in the database
        /// </summary>
        /// <param name="attribute">The attribute which existence should be tested</param>
        /// <returns>True if the attribute exists in the "attributes" table, false if not</returns>
        private bool AttributeExists(NodeAttribute attribute)
        {
            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("select_attribute_by_name");
            cmd.Parameters.AddWithValue("@name", attribute.Name);
            MySqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            bool result = reader.HasRows;
            reader.Close();
            return result;
        }

        /// <summary>
        /// Check if the specified node has the specified attribute
        /// </summary>
        /// <param name="node">The node to be tested</param>
        /// <param name="attribute">The attribute which existence should be tested</param>
        /// <returns>True if the Node has the specified Attribute, false if not</returns>
        public bool NodeHasAttribute(Node node, NodeAttribute attribute)
        {
            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("select_node_has_attribute");
            cmd.Parameters.AddWithValue("@node_id", node.Id.ToString());
            cmd.Parameters.AddWithValue("@attribute_id", attribute.Id.ToString());
            MySqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            bool result = reader.HasRows;
            reader.Close();
            return result;
        }

        /// <summary>
        /// Attaches an Attribute to a Node (creates a connection in the node_attributes table)
        /// </summary>
        /// <param name="node">The Node to which an Attribute should be attached</param>
        /// <param name="attribute">The Attribute to be attached</param>
        /// <returns>True on Success or False if the Attribute is already there</returns>
        public bool AttachAttributeToNode(Node node, NodeAttribute attribute)
        {
            if (NodeHasAttribute(node, attribute) == true)
            {
                return false;
            }

            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("insert_attribute_for_node");
            cmd.Parameters.AddWithValue("@attribute_id", attribute.Id.ToString());
            cmd.Parameters.AddWithValue("@node_id", node.Id.ToString());
            cmd.Parameters.AddWithValue("@value", attribute.Value);
            if (cmd.ExecuteNonQuery() != 1)
            {
                //Failed
                return false;
            }
            return true;

        }

        /// <summary>
        /// Save the attribute to the database
        /// </summary>
        /// <param name="attribute">Attribute to be stored for later usage in the EAV pattern</param>
        /// <returns>The supplied NodeAttribute or null on error</returns>
        /// <exception cref="DuplicateAttributeException">thrown if the Attribute already exists in the database</exception>
        public NodeAttribute SaveOrUpdateAttribute(NodeAttribute attribute)
        {
            if (AttributeExists(attribute) == true)
            {
                throw new DuplicateAttributeException("Attribute " + attribute.ToString() + " already exists!");
            }
            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("insert_attribute");
            Console.WriteLine(attribute.Id.ToString());
            cmd.Parameters.AddWithValue("@id", attribute.Id.ToString());
            cmd.Parameters.AddWithValue("@name", attribute.Name);
            cmd.Parameters.AddWithValue("@type", attribute.Type);
            if (cmd.ExecuteNonQuery() == 0)
            {
                //Failed
                return null;
            }
            return attribute;
        }

        /// <summary>
        /// Check if a tree already exists in the DB
        /// </summary>
        /// <param name="id"></param>
        /// <returns>True if it exists / false if not</returns>
        public bool TreeExists(Guid id)
        {
            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("select_tree_by_id");
            cmd.Parameters.Add("@id", MySqlDbType.VarChar).Value = id.ToString();
            MySqlDataReader reader = cmd.ExecuteReader();
            bool treeExists = reader.HasRows;
            reader.Close();
            return treeExists;
        }

        /// <summary>
        /// Loads a node from the database (by id)
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Model.Trees.Node node on success, NULL if the node with the specified id could not be found</returns>
        public Model.Trees.Node GetNodeById(Guid id)
        {
            if (id == null)
            {
                return null;
            }
            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("select_node_by_id");
            cmd.Parameters.Add("@id", MySqlDbType.VarChar).Value = id.ToString();
            MySqlDataReader reader = cmd.ExecuteReader();
            Model.Trees.Node node = null;
            if (reader.Read())
            {
                node = new Model.Trees.Node(reader.GetString("name"), reader.GetGuid("id"));

            }
            reader.Close();
            return node;
        }

        /// <summary>
        /// Stores a tree in the DB
        /// </summary>
        /// <param name="tree"></param>
        /// <returns>True on success, false otherwise</returns>
        public bool SaveOrUpdate(Model.Trees.Tree tree)
        {
            int result;

            MySqlTransaction transaction = DatabaseService.Instance.Connection.BeginTransaction();
            if (TreeExists(tree.Id) == true)
            {
                if (Delete(tree) == false)
                {
                    transaction.Rollback();
                    return false;
                }
            }

            // Insert nodes first (because of FK constraints to the root node)
            // 
            foreach (Node node in tree.Vertices)
            {
                if (SaveOrUpdateNode(node) == null)
                {
                    Console.WriteLine("Inserting node failed...");
                    transaction.Rollback();
                    return false;
                }
            }

            // Insert self ref row for the root node so everything else works smoothely
            // 

            if (SaveOrUpdateEdge(new Model.Trees.Edge(tree.RootNode, tree.RootNode)) == false)
            {
                Console.WriteLine("Inserting root node self ref edge failed...");
                transaction.Rollback();
                return false;
            }

            // Do BFT to insert he edges in the right order
            // 

            Queue<Model.Trees.Node> nodeQueue = new Queue<Model.Trees.Node>();
            Node rootNode = tree.RootNode;
            nodeQueue.Enqueue(rootNode);

            Model.Trees.Node currentNode;
            HashSet<Model.Trees.Node> visitedNodes = new HashSet<Model.Trees.Node>();
            IEnumerable<Model.Trees.Node> currentChildren = new List<Model.Trees.Node>();
            while (nodeQueue.Count > 0)
            {
                currentNode = nodeQueue.Dequeue();
                currentChildren = tree.FindChildren(currentNode);

                IEnumerator<Model.Trees.Node> enumerator = currentChildren.GetEnumerator();
                enumerator.Reset();
                while (enumerator.MoveNext())
                {
                    if (!visitedNodes.Contains(enumerator.Current))
                    {
                        nodeQueue.Enqueue(enumerator.Current);
                        Model.Trees.Edge tmpEdge;
                        tree.TryGetEdge(currentNode, enumerator.Current, out tmpEdge);
                        if (SaveOrUpdateEdge(tmpEdge) == false)
                        {
                            Console.WriteLine("Inserting edge failed...");
                            transaction.Rollback();
                            return false;
                        }
                    }
                }

            }

            // Store AND Groups
            //

            foreach (Edge edge in tree.Edges)
            {
                if (edge.IsAnd)
                {
                    if (UpdateEdgeAndGroup(edge, edge.AndGroup) == false)
                    {
                        Console.WriteLine("Updating and groups failed...");
                        transaction.Rollback();
                        return false;
                    }
                }
            }




            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("insert_tree");
            cmd.Parameters.AddWithValue("@id", tree.Id);
            cmd.Parameters.AddWithValue("@name", tree.Name);
            cmd.Parameters.AddWithValue("@description", tree.Description);
            cmd.Parameters.AddWithValue("@root_node_id", tree.RootNode.Id);

            result = cmd.ExecuteNonQuery();
            if (result == 1)
            {
                Console.WriteLine("Inserting tree successful...");
                transaction.Commit();
                return true;
            }




            Console.WriteLine("Inserting tree failed... result: " + result);
            transaction.Rollback();
            return false;
        }

        /// <summary>
        /// Delete a whole tree
        /// </summary>
        /// <param name="tree"></param>
        /// <returns>True on success, False otherwise</returns>
        public bool Delete(Model.Trees.Tree tree)
        {

            if (tree.RootNode == null)
            {
                return false;
            }

            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("select_nodes_from_tree");
            cmd.Parameters.Add("@tree_id", MySqlDbType.VarChar).Value = tree.Id.ToString();
            MySqlDataReader reader = cmd.ExecuteReader();
            List<Guid> nodeIds = new List<Guid>();

            while (reader.Read())
            {
                nodeIds.Add(reader.GetGuid("descendant"));
            }

            reader.Close();
            /// Delete edges
            /// 

            cmd = DatabaseService.Instance.GetCommandForQuery("delete_edges_by_root_node_id");
            cmd.Parameters.Add("@root_node_id", MySqlDbType.VarChar).Value = tree.RootNode.Id;
            if (cmd.ExecuteNonQuery() == 0)
            {
                return false;
            }

            //Delete permissions
            DeletePermissions(tree);

            /// Delete Tree
            /// 
            cmd = DatabaseService.Instance.GetCommandForQuery("delete_tree");
            cmd.Parameters.Add("@id", MySqlDbType.VarChar).Value = tree.Id.ToString();
            if (cmd.ExecuteNonQuery() == 0)
            {
                return false;
            }

            /// Delete nodes
            /// 
            cmd = DatabaseService.Instance.GetCommandForQuery("delete_node");
            cmd.Parameters.Add("@id", MySqlDbType.VarChar);
            foreach (Guid nodeId in nodeIds)
            {
                //Detach all attributes
                DeleteAllAttributesFromNode(nodeId);

                cmd.Parameters["@id"].Value = nodeId.ToString();
                cmd.ExecuteNonQuery();
            }

            return true;

        }

        /// <summary>
        /// Load a tree by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns>The Tree from the DB or null on error</returns>
        public Model.Trees.Tree GetById(Guid id)
        {
            if (id == null)
            {
                return null;
            }
            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("select_tree_by_id");
            cmd.Parameters.Add("@id", MySqlDbType.VarChar).Value = id.ToString();
            MySqlDataReader reader = cmd.ExecuteReader();
            Model.Trees.Tree tree = null;
            if (reader.Read())
            {
                tree = new Model.Trees.Tree(reader.GetGuid("id"));
                tree.Name = reader.GetString("name");
                if (!reader.IsDBNull(3)) //check if desc is null
                {
                    tree.Description = reader.GetString("description");
                }
                else
                {
                    tree.Description = "";

                }
            }
            reader.Close();
            /// Load the nodes
            /// 
            cmd = DatabaseService.Instance.GetCommandForQuery("select_nodes_from_tree");
            cmd.Parameters.Add("@tree_id", MySqlDbType.VarChar).Value = id.ToString();
            reader = cmd.ExecuteReader();

            Dictionary<Guid, Model.Trees.Node> nodeDictionary = new Dictionary<Guid, Model.Trees.Node>();

            while (reader.Read())
            {
                if (tree.RootNode == null)
                {
                    Model.Trees.Node rootNode = new Model.Trees.Node(reader.GetString("desc_name"), reader.GetGuid("descendant"));
                    tree.AddNode(rootNode, null);
                    nodeDictionary.Add(rootNode.Id, rootNode);
                    continue;
                }
                /// Add the rest
                /// 
                Guid ancestorId = reader.GetGuid("ancestor");
                Guid descendantId = reader.GetGuid("descendant");
                Model.Trees.Node sourceNode;
                Model.Trees.Node targetNode;

                if (!nodeDictionary.TryGetValue(ancestorId, out sourceNode))
                {
                    sourceNode = new Model.Trees.Node(reader.GetString("anc_name"), ancestorId);
                    nodeDictionary.Add(sourceNode.Id, sourceNode);
                }
                if (!nodeDictionary.TryGetValue(descendantId, out targetNode))
                {
                    targetNode = new Model.Trees.Node(reader.GetString("desc_name"), descendantId);
                    nodeDictionary.Add(targetNode.Id, targetNode);
                }

                tree.AddNode(targetNode, sourceNode);




                if (!reader.IsDBNull(2))
                {
                    String andGroup = reader.GetString("and_group");
                    Model.Trees.Edge andEdge;
                    if (tree.TryGetEdge(sourceNode, targetNode, out andEdge))
                    {
                        andEdge.AndGroup = Guid.Parse(andGroup);
                    }
                }
            }

            reader.Close();

            //Load Attribute for leaf nodes
            foreach (Node node in tree.Leafs)
            {
                cmd = DatabaseService.Instance.GetCommandForQuery("select_attributes_from_node");
                cmd.Parameters.AddWithValue("@node_id", node.Id.ToString());
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var typeName = reader.GetString("type");
                    var type = (Types)Enum.Parse(typeof(Types), typeName);
                    NodeAttribute attribute = new NodeAttribute(reader.GetGuid("id"), reader.GetString("name"), type, reader.GetString("value"));
                    node.AddAttribute(attribute);
                }
                reader.Close();

            }

            //Hack for populating the DefaultAttributes field of the tree
            foreach (Node node in tree.Vertices)
            {
                foreach(KeyValuePair<string, NodeAttribute> attributeKV in node.Attributes)
                {
                    if (!tree.DefaultAttributes.ContainsKey(attributeKV.Key))
                    {
                        NodeAttribute tempAttribute = attributeKV.Value;
                        switch (attributeKV.Value.Type)
                        {
                            case Types.Boolean:
                                tempAttribute.SetValue(false);
                                break;
                            case Types.Number:
                                tempAttribute.SetValue(0);
                                break;
                            default:
                                tempAttribute.SetValue(0);
                                break;
                        }
                        tree.DefaultAttributes.Add(attributeKV.Key, tempAttribute);
                    }
                }
            }
            //Verify consitency of attributes
            tree.UpdateAttributes();
            return tree;
        }

        /// <summary>
        /// Find a tree by an exact match
        /// </summary>
        /// <param name="pattern">The pattern to look for</param>
        /// <returns>The found tree or null on error</returns>
        public Model.Trees.Tree FindByName(string pattern)
        {
            //Load basic function information
            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("select_tree_by_name");
            cmd.Parameters.AddWithValue("@name", pattern);
            MySqlDataReader reader = cmd.ExecuteReader();
            Model.Trees.Tree tree = null;
            Guid treeGuid = Guid.Empty;
            if (reader.Read() == true)
            {
                treeGuid = reader.GetGuid("id");
            }
            reader.Close();            
            if (treeGuid == Guid.Empty) 
            {
                throw new NoSuchTreeException("Tree with Name " + pattern + " does not exists in the database.");
            }

            tree = GetById(treeGuid);
            return tree;
        }


        /// <summary>
        /// Store an edge in the DB
        /// </summary>
        /// <param name="e"></param>
        /// <returns>True on success, false otherwise</returns>
        public bool SaveOrUpdateEdge(Model.Trees.Edge e)
        {
            if (DoesEdgeExist(e) == true)
            {
                return true;
            }
            else
            {
                MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("insert_edge");
                //set new Guid if node doesnt have one               
                cmd.Parameters.AddWithValue("@node_id", e.Target.Id);
                cmd.Parameters.AddWithValue("@descendant", e.Source.Id);
                cmd.Parameters.AddWithValue("@edge_type", "ref");
                int result = cmd.ExecuteNonQuery();
                if (result > 0)
                {
                    //Updated successfully
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Load an edge by ancestore and descendant
        /// </summary>
        /// <param name="ancestor"></param>
        /// <param name="descendant"></param>
        /// <returns>Returns an Edge object or null on error</returns>
        public Model.Trees.Edge GetEdge(Model.Trees.Node ancestor, Model.Trees.Node descendant)
        {
            if (ancestor == null || descendant == null)
            {
                return null;
            }
            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("select_edge_by_ancestor_and_descendant");
            cmd.Parameters.Add("@ancestor", MySqlDbType.VarChar).Value = ancestor.Id;
            cmd.Parameters.Add("@descendant", MySqlDbType.VarChar).Value = descendant.Id;
            MySqlDataReader reader = cmd.ExecuteReader();
            Model.Trees.Edge edge = null;
            if (reader.Read())
            {
                Model.Trees.Node source = GetNodeById(reader.GetGuid("ancestor"));
                Model.Trees.Node target = GetNodeById(reader.GetGuid("descendant"));
                edge = new Model.Trees.Edge(source, target);
            }
            reader.Close();
            return edge;
        }

        /// <summary>
        /// Checks if an edge exists in the db
        /// </summary>
        /// <param name="e"></param>
        /// <returns>True if the edge exists, False if not</returns>
        public bool DoesEdgeExist(Model.Trees.Edge e)
        {
            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("select_edge_by_ancestor_and_descendant");
            cmd.Parameters.Add("@ancestor", MySqlDbType.VarChar).Value = e.Source.Id;
            cmd.Parameters.Add("@descendant", MySqlDbType.VarChar).Value = e.Target.Id;
            MySqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            bool edgeExists = reader.HasRows;
            reader.Close();
            return edgeExists;
        }

        /// <summary>
        /// Updates the AND Group ID of an Edge
        /// </summary>
        /// <param name="e"></param>
        /// <param name="andGroupId"></param>
        /// <returns>True on success, False on error</returns>
        public bool UpdateEdgeAndGroup(Model.Trees.Edge e, Guid andGroupId)
        {
            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("update_edge_and_group");
            //set new Guid if node doesnt have one               
            cmd.Parameters.AddWithValue("@ancestor", e.Source.Id);
            cmd.Parameters.AddWithValue("@descendant", e.Target.Id);
            cmd.Parameters.AddWithValue("@and_group", andGroupId);

            int result = cmd.ExecuteNonQuery();
            if (result == 1)
            {
                //Updated successfully
                return true;
            }

            return false;
        }




        /// <summary>
        /// Find the Guid of a permission tag in the database
        /// </summary>
        /// <param name="permission">The string to look up in the "tags" table</param>
        /// <returns>The Guid of the permission on success or Guid.Empty if no entry was found</returns>
        public Guid GetPermissionId(string permission)
        {
            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("select_tag");
            cmd.Parameters.Add("@name", MySqlDbType.VarChar).Value = permission;
            MySqlDataReader reader = cmd.ExecuteReader();
            Guid permissionGuid = Guid.Empty;
            if (reader.Read())
            {
                permissionGuid = reader.GetGuid("id");
            }
            reader.Close();
            return permissionGuid;
        }

        /// <summary>
        /// Checks if a permission tag has been attached to a tree
        /// </summary>
        /// <param name="tree">The Tree which should be checked</param>
        /// <param name="permission">The permission which the tree should be checked for</param>
        /// <returns>True if the tag was attached to the tree or false if not</returns>
        public bool HasPermission(Tree tree, string permission)
        {
            Guid permissionGuid = GetPermissionId(permission);
            if (permissionGuid == Guid.Empty)
            {
                return false;
            }
            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("select_tree_tag_by_tree_and_tag_id");
            cmd.Parameters.Add("@tree_id", MySqlDbType.VarChar).Value = tree.Id.ToString();
            cmd.Parameters.Add("@tag_id", MySqlDbType.VarChar).Value = permissionGuid.ToString();
            MySqlDataReader reader = cmd.ExecuteReader();
            bool result = false;
            if (reader.Read())
            {
                result = true;
            }
            reader.Close();
            return result;
        }

        /// <summary>
        /// Delete a permission tag from a tree
        /// </summary>
        /// <param name="tree">The tree from which the permission should be removed</param>
        /// <param name="permission">The permission which should be removed</param>
        /// <returns>True on success, false on failure</returns>
        /// <exception cref="NoSuchPermissionException">thrown if there is no such permission in the database</exception>
        public bool DeletePermission(Tree tree, string permission)
        {
            Guid permissionGuid = GetPermissionId(permission);
            if (permissionGuid == Guid.Empty)
            {
                throw new NoSuchPermissionException("Permission " + permission + " does not exist!");
            }

            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("delete_tree_tag_by_tree_and_tag_id");
            cmd.Parameters.Add("@tree_id", MySqlDbType.VarChar).Value = tree.Id.ToString();
            cmd.Parameters.Add("@tag_id", MySqlDbType.VarChar).Value = permissionGuid.ToString();
            if (cmd.ExecuteNonQuery() > 0)
            {
                return true;
            }
            return false;

        }

        /// <summary>
        /// Loads a list of all permission which are attached to a tree. May be used to do an intersection with the permissions of a user. 
        /// </summary>
        /// <param name="entity">The Tree which permission tags should be loaded</param>
        /// <returns>A list of strings, can be empty if no permissions were found</returns>
        public List<string> GetPermissions(Tree entity)
        {
            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("select_tags_for_tree_by_id");
            cmd.Parameters.Add("@tree_id", MySqlDbType.VarChar).Value = entity.Id.ToString();
            MySqlDataReader reader = cmd.ExecuteReader();
            List<string> permissionList = new List<string>();
            while (reader.Read())
            {
                permissionList.Add(reader.GetString("tag_name"));
            }
            reader.Close();
            return permissionList;

        }

        /// <summary>
        /// Add a new permission to a tree. If there is no existing entry in the tags table, one is created.
        /// </summary>
        /// <param name="tree">The tree to which the new permission tag should be added</param>
        /// <param name="permission">The permission tag to be added</param>
        /// <returns>True on success, false on error</returns>
        /// <exception cref="NoSuchTreeException">thrown if the tree does not exist in the database</exception>
        /// <exception cref="DuplicatePermissionException">thrown if the permission is already attached to the tree</exception>
        public bool AddPermission(Tree tree, string permission)
        {
            MySqlCommand cmd = null;
            Guid permissionGuid = GetPermissionId(permission);
            if (permissionGuid == Guid.Empty)
            {
                permissionGuid = Guid.NewGuid();
                cmd = DatabaseService.Instance.GetCommandForQuery("insert_tag");
                cmd.Parameters.Add("@id", MySqlDbType.VarChar).Value = permissionGuid.ToString();
                cmd.Parameters.Add("@name", MySqlDbType.VarChar).Value = permission;
                cmd.ExecuteNonQuery();
            }

            if (TreeExists(tree.Id) == false)
            {
                throw new NoSuchTreeException("Tree " + tree.Id.ToString() + " doesn't exists!");
            }

            if (HasPermission(tree, permission) == false)
            {
                cmd = DatabaseService.Instance.GetCommandForQuery("insert_tree_tag");
                cmd.Parameters.Add("@tree_id", MySqlDbType.VarChar).Value = tree.Id.ToString();
                cmd.Parameters.Add("@tag_id", MySqlDbType.VarChar).Value = permissionGuid.ToString();
                cmd.ExecuteNonQuery();
                return true;
            }
            else
            {
                throw new DuplicatePermissionException("User " + tree.Id.ToString() + " already has permission " + permission + "!");
            }

        }

        /// <summary>
        /// Load all available Trees from the database
        /// </summary>
        /// <returns>A possibly empty list of trees</returns>
        public List<Tree> GetAllTrees()
        {
            List<Tree> allTrees = new List<Tree>();
            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("select_all_trees");
            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                Tree tempTree = new Tree();
                tempTree.Id = Guid.Parse(reader.GetString("id"));
                tempTree.Name = reader.GetString("name");
                if (!reader.IsDBNull(3)) //check if desc is null
                {
                    tempTree.Description = reader.GetString("description");
                }
                else
                {
                    tempTree.Description = "";

                }
                allTrees.Add(tempTree);
            }
            reader.Close();
            return allTrees;
        }

        /// <summary>
        /// Deletes a permissions from the specified tree
        /// </summary>
        /// <param name="entity">The tree which should be stripped of all permissions</param>
        public void DeletePermissions(Tree entity)
        {
            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("delete_tree_tags_by_tree_id");
            cmd.Parameters.Add("@tree_id", MySqlDbType.VarChar).Value = entity.Id.ToString();
            cmd.ExecuteNonQuery();
        }

        /// <summary>
        /// Finds trees whose names match the supplied pattern. Use % wildcards.
        /// </summary>
        /// <param name="pattern">String pattern with % as a wildcard character</param>
        /// <returns>A possibly empty list of trees</returns>
        public List<Tree> FindAllByName(string pattern)
        {
            //Load basic function information
            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("select_tree_by_name_like");
            cmd.Parameters.Add(new MySqlParameter("@name", pattern));
            MySqlDataReader reader = cmd.ExecuteReader();
            List<Tree> treeList = new List<Tree>();
            List<Guid> treeGuidList = new List<Guid>();
            while (reader.Read())
            {                
                treeGuidList.Add(reader.GetGuid("id"));
            }
           
            reader.Close();
            foreach (Guid treeGuid in treeGuidList)
            {
                Tree tree = GetById(treeGuid);
                treeList.Add(tree);
            }
            
            return treeList;
        }

        /// <summary>
        /// Searches the database for trees which contain a node with the specified name
        /// </summary>
        /// <param name="pattern">The pattern to match the node name with</param>
        /// <returns>A list of all trees containing the nodes, can be empty</returns>
        public List<Tree> FindAllByNodeName(string pattern)
        {
            //Load basic function information
            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("select_tree_by_node_name_like");
            cmd.Parameters.Add(new MySqlParameter("@name", pattern));
            MySqlDataReader reader = cmd.ExecuteReader();
            List<Tree> treeList = new List<Tree>();
            List<Guid> treeGuidList = new List<Guid>();
            while (reader.Read())
            {                
                treeGuidList.Add(reader.GetGuid("id"));
            }

            reader.Close();
            foreach (Guid treeGuid in treeGuidList)
            {
                Tree tree = GetById(treeGuid);
                treeList.Add(tree);
            }

            return treeList;
        }

        /// <summary>
        /// Searches the database for trees that contain a node which has an attribute with the specified name
        /// </summary>
        /// <param name="pattern">The pattern to match the attribute name with</param>
        /// <returns>A list of all matching trees, can be empty</returns>
        public List<Tree> FindAllByAttributeName(string pattern)
        {
            //Load basic function information
            MySqlCommand cmd = DatabaseService.Instance.GetCommandForQuery("select_tree_by_attribute_name_like");
            cmd.Parameters.Add(new MySqlParameter("@name", pattern));
            MySqlDataReader reader = cmd.ExecuteReader();
            List<Tree> treeList = new List<Tree>();
            List<Guid> treeGuidList = new List<Guid>();
            while (reader.Read())
            {
                treeGuidList.Add(reader.GetGuid("id"));
            }

            reader.Close();
            foreach (Guid treeGuid in treeGuidList)
            {
                Tree tree = GetById(treeGuid);
                treeList.Add(tree);
            }
            return treeList;
        }
    }
}
