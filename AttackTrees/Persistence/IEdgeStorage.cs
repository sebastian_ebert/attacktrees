﻿// -----------------------------------------------------------------------
// <copyright file="INodeStorage.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Persistence
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Interface which provides functions to store / load edges
    /// </summary>
    public interface IEdgeStorage
    {

        bool DoesEdgeExist(Model.Trees.Edge e);

        bool SaveOrUpdateEdge(Model.Trees.Edge e);

        bool UpdateEdgeAndGroup(Model.Trees.Edge e, Guid andGroupId);

        Model.Trees.Edge GetEdge(Model.Trees.Node ancestor, Model.Trees.Node descendant);

    }
}
