﻿// -----------------------------------------------------------------------
// <copyright file="InvalidDatabaseConfigException.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Persistence
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Runtime.Serialization;

    /// <summary>
    /// Exception which is thrown when the system tries to create an attribute that already exists
    /// </summary>
    public class DuplicateAttributeException : Exception, ISerializable
    {
        public DuplicateAttributeException()
        {
        }

        public DuplicateAttributeException(string message)
            : base(message)
        {           
        }

    }
}
