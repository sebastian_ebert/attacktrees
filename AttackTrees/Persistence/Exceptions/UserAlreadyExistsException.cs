﻿// -----------------------------------------------------------------------
// <copyright file="InvalidDatabaseConfigException.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Persistence
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Runtime.Serialization;

    /// <summary>
    /// Exception which is thrown when the system tries to save a user that already exists in the DB
    /// </summary>
    public class UserAlreadyExistsException : Exception, ISerializable
    {
        public UserAlreadyExistsException()
        {
        }

        public UserAlreadyExistsException(string message)
            : base(message)
        {           
        }

    }
}
