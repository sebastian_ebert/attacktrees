﻿// -----------------------------------------------------------------------
// <copyright file="InvalidDatabaseConfigException.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Persistence
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Runtime.Serialization;

    /// <summary>
    /// Exception which is thrown when there is no or an invalid database configuration file.
    /// </summary>
    public class InvalidDatabaseConfigException : Exception, ISerializable
    {
        public InvalidDatabaseConfigException()
        {
        }

        public InvalidDatabaseConfigException(string message)
            : base(message)
        {           
        }

    }
}
