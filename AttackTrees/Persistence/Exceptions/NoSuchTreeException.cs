﻿// -----------------------------------------------------------------------
// <copyright file="InvalidDatabaseConfigException.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Persistence
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Runtime.Serialization;

    /// <summary>
    /// Exception which is thrown when a requested tree does not exist in the database
    /// </summary>
    public class NoSuchTreeException : Exception, ISerializable
    {
        public NoSuchTreeException()
        {
        }

        public NoSuchTreeException(string message)
            : base(message)
        {           
        }

    }
}
