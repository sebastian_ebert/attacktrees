﻿// -----------------------------------------------------------------------
// <copyright file="InvalidDatabaseConfigException.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Persistence
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Runtime.Serialization;

    /// <summary>
    /// Exception which is thrown when the system tries to attach a permission that already exists
    /// </summary>
    public class DuplicatePermissionException : Exception, ISerializable
    {
        public DuplicatePermissionException()
        {
        }

        public DuplicatePermissionException(string message)
            : base(message)
        {           
        }

    }
}
