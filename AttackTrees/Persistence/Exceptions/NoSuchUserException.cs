﻿// -----------------------------------------------------------------------
// <copyright file="InvalidDatabaseConfigException.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Persistence
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Runtime.Serialization;

    /// <summary>
    /// Exception which is thrown when a requested user does not exist in the database
    /// </summary>
    public class NoSuchUserException : Exception, ISerializable
    {
        public NoSuchUserException()
        {
        }

        public NoSuchUserException(string message)
            : base(message)
        {           
        }

    }
}
