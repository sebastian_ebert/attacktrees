﻿using System;
using System.Collections.Generic;
using System.Linq;
using Model;
using NUnit.Framework;

namespace ModelTests
{
    /// <summary>
    /// Test cases for class <see cref="AttackTrees.Model.User"/>.
    /// </summary>
    [TestFixture]
    public class UserTests
    {

        [Test]
        public void NewUserHasNoRights()
        {
            var user = new User();
            Assert.AreEqual(0,user.Rights.Count);

        }

        [Test]
        public void AddRightsToUser()
        {
            var user = new User();
            user.AddRight("addtestright");
            Assert.IsTrue(user.HasRightsFor("addtestright"));
        }

        [Test]
        public void RemoveRightsFromUser()
        {
            var user = new User();
            user.AddRight("addtestright");
            user.AddRight("removetestright");
            user.RemoveRight("addtestright");
            Assert.IsFalse(user.HasRightsFor("addtestright"));
        }

    }

}
