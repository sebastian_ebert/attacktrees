﻿using System;
using Model.Trees;
using NUnit.Framework;

namespace ModelTests
{
    [TestFixture]
    public class EdgeTests
    {

        [Test]
        public void CanCreateEdge()
        {
            var source = new Node("foo");
            var target = new Node("bar");
            new Edge(source, target);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NewEdgeNeedsValidSourceNode()
        {
            var target = new Node("foo");
            new Edge(null, target);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NewEdgeNeedsValidTargetNode()
        {
            var source = new Node("foo");
            new Edge(source, null);
        }

        [Test]
        public void NewEdgeIsOrByDefault()
        {
            var source = new Node("foo");
            var target = new Node("bar");
            var edge = new Edge(source, target);
            Assert.IsFalse(edge.IsAnd);
            Assert.AreEqual(Guid.Empty, edge.AndGroup);
        }

        [Test]
        public void NewEdgeCanHaveAndGroup()
        {
            var source = new Node("foo");
            var target = new Node("bar");
            var andGroup = Guid.NewGuid();
            var edge = new Edge(source, target, andGroup);
            Assert.IsTrue(edge.IsAnd);
            Assert.AreEqual(andGroup, edge.AndGroup);
        }

        [Test]
        public void EdgesWithSameSourceAndTargetAreEqual()
        {
            var source = new Node("foo");
            var target = new Node("bar");
            var target2 = new Node("baz");
            var andGroup = Guid.NewGuid();
            var edge1 = new Edge(source, target);
            var edge2 = new Edge(source, target);
            var edge3 = new Edge(source, target, andGroup);
            var edge4 = new Edge(source, target2);
            Assert.AreEqual(edge1, edge2);
            Assert.AreEqual(edge1, edge3);
            Assert.AreNotEqual(edge1, edge4);
        }

        [Test]
        public void HashCodeMatchesForEqualEdges()
        {
            var source = new Node("foo");
            var target = new Node("bar");
            var target2 = new Node("baz");
            var andGroup = Guid.NewGuid();
            var edge1 = new Edge(source, target);
            var edge2 = new Edge(source, target);
            var edge3 = new Edge(source, target, andGroup);
            var edge4 = new Edge(source, target2);

            Assert.AreEqual(edge1.GetHashCode(), edge2.GetHashCode());
            Assert.AreEqual(edge1.GetHashCode(), edge3.GetHashCode());
            Assert.AreNotEqual(edge1.GetHashCode(), edge4.GetHashCode());
        }

    }

}
