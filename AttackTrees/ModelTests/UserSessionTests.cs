﻿using System;
using System.Collections.Generic;
using System.Linq;
using Model.Trees;
using NUnit.Framework;

namespace ModelTests
{
    /// <summary>
    /// Test cases for class <see cref="AttackTrees.Model.UserSession"/>.
    /// </summary>
    [TestFixture]
    public class UserSessionTests
    {

        [Test]
        public void NewTreeIsEmpty()
        {
            var tree = new Tree();
            Assert.IsTrue(tree.IsVerticesEmpty);
            Assert.IsTrue(tree.IsEdgesEmpty);
        }

        [Test]
        public void FirstNodeIsRoot()
        {
            var tree = new Tree();
            var node = new Node("Test");
            tree.AddNode(node, null);
            Assert.AreEqual(node, tree.RootNode);
        }

        [Test]
        public void CanAddNodes()
        {
            var tree = new Tree();
            var nodes = new List<Node>()
            {
                new Node("foo"),
                new Node("bar"),
                new Node("baz"),
                new Node("buz")
            };

            tree.AddNode(nodes[0], null);
            tree.AddNode(nodes[1], nodes[0]);
            tree.AddNode(nodes[2], nodes[0]);
            tree.AddNode(nodes[3], nodes[1]);

            CollectionAssert.AreEquivalent(nodes, tree.Vertices);
        }

        [Test]
        public void NewNodeIsConnected()
        {
            var tree = new Tree();
            var nodes = new List<Node>()
            {
                new Node("foo"),
                new Node("bar"),
                new Node("baz"),
                new Node("buz")
            };

            tree.AddNode(nodes[0], null);
            tree.AddNode(nodes[1], nodes[0]);
            tree.AddNode(nodes[2], nodes[0]);
            tree.AddNode(nodes[3], nodes[1]);

            Assert.IsTrue(tree.ContainsEdge(nodes[0], nodes[1]));
            Assert.IsTrue(tree.ContainsEdge(nodes[0], nodes[2]));
            Assert.IsTrue(tree.ContainsEdge(nodes[1], nodes[3]));
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AddNodeThrowsIfNodeIsNull()
        {
            var tree = new Tree();
            tree.AddNode(null, null);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void AddNodeThrowsIfSameNodeIsAddedTwice()
        {
            var node = new Node("foo");
            var tree = new Tree();
            tree.AddNode(node, null);
            tree.AddNode(node, node);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void AddNodeThrowsIfRootNodeHasParent()
        {
            var node1 = new Node("foo");
            var node2 = new Node("bar");
            var tree = new Tree();
            tree.AddNode(node1, node2);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void AddNodeThrowsIfNonRootNodeHasNoParent()
        {
            var node1 = new Node("foo");
            var node2 = new Node("bar");
            var tree = new Tree();
            tree.AddNode(node1, null);
            tree.AddNode(node2, null);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void AddNodeThrowsIfParentIsNotInTree()
        {
            var node1 = new Node("foo");
            var node2 = new Node("bar");
            var node3 = new Node("baz");
            var tree = new Tree();
            tree.AddNode(node1, null);
            tree.AddNode(node2, node3);
        }

        [Test]
        public void CanRemoveNodes()
        {
            var node1 = new Node("foo");
            var node2 = new Node("bar");
            var node3 = new Node("baz");
            var node4 = new Node("buz");
            var tree = new Tree();
            tree.AddNode(node1, null);
            tree.AddNode(node2, node1);
            tree.AddNode(node3, node2);
            tree.AddNode(node4, node2);
            tree.RemoveNode(node2);

            // Test that all nodes that are not removed are still there
            CollectionAssert.Contains(tree.Vertices, node1);
            CollectionAssert.Contains(tree.Vertices, node3);
            CollectionAssert.Contains(tree.Vertices, node4);

            // Test that removed node isn't there anymore
            CollectionAssert.DoesNotContain(tree.Vertices, node2);
        }

        [Test]
        public void WhenNodeIsRemovedChildrenGetReConnected()
        {
            var node1 = new Node("foo");
            var node2 = new Node("bar");
            var node3 = new Node("baz");
            var node4 = new Node("buz");
            var tree = new Tree();
            tree.AddNode(node1, null);
            tree.AddNode(node2, node1);
            tree.AddNode(node3, node2);
            tree.AddNode(node4, node2);
            tree.RemoveNode(node2);

            // Test that nodes are still connected
            Assert.IsTrue(tree.ContainsEdge(node1, node3));
            Assert.IsTrue(tree.ContainsEdge(node1, node4));
        }

        [Test]
        public void RemovingNodeRemovesEdges()
        {
            var node1 = new Node("foo");
            var node2 = new Node("bar");
            var node3 = new Node("baz");
            var node4 = new Node("buz");
            var tree = new Tree();
            tree.AddNode(node1, null);
            tree.AddNode(node2, node1);
            tree.AddNode(node3, node2);
            tree.AddNode(node4, node2);
            tree.RemoveNode(node2);

            // Test that edges are deleted
            Assert.IsFalse(tree.ContainsEdge(node1, node2));
            Assert.IsFalse(tree.ContainsEdge(node2, node3));
            Assert.IsFalse(tree.ContainsEdge(node2, node4));

            // Test that remaining edges are as expected
            var expectedEdges = new List<Edge>()
            {
                new Edge(node1, node3),
                new Edge(node1, node4)
            };
            CollectionAssert.AreEqual(expectedEdges, tree.Edges);
        }

        [Test]
        public void CanUpdateNodeConnection()
        {
            var node1 = new Node("foo");
            var node2 = new Node("bar");
            var node3 = new Node("baz");
            var node4 = new Node("buz");
            var tree = new Tree();
            tree.AddNode(node1, null);
            tree.AddNode(node2, node1);
            tree.AddNode(node3, node2);
            tree.AddNode(node4, node2);

            // Update single connection
            tree.UpdateConnection(node4, node3);

            // Test that old connection is gone
            Assert.IsFalse(tree.ContainsEdge(node2, node4));

            // Test that new connection is there
            Assert.IsTrue(tree.ContainsEdge(node3, node4));
        }

        [Test]
        public void CanGetNodes()
        {
            var tree = new Tree();
            var nodes = new List<Node>()
            {
                new Node("foo"),
                new Node("bar"),
                new Node("baz"),
                new Node("buz")
            };

            tree.AddNode(nodes[0], null);
            tree.AddNode(nodes[1], nodes[0]);
            tree.AddNode(nodes[2], nodes[0]);
            tree.AddNode(nodes[3], nodes[1]);

            CollectionAssert.AreEquivalent(nodes, tree.Vertices);
        }

        [Test]
        public void CanGetOutEdgesforNode()
        {
            var tree = new Tree();
            var nodes = new List<Node>()
            {
                new Node("foo"),
                new Node("bar"),
                new Node("baz"),
                new Node("buz")
            };

            tree.AddNode(nodes[0], null);
            tree.AddNode(nodes[1], nodes[0]);
            tree.AddNode(nodes[2], nodes[0]);
            tree.AddNode(nodes[3], nodes[0]);

            var outEdges = tree.OutEdges(nodes[0]);
            Assert.AreEqual(3, outEdges.Count());
            CollectionAssert.Contains(outEdges, new Edge(nodes[0], nodes[1]));
            CollectionAssert.Contains(outEdges, new Edge(nodes[0], nodes[2]));
            CollectionAssert.Contains(outEdges, new Edge(nodes[0], nodes[3]));
        }

    }

}
