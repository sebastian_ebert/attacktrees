﻿using System;
using Model.Trees;
using NUnit.Framework;

namespace ModelTests
{
    [TestFixture]
    public class NodeTests
    {

        [Test]
        public void CanCreateNode()
        {
            new Node("Test");
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NewNodeNeedsNonNullName()
        {
            new Node(null);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void NewNodeNeedsNonEmptyName()
        {
            new Node("");
        }

        [Test]
        public void NewNodeHasId()
        {
            var node = new Node("Test");
            Assert.IsNotNull(node.Id);
            Assert.AreNotEqual(Guid.Empty, node.Id);
        }

        [Test]
        public void NewNodeHasName()
        {
            var node = new Node("Test");
            Assert.AreEqual("Test", node.Name);
        }

        [Test]
        public void NodeIdsAreUnique()
        {
            var node1 = new Node("foo");
            var node2 = new Node("bar");
            Assert.IsFalse(node1.Id == node2.Id);
        }

    }

}
