﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Model.Functions;
using Model.Functions.Operators;
using Model.Trees;
using Model.Functions.Evaluation.Local;

namespace ModelTests
{
    [TestFixture]
    public class EvaluationTests
    {
        [Test]
        public void CanEvaluateSingleAttribute()
        {
            // Create function
            var func = new Function("Least Cost");
            func.Add("Cost", AttributeOperators.Add, GroupOperators.Undefined, SelectOperators.Minimum);

            // Create sample tree
            var root = new Node("Root");
            var nodeA = new Node("A");
            var nodeB = new Node("B");
            var nodeC = new Node("C");
            var nodeD = new Node("D");
            var nodeE = new Node("E");
            var tree = new Tree();
            var andGroup = Guid.NewGuid();

            tree.AddNode(root, null);
            tree.AddNode(nodeA, root, andGroup);
            tree.AddNode(nodeB, root, andGroup);
            tree.AddNode(nodeC, root);
            tree.AddNode(nodeD, nodeC);
            tree.AddNode(nodeE, nodeC);
            nodeA.AddAttribute("Cost", 100m);
            nodeB.AddAttribute("Cost", 50m);
            nodeD.AddAttribute("Cost", 160m);
            nodeE.AddAttribute("Cost", 180m);

            // Evaluate
            var eval = new Evaluator(func);
            var result = eval.Evaluate(tree);

            // Check results
            Assert.IsTrue(result);
            Assert.IsTrue(nodeC.Attributes.ContainsKey("Cost"));
            Assert.AreEqual(160m, nodeC.Attributes["Cost"].AsNumber());
            Assert.IsTrue(root.Attributes.ContainsKey("Cost"));
            Assert.AreEqual(150m, root.Attributes["Cost"].AsNumber());
        }

        [Test]
        public void CanEvaluateBoolAndNumber()
        {
            // Create function
            var func = new Function("LeastCostWithNoSepcialTools");
            func.Add("SpecialTools?", AttributeOperators.Or, GroupOperators.Undefined, SelectOperators.IsFalse);
            func.Add("Cost", AttributeOperators.Add, GroupOperators.Undefined, SelectOperators.Minimum);

            // Create test tree
            var root = new Node("Root");
            var nodeA = new Node("A");
            var nodeB = new Node("B");
            var nodeC = new Node("C");
            var nodeD = new Node("D");
            var nodeE = new Node("E");
            var tree = new Tree();
            var andGroup = Guid.NewGuid();

            tree.AddNode(root, null);
            tree.AddNode(nodeA, root, andGroup);
            tree.AddNode(nodeB, root, andGroup);
            tree.AddNode(nodeC, root);
            tree.AddNode(nodeD, nodeC);
            tree.AddNode(nodeE, nodeC);
            nodeA.AddAttribute("Cost", 100m);
            nodeA.AddAttribute("SpecialTools?", true);
            nodeB.AddAttribute("Cost", 50m);
            nodeB.AddAttribute("SpecialTools?", false);
            nodeD.AddAttribute("Cost", 160m);
            nodeD.AddAttribute("SpecialTools?", true);
            nodeE.AddAttribute("Cost", 180m);
            nodeE.AddAttribute("SpecialTools?", false);

            // Evaluate
            var eval = new Evaluator(func);
            var result = eval.Evaluate(tree);

            // Check results
            Assert.IsTrue(result);
            Assert.IsTrue(nodeC.Attributes.ContainsKey("Cost"));
            Assert.AreEqual(180m, nodeC.Attributes["Cost"].AsNumber());
            Assert.IsTrue(nodeC.Attributes.ContainsKey("SpecialTools?"));
            Assert.IsFalse(nodeC.Attributes["SpecialTools?"].AsBool());
            Assert.IsTrue(root.Attributes.ContainsKey("Cost"));
            Assert.AreEqual(180m, root.Attributes["Cost"].AsNumber());
            Assert.IsTrue(root.Attributes.ContainsKey("SpecialTools?"));
            Assert.IsFalse(root.Attributes["SpecialTools?"].AsBool());
        }

        [Test]
        public void CanEvaluateBoolAndManyNumbers()
        {
            // Create function
            var func = new Function("LeastCostAndTimeWithNoSepcialTools");
            func.Add("SpecialTools?", AttributeOperators.Or, GroupOperators.Undefined, SelectOperators.IsFalse);
            func.Add("Time", AttributeOperators.Add, GroupOperators.Add, SelectOperators.Undefined);
            func.Add("Cost", AttributeOperators.Add, GroupOperators.Undefined, SelectOperators.Minimum);

            // Create test tree
            var root = new Node("Root");
            var nodeA = new Node("A");
            var nodeB = new Node("B");
            var nodeC = new Node("C");
            var nodeD = new Node("D");
            var nodeE = new Node("E");
            var tree = new Tree();
            var andGroup = Guid.NewGuid();

            tree.AddNode(root, null);
            tree.AddNode(nodeA, root, andGroup);
            tree.AddNode(nodeB, root, andGroup);
            tree.AddNode(nodeC, root);
            tree.AddNode(nodeD, nodeC);
            tree.AddNode(nodeE, nodeC);
            nodeA.AddAttribute("Cost", 100m);
            nodeA.AddAttribute("Time", 5m);
            nodeA.AddAttribute("SpecialTools?", true);
            nodeB.AddAttribute("Cost", 50m);
            nodeB.AddAttribute("Time", 8m);
            nodeB.AddAttribute("SpecialTools?", false);
            nodeD.AddAttribute("Cost", 160m);
            nodeD.AddAttribute("Time", 3m);
            nodeD.AddAttribute("SpecialTools?", true);
            nodeE.AddAttribute("Cost", 180m);
            nodeE.AddAttribute("Time", 7m);
            nodeE.AddAttribute("SpecialTools?", false);

            // Evaluate
            var eval = new Evaluator(func);
            var result = eval.Evaluate(tree);

            // Check results
            Assert.IsTrue(result);

            Assert.IsTrue(nodeC.Attributes.ContainsKey("Cost"));
            Assert.AreEqual(180m, nodeC.Attributes["Cost"].AsNumber());
            Assert.IsTrue(nodeC.Attributes.ContainsKey("SpecialTools?"));
            Assert.IsFalse(nodeC.Attributes["SpecialTools?"].AsBool());
            Assert.IsTrue(nodeC.Attributes.ContainsKey("Time"));
            Assert.AreEqual(7m, nodeC.Attributes["Time"].AsNumber());

            Assert.IsTrue(root.Attributes.ContainsKey("Cost"));
            Assert.AreEqual(180m, root.Attributes["Cost"].AsNumber());
            Assert.IsTrue(root.Attributes.ContainsKey("SpecialTools?"));
            Assert.IsFalse(root.Attributes["SpecialTools?"].AsBool());
            Assert.IsTrue(root.Attributes.ContainsKey("Time"));
            Assert.AreEqual(7m, root.Attributes["Time"].AsNumber());
        }

        [Test]
        public void CantEvaluateIfAllNodesFiltered()
        {
            // Create function
            var func = new Function("LeastCostWithNoSepcialTools");
            func.Add("SpecialTools?", AttributeOperators.Or, GroupOperators.Undefined, SelectOperators.IsFalse);
            func.Add("Cost", AttributeOperators.Add, GroupOperators.Undefined, SelectOperators.Minimum);

            // Create test tree
            var root = new Node("Root");
            var nodeA = new Node("A");
            var nodeB = new Node("B");
            var nodeC = new Node("C");
            var nodeD = new Node("D");
            var nodeE = new Node("E");
            var tree = new Tree();
            var andGroup = Guid.NewGuid();

            tree.AddNode(root, null);
            tree.AddNode(nodeA, root, andGroup);
            tree.AddNode(nodeB, root, andGroup);
            tree.AddNode(nodeC, root);
            tree.AddNode(nodeD, nodeC);
            tree.AddNode(nodeE, nodeC);
            nodeA.AddAttribute("Cost", 100m);
            nodeA.AddAttribute("SpecialTools?", true);
            nodeB.AddAttribute("Cost", 50m);
            nodeB.AddAttribute("SpecialTools?", true);
            nodeD.AddAttribute("Cost", 160m);
            nodeD.AddAttribute("SpecialTools?", true);
            nodeE.AddAttribute("Cost", 180m);
            nodeE.AddAttribute("SpecialTools?", true);

            // Evaluate
            var eval = new Evaluator(func);
            var result = eval.Evaluate(tree);

            // Check results
            Assert.IsFalse(result);
        }
    }
}
