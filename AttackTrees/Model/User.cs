﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace Model
{
    /// <summary>
    /// The user properties and functions
    /// </summary>
    public class User
    {

        /// <summary>
        /// The user's Id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// The user's username
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// The user's first name
        /// </summary>
        public string FirstName{ get; set; }
        
        /// <summary>
        /// The user's last name
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// The user's password
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// The user's salt
        /// </summary>
        public string Salt { get; set; }

        /// <summary>
        /// The user's rights
        /// </summary>
        public List<string> Rights { get; set;}

        /// <summary>
        /// Standard constructor
        /// </summary>
        public User() 
        {
            this.Rights = new List<string>();
            this.Id = Guid.NewGuid();
        }

        /// <summary>
        /// Standard constructor for and existing user
        /// </summary>
        public User(Guid Id)
        {
            this.Rights = new List<string>();
            this.Id = Id;
        }

        /// <summary>
        /// Searches if the user has a given right
        /// </summary>
        /// <param name="searchRight">The right you are checking for</param>
        /// <returns>True if the user has the rights</returns>
        public bool HasRightsFor(string searchRight)
        {
            foreach (string i in this.Rights) 
            {
                if (i.Equals(searchRight))
                {
                    return true;
                }
            }

            return false;
        } 

        /// <summary>
        /// Adds a new right to the user
        /// </summary>
        /// <param name="newRight">The right you are adding</param>
        public void AddRight(string newRight)
        {
           this.Rights.Add(newRight);
        }

        /// <summary>
        /// Removes a right from the user
        /// </summary>
        /// <param name="oldRight">The right you are removing</param>
        /// <returns>True if removed, False if not found or could not be deleted</returns>
        public bool RemoveRight(string oldRight)
        {
            return this.Rights.Remove(oldRight);
        }
    }
}
