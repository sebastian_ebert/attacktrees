﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.Trees;

namespace Model
{

    public interface ITreeRepository
    {

        Node SaveOrUpdateNode(Model.Trees.Node node);

        bool SaveOrUpdate(Tree tree);

        bool TreeExists(Guid id);

        Node GetNodeById(Guid id);

        bool Delete(Tree tree);

        Tree GetById(Guid id);

        Tree FindByName(string pattern);

        List<Tree> FindAllByName(string pattern);

        List<Tree> FindAllByNodeName(string pattern);

        List<Tree> FindAllByAttributeName(string pattern);

        bool SaveOrUpdateEdge(Model.Trees.Edge e);

        Edge GetEdge(Node ancestor, Node descendant);

        bool DoesEdgeExist(Edge e);

        bool UpdateEdgeAndGroup(Model.Trees.Edge e, Guid andGroupId);

        bool HasPermission(Tree entity, string permission);

        bool DeletePermission(Tree entity, string permission);

        List<string> GetPermissions(Tree entity);

        bool AddPermission(Tree entity, string permission);

        List<Tree> GetAllTrees();

    }

}
