﻿namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Interface for CRUD User operations
    /// </summary>
    public interface IUserRepository
    {
        /// <summary>
        /// Inserts a new user into the database.
        /// </summary>
        /// <param name="user">The User Object to insert.</param>
        /// <returns>True on success, false otherwise</returns>
        /// <exception cref="UserAlreadyExistsException"></exception>
        bool CreateUpdateUser(User user);

        /// <summary>
        /// Checks if a user already exists in the database
        /// </summary>
        /// <param name="user"></param>
        /// <returns>True if the user exists, false otherwise</returns>
        bool UserExists(string username);

        /// <summary>
        /// Delete a user from the DB
        /// </summary>
        /// <param name="user">The User Object which should be deleted</param>
        /// <returns>True on success, false otherwise</returns>
        /// <exception cref="NoSuchUserException"></exception>
        bool DeleteUser(User user);

        /// <summary>
        /// Load a User object from the database
        /// </summary>
        /// <param name="userName">Username string</param>
        /// <returns>The loaded user or null if none could be found</returns>
        User ReadUser(string userName);

        Guid GetPermissionId(string permission);

        bool HasPermission(User user, string permission);

        bool DeletePermission(User user, string permission);

        List<string> GetPermissions(User entity);

        bool AddPermission(User user, string permission);
    }
}
