﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.Trees;

namespace Model.Functions
{
    public interface IEvaluator
    {

        Function Function { get; }

        bool Evaluate(Tree tree);

    }
}
