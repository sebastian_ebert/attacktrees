﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.Functions.Operators
{
    /// <summary>
    /// Available operators for combining attribute values
    /// to propagate them up the tree.
    /// </summary>
    public enum AttributeOperators
    {
        /// <summary>
        /// Add values.
        /// </summary>
        Add,

        /// <summary>
        /// Subtract values.
        /// </summary>
        Subtract,

        /// <summary>
        /// Multiply values.
        /// </summary>
        Multiply,

        /// <summary>
        /// Divide values.
        /// </summary>
        Divide,

        /// <summary>
        /// Minimum value.
        /// </summary>
        Minimum,

        /// <summary>
        /// Maximum value.
        /// </summary>
        Maximum,

        /// <summary>
        /// Average value.
        /// </summary>
        Average,

        /// <summary>
        /// Logical AND combined value.
        /// </summary>
        And,

        /// <summary>
        /// Logical OR combined value.
        /// </summary>
        Or,

        /// <summary>
        /// Logical XOR combined value.
        /// </summary>
        XOr
    }
}
