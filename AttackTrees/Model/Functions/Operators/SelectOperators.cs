﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.Functions.Operators
{

    /// <summary>
    /// Available operators for selection nodes or sub-trees
    /// as part of the result tree.
    /// </summary>
    public enum SelectOperators
    {
        Undefined,
   
        /// <summary>
        /// Selects node or sub-tree with minimum value.
        /// </summary>
        Minimum,

        /// <summary>
        /// Select node or sub-tree with maximum value.
        /// </summary>
        Maximum,

        /// <summary>
        /// Select nodes where value is true.
        /// </summary>
        IsTrue,

        /// <summary>
        /// Select nodes where value is false.
        /// </summary>
        IsFalse
    }
}
