﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.Functions.Operators
{
    public enum GroupOperators
    {
        Undefined,
        Add,
        Subtract,
        Multiply,
        Divide
    }
}
