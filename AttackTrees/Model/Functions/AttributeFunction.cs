﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.Functions.Operators;

namespace Model.Functions
{
    public class AttributeFunction
    {

        #region Constructors & Finalizers

        public AttributeFunction(string attributeName, AttributeOperators andOp, GroupOperators groupOp, SelectOperators selectOp)
        {
            this.AttributeName = attributeName;
            this.AndOp = andOp;
            this.GroupOp = groupOp;
            this.SelectOp = selectOp;

            // Get the result type from operator
            if (andOp == AttributeOperators.And ||
                andOp == AttributeOperators.Or ||
                andOp == AttributeOperators.XOr)
            {
                this.ReturnType = Types.Boolean;
            }
            else
            {
                this.ReturnType = Types.Number;
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the name of the attribute this function applies to.
        /// </summary>
        public string AttributeName { get; private set; }

        /// <summary>
        /// Operation for propagation AND-groups.
        /// </summary>
        public AttributeOperators AndOp { get; private set; }

        /// <summary>
        /// Operation for grouping multiple values.
        /// </summary>
        public GroupOperators GroupOp { get; private set; }

        /// <summary>
        /// Operation for selecting sub-trees as part of the
        /// result tree.
        /// </summary>
        public SelectOperators SelectOp { get; private set; }

        /// <summary>
        /// Gets the attribute value type this function works
        /// on, can be either <see cref="Types.Number"/> or
        /// <see cref="Types.Boolean"/>.
        /// </summary>
        public Types ReturnType { get; private set; }

        #endregion

    }
}
