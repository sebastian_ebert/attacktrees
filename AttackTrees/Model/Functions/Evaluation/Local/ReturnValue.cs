﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.Trees;

namespace Model.Functions.Evaluation.Local
{
    
    public class ReturnValue
    {

        public Types Type { get; private set; }

        public object Value { get; private set; }


        public ReturnValue(Decimal value)
        {
            this.Type = Types.Number;
            this.Value = value;
        }

        public ReturnValue(bool value)
        {
            this.Type = Types.Boolean;
            this.Value = value;
        }

        public ReturnValue(Node node)
        {
            this.Type = Types.Node;
            this.Value = Value;
        }

    }

}
