﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.Trees;
using Model.Functions.Operators;

namespace Model.Functions.Evaluation.Local
{

    /// <summary>
    /// Local, in-memory function evaluator.
    /// </summary>
    public class Evaluator : IEvaluator
    {
        
        #region Embedded Classes

        private class NodeOrAndGroup
        {

            public Guid AndGroup { get; private set; }

            public Node Node { get; private set; }

            public List<Node> AndGroupNodes { get; private set; }


            public NodeOrAndGroup(Node node)
            {
                this.AndGroup = Guid.Empty;
                this.Node = node;
                this.AndGroupNodes = null;
            }

            public NodeOrAndGroup(Guid andGroup, List<Node> nodes)
            {
                this.AndGroup = andGroup;
                this.Node = new Node("@AND-" + andGroup);
                this.AndGroupNodes = nodes;
            }

        }

        #endregion

        #region Constructors & Finalizers

        /// <summary>
        /// Initializes a new instance of class Evaluator given
        /// a function to use.
        /// </summary>
        /// <param name="function">A function.</param>
        public Evaluator(Function function)
        {
            this.Function = function;
        }

        #endregion

        #region Properties

        /// <summary>
        /// The function this Evaluator applies to a tree.
        /// </summary>
        public Function Function { get; private set; }

        #endregion

        #region Methods

        public bool Evaluate(Tree tree)
        {
            // Clear result state
            foreach (var node in tree.Vertices)
                node.IsResult = false;

            // Propagate attribute values
            var result = EvaluateNode(tree, tree.RootNode);

            // Clear now invalid, intermediate results
            if (result)
            {
                tree.RootNode.IsResult = true;
            }
            foreach (var node in tree.Vertices)
            {
                var parent = tree.FindParent(node);
                if (parent != null)
                {
                    if (node.IsResult && !parent.IsResult)
                    {
                        node.IsResult = false;
                    }
                }
            }

            return result;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Recursively evaluates a node.
        /// </summary>
        /// <remarks>
        /// <p>Leaf nodes are always 'evaluated' successfully.</p>
        /// </remarks>
        /// <param name="tree">Tree to evaluate.</param>
        /// <param name="node">Node to evaluate.</param>
        /// <returns>Could the node be evaluated successfully? Meaning: there
        /// were attributes propagated to this node.</returns>
        private bool EvaluateNode(Tree tree, Node node)
        {
            // Leafs don't have to be evaluated
            if (tree.IsLeaf(node))
                return true;

            // Evaluate children
            var validChilren = new List<Node>();
            foreach (var child in tree.FindChildren(node))
            {
                if (EvaluateNode(tree, child))
                    validChilren.Add(child);
            }
            // Don't have to go on if there isn't at least one child
            // node that could be evaluated successfully
            if (validChilren.Count == 0)
                return false;

            // 1. Sort child nodes into AND-Groups and OR nodes
            var andGroups = new Dictionary<Guid, List<Node>>();
            var orNodes = new List<Node>();
            foreach (var childEdge in tree.OutEdges(node).Where(e => validChilren.Contains(e.Target)))
            {
                var child = childEdge.Target;
                if (childEdge.IsAnd)
                {
                    if (!andGroups.ContainsKey(childEdge.AndGroup))
                        andGroups[childEdge.AndGroup] = new List<Node>();
                    andGroups[childEdge.AndGroup].Add(child);
                }
                else
                {
                    orNodes.Add(child);
                }
            }

            // 2. Sort functions into boolean and numeric
            var attribFuncsBool = new List<AttributeFunction>(Function.AttributeFunctions.Where(af => af.ReturnType == Types.Boolean));
            var attribFuncsNumber = new List<AttributeFunction>(Function.AttributeFunctions.Where(af => af.ReturnType == Types.Number));

            // 3. Keep track of if boolean steps return empty result sets
            bool hasResult = true;

            // 4. Start evaluating attribute by attribute - booleans first
            var andGroupValuesBool = new Dictionary<Guid, Dictionary<string, bool>>(attribFuncsBool.Count);
            foreach (var attribFunc in attribFuncsBool)
            {
                // 4.1 Accumulate AND-group values
                foreach (var andGroup in andGroups)
                {
                    if (!andGroupValuesBool.ContainsKey(andGroup.Key))
                        andGroupValuesBool.Add(andGroup.Key, new Dictionary<string, bool>());
                    andGroupValuesBool[andGroup.Key].Add(attribFunc.AttributeName, EvaluateBool(attribFunc.AttributeName, attribFunc.AndOp, andGroup.Value));
                }

                // 4.2 Apply filter
                // - To AND-groups
                var filteredAndGroups = new Dictionary<Guid, List<Node>>();
                foreach (var andGroup in andGroups)
                {
                    var value = andGroupValuesBool[andGroup.Key][attribFunc.AttributeName];
                    if (ApplyFilter(attribFunc.SelectOp, value))
                    {
                        filteredAndGroups.Add(andGroup.Key, andGroup.Value);
                    }
                }

                // - To OR nodes
                var filteredOrNodes = new List<Node>();
                foreach (var n in orNodes)
                {
                    var value = n.Attributes[attribFunc.AttributeName];
                    if (ApplyFilter(attribFunc.SelectOp, value.AsBool()))
                    {
                        filteredOrNodes.Add(n);
                    }
                }

                // - Check if result set is empty
                if (filteredAndGroups.Count == 0 && filteredOrNodes.Count == 0)
                {
                    hasResult = false;
                }

                // Only use filter if there are nodes left
                if (hasResult)
                {
                    andGroups = filteredAndGroups;
                    orNodes = filteredOrNodes;
                }
            }

            // 5. Evaluate attribute by attribute - numbers
            // 5.1 First run - accumulate AND-groups
            var andGroupValuesNumber = new Dictionary<Guid, Dictionary<string, decimal>>(andGroups.Count);
            foreach (var andGroup in andGroups)
            {
                var groupValues = new Dictionary<string, decimal>(attribFuncsNumber.Count);
                foreach (var attribFunc in attribFuncsNumber)
                {
                    var value = EvaluateNumber(attribFunc.AttributeName, attribFunc.AndOp, andGroup.Value);
                    groupValues.Add(attribFunc.AttributeName, value);
                }
                andGroupValuesNumber.Add(andGroup.Key, groupValues);
            }

            // 5.2 Second run - accumulate attribte values by grouping
            var andGroupValuesGrouped = new Dictionary<Guid, decimal>(andGroups.Count);
            var orNodeValuesGrouped = new Dictionary<Node, decimal>(orNodes.Count);
            var resultNodes = new List<Node>();
            var resultNodeValues = new Dictionary<Node, Dictionary<string, ReturnValue>>();
            if (attribFuncsNumber.Count() > 0)
            {
                // AND-groups
                foreach (var andGroup in andGroups)
                {
                    var accum = andGroupValuesNumber[andGroup.Key][attribFuncsNumber[0].AttributeName];
                    for (var i = 1; i < attribFuncsNumber.Count; ++i)
                    {
                        var op = attribFuncsNumber[i - 1].GroupOp;
                        var attrib = attribFuncsNumber[i].AttributeName;
                        var value = andGroupValuesNumber[andGroup.Key][attrib];
                        var result = EvaluateNumber(op, accum, value);
                    }
                    andGroupValuesGrouped.Add(andGroup.Key, accum);
                }

                // OR nodes
                foreach (var orNode in orNodes)
                {
                    var accum = orNode.Attributes[attribFuncsNumber[0].AttributeName].AsNumber();
                    for (var i = 1; i < attribFuncsNumber.Count; ++i)
                    {
                        var op = attribFuncsNumber[i - 1].GroupOp;
                        var attrib = attribFuncsNumber[i].AttributeName;
                        var value = orNode.Attributes[attrib].AsNumber();
                        var result = EvaluateNumber(op, accum, value);
                    }
                    orNodeValuesGrouped.Add(orNode, accum);
                }

                // 5.3 Apply selection operator
                var selectOp = attribFuncsNumber.Last().SelectOp;
                var selectedValue = 0m;
                var isFirst = true;
                if (andGroups.Count > 0)
                {
                    foreach (var andGroup in andGroups)
                    {
                        if (isFirst)
                        {
                            selectedValue = andGroupValuesGrouped[andGroup.Key];
                            isFirst = false;
                        }
                        else
                        {
                            switch (selectOp)
                            {
                                case SelectOperators.Minimum:
                                    selectedValue = Math.Min(selectedValue, andGroupValuesGrouped[andGroup.Key]);
                                    break;

                                case SelectOperators.Maximum:
                                    selectedValue = Math.Max(selectedValue, andGroupValuesGrouped[andGroup.Key]);
                                    break;

                                default:
                                    throw new InvalidOperationException("Operation " + selectOp + " isn't a valid select operator for numbers");
                            }
                        }
                    }
                }
                foreach (var orNode in orNodes)
                {
                    if (isFirst)
                    {
                        selectedValue = orNodeValuesGrouped[orNode];
                        isFirst = false;
                    }
                    else
                    {
                        switch (selectOp)
                        {
                            case SelectOperators.Minimum:
                                selectedValue = Math.Min(selectedValue, orNodeValuesGrouped[orNode]);
                                break;

                            case SelectOperators.Maximum:
                                selectedValue = Math.Max(selectedValue, orNodeValuesGrouped[orNode]);
                                break;

                            default:
                                throw new InvalidOperationException("Operation " + selectOp + " isn't a valid select operator for numbers");
                        }
                    }
                }

                // 5.4 pick nodes that match selected value
                foreach (var andGroup in andGroups)
                {
                    if (andGroupValuesGrouped[andGroup.Key] == selectedValue)
                    {
                        foreach (var andNode in andGroup.Value)
                        {
                            resultNodes.Add(andNode);
                            var attribs = new Dictionary<string, ReturnValue>();
                            foreach (var attribFunc in attribFuncsBool)
                            {
                                attribs.Add(attribFunc.AttributeName, new ReturnValue(andGroupValuesBool[andGroup.Key][attribFunc.AttributeName]));
                            }

                            foreach (var attribFunc in attribFuncsNumber)
                            {
                                attribs.Add(attribFunc.AttributeName, new ReturnValue(andGroupValuesNumber[andGroup.Key][attribFunc.AttributeName]));
                            }
                            resultNodeValues.Add(andNode, attribs);
                        }
                    }
                }
                foreach (var orNode in orNodes)
                {
                    if (orNodeValuesGrouped[orNode] == selectedValue)
                    {
                        resultNodes.Add(orNode);
                        var attribs = new Dictionary<string, ReturnValue>();
                        foreach (var attribFunc in attribFuncsBool)
                        {
                            attribs.Add(attribFunc.AttributeName, new ReturnValue(orNode.Attributes[attribFunc.AttributeName].AsBool()));
                        }

                        foreach (var attribFunc in attribFuncsNumber)
                        {
                            attribs.Add(attribFunc.AttributeName, new ReturnValue(orNode.Attributes[attribFunc.AttributeName].AsNumber()));
                        }
                        resultNodeValues.Add(orNode, attribs);
                    }
                }
            }
            else
            {
                foreach (var andGroup in andGroups)
                {
                    foreach (var andNode in andGroup.Value)
                    {
                        resultNodes.Add(andNode);
                        var attribs = new Dictionary<string, ReturnValue>();
                        foreach (var attribFunc in attribFuncsBool)
                        {
                            attribs.Add(attribFunc.AttributeName, new ReturnValue(andGroupValuesBool[andGroup.Key][attribFunc.AttributeName]));
                        }
                    }
                }
                foreach (var orNode in orNodes)
                {
                    resultNodes.Add(orNode);
                    var attribs = new Dictionary<string, ReturnValue>();
                    foreach (var attribFunc in attribFuncsBool)
                    {
                        attribs.Add(attribFunc.AttributeName, new ReturnValue(orNode.Attributes[attribFunc.AttributeName].AsBool()));
                    }

                    foreach (var attribFunc in attribFuncsNumber)
                    {
                        attribs.Add(attribFunc.AttributeName, new ReturnValue(orNode.Attributes[attribFunc.AttributeName].AsNumber()));
                    }
                    resultNodeValues.Add(orNode, attribs);
                }
            }

            // TODO: 5.5 Use attribute function order to resolve cases where there are
            // multiple nodes in the result set
            if (resultNodes.Count == 0)
                return false;

            if (hasResult)
            {
                foreach (var resultNode in resultNodes)
                {
                    resultNode.IsResult = true;
                }
            }

            foreach (var attribFunc in Function.AttributeFunctions)
            {
                if (!node.Attributes.ContainsKey(attribFunc.AttributeName))
                {
                    var result = resultNodeValues[resultNodes[0]][attribFunc.AttributeName];
                    if (result.Type == Types.Boolean)
                        node.AddAttribute(attribFunc.AttributeName, (bool)result.Value);
                    else
                        node.AddAttribute(attribFunc.AttributeName, (decimal)result.Value);
                }
                else
                {
                    var result = resultNodeValues[resultNodes[0]][attribFunc.AttributeName];
                    if (result.Type == Types.Boolean)
                        node.SetAttributeValue(attribFunc.AttributeName, (bool)result.Value);
                    else
                        node.SetAttributeValue(attribFunc.AttributeName, (decimal)result.Value);

                    node.SetAttributeValue(attribFunc.AttributeName, resultNodes[0].Attributes[attribFunc.AttributeName].AsNumber());
                }
            }

            return hasResult;
        }

        private decimal EvaluateNumber(string attributeName, AttributeOperators op, List<Node> nodes)
        {
            var accum = nodes[0].Attributes[attributeName].AsNumber();

            switch (op)
            {
                case AttributeOperators.Add:
                    for (var i = 1; i < nodes.Count;++i)
                        accum += nodes[1].Attributes[attributeName].AsNumber();
                    break;

                case AttributeOperators.Subtract:
                    for (var i = 1; i < nodes.Count; ++i)
                        accum -= nodes[1].Attributes[attributeName].AsNumber();
                    break;

                case AttributeOperators.Multiply:
                    for (var i = 1; i < nodes.Count; ++i)
                        accum *= nodes[1].Attributes[attributeName].AsNumber();
                    break;

                case AttributeOperators.Divide:
                    for (var i = 1; i < nodes.Count;++i)
                        accum /= nodes[1].Attributes[attributeName].AsNumber();
                    break;

                case AttributeOperators.Minimum:
                    for (var i = 1; i < nodes.Count; ++i)
                        accum = Math.Min(accum, nodes[1].Attributes[attributeName].AsNumber());
                    break;

                case AttributeOperators.Maximum:
                    for (var i = 1; i < nodes.Count; ++i)
                        accum = Math.Max(accum, nodes[1].Attributes[attributeName].AsNumber());
                    break;

                case AttributeOperators.Average:
                    for (var i = 1; i < nodes.Count; ++i)
                        accum += nodes[1].Attributes[attributeName].AsNumber();
                    accum /= new Decimal(nodes.Count);
                    break;

                default:
                    throw new ArgumentException("Operation can't be used on numbers", "op");
            }

            return accum;
        }

        private static decimal EvaluateNumber(GroupOperators op, decimal lhs, decimal rhs)
        {
            switch (op)
            {
                case GroupOperators.Add:
                    return lhs + rhs;

                case GroupOperators.Subtract:
                    return lhs - rhs;

                case GroupOperators.Multiply:
                    return lhs * rhs;

                case GroupOperators.Divide:
                    return lhs / rhs;

                default:
                    throw new ArgumentException("Unknown or invalid grouping operator", "op");
            }
        }

        private static bool EvaluateBool(string attributeName, AttributeOperators op, List<Node> nodes)
        {
            var accum = nodes[0].Attributes[attributeName].AsBool();

            switch (op)
            {
                case AttributeOperators.And:
                    for (var i = 1; i < nodes.Count; ++i)
                        accum = accum & nodes[1].Attributes[attributeName].AsBool();
                    break;

                case AttributeOperators.Or:
                    for (var i = 1; i < nodes.Count; ++i)
                        accum = accum | nodes[1].Attributes[attributeName].AsBool();
                    break;

                case AttributeOperators.XOr:
                    for (var i = 1; i < nodes.Count; ++i)
                        accum = accum ^ nodes[1].Attributes[attributeName].AsBool();
                    break;

                default:
                    throw new ArgumentException("Operation can't be used on booleans", "op");
            }

            return accum;
        }

        private static bool ApplyFilter(SelectOperators filterOp, bool value)
        {
            switch (filterOp)
            {
                case SelectOperators.IsTrue: return value;
                case SelectOperators.IsFalse: return !value;
                default:
                    throw new InvalidOperationException("Operator " + filterOp + " is not a valid filter operator");
            }
        }

        #endregion
        
    }

}
