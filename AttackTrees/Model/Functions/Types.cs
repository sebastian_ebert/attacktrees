﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.Functions
{

    /// <summary>
    /// Types as used in functions.
    /// </summary>
    public enum Types
    {
        /// <summary>
        /// Numeric type.
        /// </summary>
        Number,

        /// <summary>
        /// Boolean type.
        /// </summary>
        Boolean,

        /// <summary>
        /// Node type.
        /// </summary>
        Node,

        /// <summary>
        /// List of nodes type.
        /// </summary>
        NodeList
    }

}
