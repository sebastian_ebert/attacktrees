﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.Trees;

namespace Model.Functions
{
    public static class DefaultAttributes
    {

        public static NodeAttribute Cost(decimal value = 0m) { return new NodeAttribute("Cost", value); }

        public static NodeAttribute TimeNeeded(decimal value = 0m) { return new NodeAttribute("Time Needed", value); }

        public static NodeAttribute NoSpecialToolsRequired(bool value = false) { return new NodeAttribute("Special Tools?", value); }

    }
}
