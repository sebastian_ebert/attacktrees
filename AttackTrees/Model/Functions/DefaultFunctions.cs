﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.Functions
{
    public static class DefaultFunctions
    {

        public static readonly Function LeastCost;

        public static readonly Function NoSpecialToolsRequired;

        public static readonly Function LeastCostAndNoSpecialToolsRequired;


        static DefaultFunctions()
        {
            LeastCost = new Function("Least Cost");
            LeastCost.Add(DefaultAttributes.Cost().Name, Operators.AttributeOperators.Add, Operators.GroupOperators.Undefined, Operators.SelectOperators.Minimum);

            NoSpecialToolsRequired = new Function("No Special Tools");
            NoSpecialToolsRequired.Add(DefaultAttributes.NoSpecialToolsRequired().Name, Operators.AttributeOperators.Or, Operators.GroupOperators.Undefined, Operators.SelectOperators.IsFalse);

            LeastCostAndNoSpecialToolsRequired = new Function("Least Cost and no Special Tools");
            LeastCostAndNoSpecialToolsRequired.Add(DefaultAttributes.Cost().Name, Operators.AttributeOperators.Add, Operators.GroupOperators.Undefined, Operators.SelectOperators.Minimum);
            LeastCostAndNoSpecialToolsRequired.Add(DefaultAttributes.NoSpecialToolsRequired().Name, Operators.AttributeOperators.Or, Operators.GroupOperators.Undefined, Operators.SelectOperators.IsFalse);
        }

    }
}
