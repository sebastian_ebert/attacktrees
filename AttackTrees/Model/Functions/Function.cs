﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.Functions.Operators;

namespace Model.Functions
{
    public class Function
    {

        public Guid Id { get; set; }

        public string Name { get; private set;}

        public List<AttributeFunction> AttributeFunctions { get; private set; }

        public ISet<string> RequiredAttributes { get; private set; }


        public Function(string name)
        {
            this.Name = name;
            this.AttributeFunctions = new List<AttributeFunction>();
            this.RequiredAttributes = new HashSet<string>();
            this.Id = Guid.NewGuid();
        }


        public void Add(string attributeName, AttributeOperators andOp, GroupOperators groupOp, SelectOperators selectOp)
        {
            if (RequiredAttributes.Contains(attributeName))
                throw new InvalidOperationException("Can't add multiple operations for a single attribute");

            RequiredAttributes.Add(attributeName);
            AttributeFunctions.Add(new AttributeFunction(attributeName, andOp, groupOp, selectOp));
        }

    }
}
