﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QuickGraph;
using System.ComponentModel;
using System.Windows.Media;

namespace Model.Trees
{
    

    /// <summary>
    /// Defines a single edge of an attack tree.
    /// </summary>
    public class Edge : IEdge<Node>, INotifyPropertyChanged
    {
        #region Fields

        private Guid _andGroup;

        #endregion

        #region Constructors & Finalizers

        /// <summary>
        /// Initializes a new edge instance with source and target nodes.
        /// The new edge will not be part of an AND-group.
        /// </summary>
        /// <param name="source">Source node of the new edge.</param>
        /// <param name="target">Target node of the new edge.</param>
        public Edge(Node source, Node target)
            : this(source, target, Guid.Empty)
        {
        }

        /// <summary>
        /// Initializes a new edge instance with source and target nodes and
        /// an AND-group. <c>Guid.Empty</c> is used for 'no AND-group'.
        /// </summary>
        /// <param name="source">Source node of the new edge.</param>
        /// <param name="target">Target node of the new edge.</param>
        /// <param name="andGroup">The new nodes AND-group. Set to <c>Guid.Empty</c>
        /// for 'no AND-group'.</param>
        public Edge(Node source, Node target, Guid andGroup)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }
            if (target == null)
            {
                throw new ArgumentNullException("target");
            }

            this.Source = source;
            this.Target = target;
            this._andGroup = andGroup;

            this.Source.PropertyChanged += (sender, args) => {
                if (args.PropertyName == "IsExpanded")
                    NotifyPropertyChanged("IsExpanded");
                else if (args.PropertyName == "IsSelected")
                    NotifyPropertyChanged("IsSelected");
            };
            this.Target.PropertyChanged += (sender, args) => {
                if (args.PropertyName == "IsExpanded")
                    NotifyPropertyChanged("IsExpanded");
                else if (args.PropertyName == "IsSelected")
                    NotifyPropertyChanged("IsSelected");
            };
        }

        #endregion

        #region Properties


        /// <summary>
        /// Gets or sets the edges source or parent node.
        /// </summary>
        public Node Source { get; set; }

        /// <summary>
        /// Gets or sets the target node of the edge.
        /// </summary>
        public Node Target { get; set; }

        /// <summary>
        /// Gets or sets he AND-group of this edge. All AND-connected edges
        /// share the same group.
        /// </summary>
        public Guid AndGroup
        {
            get { return _andGroup; }
            set
            {
                if (_andGroup != value)
                {
                    _andGroup = value;
                    NotifyPropertyChanged("AndGroup");
                    NotifyPropertyChanged("IsAnd");
                }
            }
        }

        /// <summary>
        /// Gets if the edge is expanded/visible.
        /// </summary>
        public bool IsExpanded
        {
            get
            {
                return Target.IsExpanded; 
            }
        }

        /// <summary>
        /// Gets if the edge is selected/not selected.
        /// </summary>
        public bool IsSelected
        {
            get
            {
                return Target.IsSelected && Source.IsSelected;
            }
        }

        /// <summary>
        /// Gets if the edge is part of an AND-group.
        /// </summary>
        public bool IsAnd
        {
            get { return AndGroup != Guid.Empty; }
        }
         
        #endregion

        #region Methods

        public override bool Equals(object obj)
        {
            var other = obj as Edge;
            if (other == null)
                return false;
            if (other.Source != Source)
                return false;
            if (other.Target != Target)
                return false;
            return true;
        }

        public override int GetHashCode()
        {
            return Source.GetHashCode() ^ Target.GetHashCode();
        }

        #endregion

        #region INotifyPropertyChanged Implementation

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        #endregion
        
    }
}
