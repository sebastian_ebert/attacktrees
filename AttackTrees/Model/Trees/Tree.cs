﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QuickGraph;
using System.Diagnostics.Contracts;
using System.Diagnostics;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Model.Trees
{
    public class Tree : IBidirectionalGraph<Node, Edge>, INotifyPropertyChanged, ICloneable

    {

        #region Members

        private BidirectionalGraph<Node, Edge> _backingGraph;

        private HashSet<Guid> _andGroups;

        #endregion

        #region Constructors & Finalizers

        public Tree()
            : this(Guid.NewGuid())
        {
        }

        public Tree(Guid id)
        {

            _backingGraph = new BidirectionalGraph<Node, Edge>(false);
            _andGroups = new HashSet<Guid>();
            DefaultAttributes = new Dictionary<string, NodeAttribute>();
            this.Id = id;
        }

        #endregion

        #region Events - Implementation of INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the unique identifier of this tree.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the root node of the tree.
        /// </summary>
        public Node RootNode { get; private set; }

        /// <summary>
        /// Gets a dictionary of all default node attributes of
        /// this tree.
        /// </summary>
        public Dictionary<string, NodeAttribute> DefaultAttributes { get; private set; }

        /// <summary>
        /// Gets or sets the trees name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a textual description for this tree.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the 'tree has changed' flag.
        /// </summary>
        public bool HasChanged { get; set; }

        /// <summary>
        /// Gets all leaf nodes of this tree.
        /// </summary>
        public IEnumerable<Node> Leafs
        {
            get
            {
                // Special cases
                if (VertexCount == 0)
                    return new List<Node>();
                if (VertexCount == 1)
                    return new List<Node>() { RootNode };

                // General case
                return Vertices.Where(node => IsLeaf(node));
            }
        }

        /// <summary>
        /// Always <c>false</c> as trees don't allow for parallel edges.
        /// </summary>
        public bool AllowParallelEdges
        {
            get { return false; }
        }

        /// <summary>
        /// Always <c>true</c> as trees are always directed graphs.
        /// </summary>
        public bool IsDirected
        {
            get { return true; }
        }

        /// <summary>
        /// Gets if the tree contains any nodes.
        /// </summary>
        public bool IsVerticesEmpty
        {
            get { return _backingGraph.IsVerticesEmpty; }
        }

        /// <summary>
        /// Gets the number of nodes stored in the tree.
        /// </summary>
        public int VertexCount
        {
            get { return _backingGraph.VertexCount; }
        }

        /// <summary>
        /// Gets an enumeration of all vertices in the tree.
        /// </summary>
        public IEnumerable<Node> Vertices
        {
            get { return _backingGraph.Vertices; }
        }

        /// <summary>
        /// Gets the number of edges of the tree.
        /// </summary>
        public int EdgeCount
        {
            get { return _backingGraph.EdgeCount; }
        }

        /// <summary>
        /// Gets an enumeration of all edges in the tree.
        /// </summary>
        public IEnumerable<Edge> Edges
        {
            get { return _backingGraph.Edges; }
        }

        /// <summary>
        /// Gets if the tree contains any edges.
        /// </summary>
        public bool IsEdgesEmpty
        {
            get { return _backingGraph.IsEdgesEmpty; }
        }
        
        #endregion

        #region Methods

        /// <summary>
        /// Adds a new node to the tree and connects it to it's parent.
        /// </summary>
        /// <param name="node">The new node to add.</param>
        /// <param name="parent">The new nodes parent.</param>
        /// <exception cref="ArgumentNullException">When the node parameter
        /// is <c>null</c>.</exception>
        /// <exception cref="ArgumentException">When the combination of nodes
        /// given isn't valid or one of them isn't part of the tree.</exception>
        public void AddNode(Node node, Node parent)
        {
            AddNode(node, parent, Guid.Empty);
        }

        /// <summary>
        /// Adds a new node to the tree and connects it to it's parent. Also
        /// allows to specify an AND-group for the edge.
        /// </summary>
        /// <param name="node">The new node to add.</param>
        /// <param name="parent">The new nodes parent.</param>
        /// <param name="andGroup">AND-group to use for the newly created edge.</param>
        /// <exception cref="ArgumentNullException">When the node parameter
        /// is <c>null</c>.</exception>
        /// <exception cref="ArgumentException">When the combination of nodes
        /// given isn't valid or one of them isn't part of the tree.</exception>
        public void AddNode(Node node, Node parent, Guid andGroup)
        {
            if (node == null)
            {
                throw new ArgumentNullException("node", "Node can't be null");
            }
            if (ContainsVertex(node))
            {
                throw new ArgumentException("node", "Can't add the same node twice");
            }
            if (RootNode == null && parent != null)
            {
                throw new ArgumentException("parent", "Root node can't have a parent");
            }
            if (RootNode != null && parent == null)
            {
                throw new ArgumentException("parent", "Non-root node must have a parent");
            }
            if (parent != null && !ContainsVertex(parent))
            {
                throw new ArgumentException("parent", "Parent has to be part of the tree");
            }

            this._backingGraph.AddVertex(node);

            ClearResults();
            PropageateDefaultAttributes();

            OnPropertyChanged(new PropertyChangedEventArgs("Vertices"));
            if (parent != null)
            {
                _backingGraph.AddEdge(new Edge(parent, node, andGroup));
                OnPropertyChanged(new PropertyChangedEventArgs("Edges"));
            }
            else
            {
                RootNode = node;
                OnPropertyChanged(new PropertyChangedEventArgs("RootNode"));
            }

            UpdateAttributes();
        }

        /// <summary>
        /// Connects an existing node to a new parent.
        /// </summary>
        /// <param name="node">The node to connect anew.</param>
        /// <param name="parent">The new parent of the node.</param>
        /// <exception cref="ArgumentException">When one of the arguments is invalid
        /// i.e. one of the nodes is <c>null</c> or not part of the tree.</exception>
        public void UpdateConnection(Node node, Node parent)
        {
            if (!ContainsVertex(node))
            {
                throw new ArgumentException("node", "Node has to be part of the tree");
            }
            if (!ContainsVertex(parent))
            {
                throw new ArgumentException("parent", "Parent node has to be part of tree");
            }

            // Find existing parent edge and remove
            if (_backingGraph.RemoveEdgeIf(edge => edge.Target == node) > 0)
                OnPropertyChanged(new PropertyChangedEventArgs("Edges"));

            // Add new connection
            _backingGraph.AddEdge(new Edge(parent, node));
            OnPropertyChanged(new PropertyChangedEventArgs("Vertices"));

            ClearResults();
            PropageateDefaultAttributes();
            UpdateAttributes();
        }

        /// <summary>
        /// Removes a node from the tree. The nodes children get connected to the parent
        /// of the node thats deleted.
        /// </summary>
        /// <param name="node">Node to remove.</param>
        public void RemoveNode(Node node)
        {
            if (node == null)
            {
                return;
            }
            if (!ContainsVertex(node))
            {
                throw new ArgumentException("node", "Node has to be part of the tree");
            }

            // Find parent and children
            var parent = FindParent(node);
            var children = FindChildren(node);

            // Remove the node and any edges that reference it
            _backingGraph.RemoveVertex(node);
            _backingGraph.RemoveEdgeIf(edge => edge.Source == node || edge.Target == node);

            // Reconnect children to parent of removed node
            foreach (var child in children)
            {
                _backingGraph.AddEdge(new Edge(parent, child));
            }
            OnPropertyChanged(new PropertyChangedEventArgs("Vertices"));
            OnPropertyChanged(new PropertyChangedEventArgs("Edges"));
            if (node == RootNode)
                OnPropertyChanged(new PropertyChangedEventArgs("RootNode"));

            ClearResults();
            PropageateDefaultAttributes();
            UpdateAttributes();
        }

        /// <summary>
        /// Creates a new tree that contains a sub-tree of this tree.
        /// </summary>
        /// <param name="subTreeRoot">Root node of the sub-tree to copy.</param>
        /// <returns>A new tree that has subTreeRoot as it's root node and the tree below.</returns>
        public Tree CopySubTreeAsNewTree(Node subTreeRoot)
        {
            var newTree = new Tree();

            // Copy the sub tree root itself
            Node copy = new Node(subTreeRoot.Name);
            foreach (var attribute in subTreeRoot.Attributes.Values)
            {
                if (attribute.Type == Functions.Types.Boolean)
                    copy.AddAttribute(attribute.Name, attribute.AsBool());
                else
                    copy.AddAttribute(attribute.Name, attribute.AsNumber());
            }
            newTree._backingGraph.AddVertex(copy);
            newTree.RootNode = copy;

            var andGroupLookup = new Dictionary<Guid, Guid>();

            // Recursively copy it's children
            var children = FindChildren(subTreeRoot);
            if (children.Count() > 0)
            {
                foreach (var child in children)
                {
                    newTree.CopySubTreeRec(this, child, copy, andGroupLookup);
                }
            }

            return newTree;
        }

        /// <summary>
        /// Copies a subtree to this tree.
        /// </summary>
        /// <param name="tree">The tree, the sub tree belongs to. Can be this tree or any other tree.</param>
        /// <param name="subTreeRoot">Root node of the sub tree to copy.</param>
        /// <param name="parent">Parent node to connect the copied sub tree to.</param>
        public void CopySubTree(Tree tree, Node subTreeRoot, Node parent)
        {
            var andGroupLookup = new Dictionary<Guid, Guid>();

            // Copy the sub tree
            CopySubTreeRec(tree, subTreeRoot, parent, andGroupLookup);

            // Clear function results and refresh
            ClearResults();
            UpdateAttributes();
        }

        private void CopySubTreeRec(Tree tree, Node subTreeRoot, Node parent, Dictionary<Guid, Guid> andGroupLookup)
        {
            // Copy the sub tree root itself
            Node copy = new Node(subTreeRoot.Name);
            foreach (var attribute in subTreeRoot.Attributes.Values)
            {
                if (attribute.Type == Functions.Types.Boolean)
                    copy.AddAttribute(attribute.Name, attribute.AsBool());
                else
                    copy.AddAttribute(attribute.Name, attribute.AsNumber());
            }
            this._backingGraph.AddVertex(copy);
            var andGroup = tree.GetAndGroup(subTreeRoot);
            if (andGroup == Guid.Empty)
            {
                _backingGraph.AddEdge(new Edge(parent, copy));
            }
            else
            {
                Guid newAndGroup;
                if (andGroupLookup.TryGetValue(andGroup, out newAndGroup))
                {
                    _backingGraph.AddEdge(new Edge(parent, copy, newAndGroup));
                }
                else
                {
                    newAndGroup = GetNextAndGroup();
                    _backingGraph.AddEdge(new Edge(parent, copy, newAndGroup));
                    andGroupLookup.Add(andGroup, newAndGroup);
                }
            }
            UpdateAttributes();

            // Recursively copy it's children
            var children = tree.FindChildren(subTreeRoot);
            if (children.Count() > 0)
            {
                foreach (var child in children)
                {
                    CopySubTreeRec(tree, child, copy, andGroupLookup);
                }
            }
        }

        /// <summary>
        /// Finds all child nodes of a given node.
        /// </summary>
        /// <param name="node">A node in this tree.</param>
        /// <returns>A collection of all nodes that are children of the given node.</returns>
        /// <exception cref="ArgumentNullException">When the given node is <c>null</c>.</exception>
        /// <exception cref="ArgumentException">When the given node isn't part of this tree.</exception>
        public IEnumerable<Node> FindChildren(Node node)
        {
            if (node == null)
            {
                throw new ArgumentNullException("node", "Node can't be null");
            }
            if (!ContainsVertex(node))
            {
                throw new ArgumentException("node", "Node has to be part of the tree");
            }

            var children = new List<Node>(_backingGraph.OutDegree(node));

            // Get all edges that start at the given node
            foreach (var edge in OutEdges(node))
            {
                // Their target nodes are th children of the node
                children.Add(edge.Target);
            }
            return children;
        }

        /// <summary>
        /// Finds all nodes in an AND-group.
        /// </summary>
        /// <param name="andGroup">And group to find.</param>
        /// <returns>An enumeration of nodes that all are part of the given
        /// AND-group.</returns>
        public IEnumerable<Node> FindAndGroupNodes(Guid andGroup)
        {
            var groupEdges = Edges.Where(e => e.AndGroup == andGroup);
            return groupEdges.Select(e => e.Target).Distinct();
        }

        /// <summary>
        /// Finds the parent node of a node in the tree.
        /// </summary>
        /// <param name="node">A node in the tree.</param>
        /// <returns>The parent of the given node or <c>null</c> if the
        /// given node is the root.</returns>
        /// <exception cref="ArgumentNullException">When the given node is
        /// <c>null</c>.</exception>
        /// <exception cref="ArgumentException">When the given node is not
        /// part of the tree.</exception>
        public Node FindParent(Node node)
        {
            if (node == null)
            {
                throw new ArgumentNullException("node", "Node can't be null");
            }
            if (!ContainsVertex(node))
            {
                throw new ArgumentException("node", "Node is not part of the tree");
            }

            // Simple case
            if (node == RootNode)
            {
                return null;
            }

            // Find edge to parent
            return InEdge(node, 0).Source;
        }

        /// <summary>
        /// Checks if a node is a leaf node.
        /// </summary>
        /// <remarks>
        /// If node isn't part of the tree just returns <c>false</c>.
        /// </remarks>
        /// <param name="node">Node to check.</param>
        /// <returns><c>true</c> if the node is a leaf, otherwise <c>false</c>.</returns>
        public bool IsLeaf(Node node)
        {
            // Gracefully handle nodes that are not part of the tree
            if (!ContainsVertex(node))
                return false;

            // Criteria is that node doesn't have any children/out edges
            return IsOutEdgesEmpty(node);
        }

        /// <summary>
        /// Gets the next free AND-group GUID.
        /// </summary>
        /// <returns>A GUID to use for a new AND-group.</returns>
        public Guid GetNextAndGroup()
        {
            // This looks horribly unefficient but shouldn't be
            // as the loop is only there to resolve collisions and
            // GUIDs are supposed to collide only in very very very
            // ... very rare cases
            Guid andGroup = Guid.NewGuid();
            while (_andGroups.Contains(andGroup))
            {
                andGroup = Guid.NewGuid();
            }
            return andGroup;
        }

        /// <summary>
        /// Gets the AND-group a node is part of.
        /// </summary>
        /// <param name="node">Node to get the AND-group for.</param>
        /// <returns>The nodes AND-group or <c>Guid.Empty</c> if the node
        /// isn't part of an AND-group.</returns>
        public Guid GetAndGroup(Node node)
        {
            // Check corner cases
            if (!Vertices.Contains(node))
                return Guid.Empty;
            if (node == RootNode)
                return Guid.Empty;

            var edge = Edges.Single(e => e.Target == node);
            return edge.AndGroup;
        }

        /// <summary>
        /// Clears all flags and attributes set/propageted by a previous function
        /// evaluation.
        /// </summary>
        public void ClearResults()
        {
            foreach (var node in Vertices)
            {
                // Clear the result marker
                node.IsResult = false;

                // If it's not a leaf: Clear attributes, too
                // as non-leaf nodes only get their attributes
                // by evaluating a function
                if (!IsLeaf(node))
                {
                    node.ClearAttributes();
                }
            }
        }

        /// <summary>
        /// Triggers an update of all node attributes. Needed in some cases
        /// with non trivial attribute updates to fire PropertyChanged events.
        /// </summary>
        public void UpdateAttributes()
        {
            foreach (var node in Vertices)
            {
                node.UpdateAttributes();
            }
            OnPropertyChanged(new PropertyChangedEventArgs("Vertices"));
        }

        public bool AddDefaultAttribute(NodeAttribute attribute)
        {
            if (!DefaultAttributes.ContainsKey(attribute.Name))
            {
                ClearResults();

                DefaultAttributes.Add(attribute.Name, attribute);
                foreach (var leaf in Leafs)
                {
                    if (!leaf.Attributes.ContainsKey(attribute.Name))
                    {
                        if (attribute.Type == Functions.Types.Boolean)
                            leaf.AddAttribute(attribute.Name, attribute.AsBool());
                        else
                            leaf.AddAttribute(attribute.Name, attribute.AsNumber());
                    }
                }
                UpdateAttributes();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Removes a default attribute from the tree (if it existed).
        /// </summary>
        /// <param name="name">Name of the attribute to remove.</param>
        /// <returns><c>true</c> if the attribute existed and was removed.</returns>
        public bool RemoveDefaultAttribute(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                return false;

            if (DefaultAttributes.Remove(name))
            {
                ClearResults();
                foreach (var leaf in Leafs)
                {
                    if (leaf.Attributes.ContainsKey(name))
                    {
                        leaf.Attributes.Remove(name);
                        leaf.UpdateAttributes();
                    }
                }
                UpdateAttributes();
                return true;
            }

            return false;
        }

        private void PropageateDefaultAttributes()
        {
            foreach (var leaf in Leafs)
            {
                foreach (var attribute in DefaultAttributes.Values)
                {
                    if (!leaf.Attributes.ContainsKey(attribute.Name))
                    {
                        if (attribute.Type == Functions.Types.Boolean)
                            leaf.AddAttribute(attribute.Name, attribute.AsBool());
                        else
                            leaf.AddAttribute(attribute.Name, attribute.AsNumber());
                    }
                }
            }
        }

        /// <summary>
        /// Gets all the nodes attributes on the tree
        /// </summary>
        /// <returns>a Dictionary containing every NodeAttribute just once</returns>
        public Dictionary<string, NodeAttribute> GetNodeAttributes()
        {
            Dictionary<string, NodeAttribute> Attributes = new Dictionary<string, NodeAttribute>();
            //iterate over everyleaf
            foreach (Node leafNode in this.Leafs)
            {

                //check which attributes a node has got
                foreach (KeyValuePair<string, NodeAttribute> entry in leafNode.Attributes)
                {
                    //check if we already saved this attribute
                    if (!(Attributes.ContainsKey(entry.Key)))
                    {
                        Attributes.Add(entry.Key, entry.Value);
                    }
                }
            }
            return Attributes;
        }

        /// <summary>
        /// Gets a list of all the attributes which are contained in the tree
        /// </summary>
        /// <returns>A List containing all of the attributes contained in the tree</returns>
        public List<string> GetNodeAttributesAsList()
        {
            List<string> nodeAttrList = new List<string>();
            foreach (KeyValuePair<string, NodeAttribute> entry in GetNodeAttributes())
            {
                nodeAttrList.Add(entry.Key);
            }
            return nodeAttrList;
        }

        /// <summary>
        /// Gets the level a node is at.
        /// </summary>
        /// <param name="n">Node to check.</param>
        /// <returns>The nodes level in the tree (distance to root) or
        /// -1 if the node isn't part of the tree.</returns>
        public int GetNodeLevel(Node n)
        {
            // Special cases
            if (!ContainsVertex(n))
                return -1;

            // Regular case
            var parent = FindParent(n);
            var level = 0;
            while (parent != null)
            {
                level++;
                parent = FindParent(parent);
            }
            return level;
        }

        /// <summary>
        /// Gets the maximum node level for al nodes
        /// in the tree.
        /// </summary>
        /// <returns>The maximum node level.</returns>
        public int GetMaxNodeLevel()
        {
            var maxLevel = 0;
            foreach (var leaf in Leafs)
            {
                var level = GetNodeLevel(leaf);
                maxLevel = Math.Max(level, maxLevel);
            }
            return maxLevel;
        }


        /// <summary>
        /// Collapses all nodes from a selected level downwards (towards the leafs).
        /// </summary>
        /// <param name="level">Level to start collapsing at.</param>
        public void CollapseLevel(int level)
        {
            foreach (var node in Vertices)
                if (GetNodeLevel(node) >= level)
                    node.IsExpanded = false;
        }

        /// <summary>
        /// Expands all nodes from a selected level downwards (towards the leafs).
        /// </summary>
        /// <param name="level">Level to start expanding at.</param>
        public void ExpandLevel(int level)
        {
            foreach (var node in Vertices)
                if (GetNodeLevel(node) >= level)
                    node.IsExpanded = true;
        }

        #endregion

        #region Methods - Implementation of IBidirectionalGraph

        /// <summary>
        /// Tests if two nodes in the tree are connected by an edge.
        /// </summary>
        /// <param name="source">Source node (parent) of the edge.</param>
        /// <param name="target">Target node (child) of the edge.</param>
        /// <returns><c>true</c> if the two given nodes are connected.</returns>
        public bool ContainsEdge(Node source, Node target)
        {
            return _backingGraph.ContainsEdge(source, target);
        }

        /// <summary>
        /// Tests if two nodes in the tree are connected by an edge and
        /// returns the edge on success.
        /// </summary>
        /// <param name="source">Source node (parent) of the edge.</param>
        /// <param name="target">Target node (child) of the edge.</param>
        /// <param name="edge">After the method returns, contains the edge that
        /// connects the given nodes.</param>
        /// <returns><c>true</c> if the two given nodes are connected.</returns>
        public bool TryGetEdge(Node source, Node target, out Edge edge)
        {
            return _backingGraph.TryGetEdge(source, target, out edge);
        }

        /// <summary>
        /// Implemented as part of <see cref="IVertexAndEdgeListGraph"/> interface. Doesn't
        /// actually make sense for trees (will always return one edge at max).
        /// </summary>
        /// <param name="source">Source node (parent) of the edge.</param>
        /// <param name="target">Target node (child) of the edge.</param>
        /// <param name="edge">After the method returns, contains the edge(es) that
        /// connect the given nodes.</param>
        /// <returns><c>true</c> if the two given nodes are connected.</returns>
        public bool TryGetEdges(Node source, Node target, out IEnumerable<Edge> edges)
        {
            return _backingGraph.TryGetEdges(source, target, out edges);
        }

        /// <summary>
        /// Tests if a node is a leaf (has no out edges).
        /// </summary>
        /// <param name="v">The node to test.</param>
        /// <returns><c>true</c> if the given node has no out edges (is a leaf).</returns>
        public bool IsOutEdgesEmpty(Node v)
        {
            return _backingGraph.IsOutEdgesEmpty(v);
        }

        /// <summary>
        /// Gets the number of outgoing edges a node has.
        /// </summary>
        /// <param name="v">The node to get the number of outgoing edges for.</param>
        /// <returns>The number of outgoing edges for the given node.</returns>
        public int OutDegree(Node v)
        {
            return _backingGraph.OutDegree(v);
        }

        /// <summary>
        /// Gets a specific outgoing edge for a node.
        /// </summary>
        /// <param name="v">The node the edge starts at.</param>
        /// <param name="index">The index of the edge to get (has to be in range [0, OutDegree)).</param>
        /// <returns>The selected edge or <c>null</c> if no such edge exists.</returns>
        public Edge OutEdge(Node v, int index)
        {
            return _backingGraph.OutEdge(v, index);
        }

        public IEnumerable<Edge> OutEdges(Node v)
        {
            return _backingGraph.OutEdges(v);
        }

        public bool TryGetOutEdges(Node v, out IEnumerable<Edge> edges)
        {
            return _backingGraph.TryGetOutEdges(v, out edges);
        }

        public bool ContainsVertex(Node vertex)
        {
            return _backingGraph.ContainsVertex(vertex);
        }

        public bool ContainsEdge(Edge edge)
        {
            return _backingGraph.ContainsEdge(edge);
        }

        public int Degree(Node v)
        {
            return _backingGraph.Degree(v);
        }

        public int InDegree(Node v)
        {
            return _backingGraph.InDegree(v);
        }

        public Edge InEdge(Node v, int index)
        {
            return _backingGraph.InEdge(v, index);
        }

        public IEnumerable<Edge> InEdges(Node v)
        {
            return _backingGraph.InEdges(v);
        }

        public bool IsInEdgesEmpty(Node v)
        {
            return _backingGraph.IsInEdgesEmpty(v);
        }

        public bool TryGetInEdges(Node v, out IEnumerable<Edge> edges)
        {
            return _backingGraph.TryGetInEdges(v, out edges);
        }

        #endregion

        #region Methods - Implementation of INotifyPropertyChanged

        /// <summary>
        /// Invokes the PropertyChanged event.
        /// </summary>
        /// <param name="args">Event arguments.</param>
        protected virtual void OnPropertyChanged(PropertyChangedEventArgs args)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, args);
        }

        #endregion

        #region Methods - Implementation of IClonable

        /// <summary>
        /// Creates a deep clone of this tree.
        /// </summary>
        /// <returns>A new tree that is a deep clone of this tree.</returns>
        public object Clone()
        {
            var clone = new Tree();
            clone.Name = this.Name;
            clone.Description = this.Description;
            clone.HasChanged = false;

            // Copy nodes
            var nodeLookup = new Dictionary<Node, Node>(this.VertexCount);
            foreach (var node in this.Vertices)
            {
                var nodeClone = (Node)node.Clone();
                clone._backingGraph.AddVertex(nodeClone);

                // Keep a lookup table of old to new nodes for reconstructing
                // the edges
                nodeLookup.Add(node, nodeClone);
            }

            // Copy root node association
            clone.RootNode = nodeLookup[this.RootNode];

            // Copy edges
            var andGroupLookup = new Dictionary<Guid, Guid>();
            andGroupLookup.Add(Guid.Empty, Guid.Empty);
            foreach (var edge in this.Edges)
            {
                // Keep track of and groups
                Guid oldAndGroup = edge.AndGroup;
                Guid newAndGroup;
                if (!andGroupLookup.TryGetValue(oldAndGroup, out newAndGroup))
                {
                    newAndGroup = clone.GetNextAndGroup();
                    andGroupLookup.Add(oldAndGroup, newAndGroup);
                }

                clone._backingGraph.AddEdge(new Edge(nodeLookup[edge.Source], nodeLookup[edge.Target], newAndGroup));
            }

            return clone;
        }

        #endregion

    }

}
