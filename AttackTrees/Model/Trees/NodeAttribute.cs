﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.Functions;
using System.ComponentModel;

namespace Model.Trees
{

    /// <summary>
    /// Defines a single attribute of a node. Attributes are key value
    /// pairs that allow can be attached to a node.
    /// </summary>
    public class NodeAttribute : INotifyPropertyChanged
    {

        #region Constructors & Finalizers

        /// <summary>
        /// Initializes a new instance of class NodeAttribute.
        /// </summary>
        /// <param name="id">Attribute guid.</param>
        /// <param name="name">Attribute name.</param>
        /// <param name="type">Attribute value type.</param>
        /// <param name="value">Attribute value as string.</param>
        public NodeAttribute(Guid id, string name, Types type, string value)
        {
            this.Id = id;
            this.Name = name;
            this.Type = type;
            this.Value = value;
        }

        /// <summary>
        /// Initializes a new instance of class NodeAttribute.
        /// </summary>
        /// <param name="name">Attribute name.</param>
        /// <param name="type">Attribute value type.</param>
        /// <param name="value">Attribute value as string.</param>
        public NodeAttribute(string name, Types type, string value)
        {
            this.Id = Guid.NewGuid();
            this.Name = name;
            this.Type = type;
            this.Value = value;
        }

        /// <summary>
        /// Initializes a new instance of class NodeAttribute with
        /// a numeric value.
        /// </summary>
        /// <param name="name">Attribute name.</param>
        /// <param name="value">Attribute value.</param>
        public NodeAttribute(string name, Decimal value)
        {
            this.Id = Guid.NewGuid();
            this.Name = name;
            this.Type = Types.Number;
            this.Value = value.ToString();
        }

        /// <summary>
        /// Initializes a new instance of class NodeAttribute with
        /// a numeric value.
        /// </summary>
        /// <param name="name">Attribute name.</param>
        /// <param name="value">Attribute value.</param>
        public NodeAttribute(string name, bool value)
        {
            this.Id = Guid.NewGuid();
            this.Name = name;
            this.Type = Types.Boolean;
            this.Value = value.ToString();
        }

        #endregion

        #region Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Properties


        /// <summary>
        /// Gets the attributes id.
        /// </summary>
        public Guid Id { get; private set; }

        /// <summary>
        /// Gets the attributes name.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the type of data stored.
        /// </summary>
        public Types Type { get; private set; }

        /// <summary>
        /// Gets the attributes value in string form as stored
        /// in the repository.
        /// </summary>
        public string Value { get; private set; }

        /// <summary>
        /// Returns TRUE if current Type is a boolean
        /// </summary>
        public bool IsBoolean
        {
            get
            {
                return this.Type == Types.Boolean;
            }
        }
        
        /// <summary>
        /// Gets or sets the attribute value as a boolean.
        /// </summary>
        public bool BoolValue
        {
            get { return AsBool(); }
            set
            {
                SetValue(value);
            }
        }

        /// <summary>
        /// Gets or sets the attribute value as a number.
        /// </summary>
        public decimal NumberValue
        {
            get { return AsNumber(); }
            set
            {
                SetValue(value);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the attribute value as a number. If <c>Type</c> is
        /// <see cref="Types.Boolean"/> the value is converted
        /// to <see cref="Decimal.Zero"/> if the value is <c>false</c> or
        /// <see cref="Decimal.One"/> if the value is <c>true</c>.
        /// </summary>
        /// <returns>The numeric value of this attribute.</returns>
        public Decimal AsNumber()
        {
            switch (Type)
            {
                case Types.Number:
                    return Decimal.Parse(Value);

                case Types.Boolean:
                    return (Boolean.Parse(Value) ? Decimal.One : Decimal.Zero);
            }
            throw new InvalidOperationException("Can't convert from type " + Type + " to " + Types.Number);
        }

        /// <summary>
        /// Gets the attribute value as a boolean flag. If <c>Type</c> is
        /// <see cref="Types.Number"/> the value is converted
        /// to <c>false</c> if the value is <see cref="Decimal.Zero"/> or
        /// <c>true</c> if not.
        /// </summary>
        /// <returns>The boolean value of this attribute.</returns>
        public bool AsBool()
        {
            switch (Type)
            {
                case Types.Number:
                    return (Decimal.Parse(Value) != Decimal.Zero);

                case Types.Boolean:
                    return Boolean.Parse(Value);
            }
            throw new InvalidOperationException("Can't convert from type " + Type + " to " + Types.Boolean);
        }

        /// <summary>
        /// Sets the attributes value. If the attributes type is not
        /// <see cref="Types.Number"/>, value conversion
        /// is performed the same way as for <see cref="NodeAttribute.AsBool()"/>.
        /// </summary>
        /// <param name="value">A new value.</param>
        public void SetValue(Decimal value)
        {
            if (Type == Types.Number)
                Value = value.ToString();
            else
                Value = (value == Decimal.Zero) ? false.ToString() : true.ToString();

            OnPropertyChanged(new PropertyChangedEventArgs("Value"));
            OnPropertyChanged(new PropertyChangedEventArgs("BoolValue"));
            OnPropertyChanged(new PropertyChangedEventArgs("NumberValue"));
        }

        /// <summary>
        /// Sets the attributes value. If the attributes type is not
        /// <see cref="Types.Boolean"/>, value conversion
        /// is performed the same way as for <see cref="NodeAttribute.AsNumber()"/>.
        /// </summary>
        /// <param name="value">A new value.</param>
        public void SetValue(bool value)
        {
            if (Type == Types.Number)
                Value = value ? Decimal.One.ToString() : Decimal.Zero.ToString();
            else
                Value = value.ToString();

            OnPropertyChanged(new PropertyChangedEventArgs("Value"));
            OnPropertyChanged(new PropertyChangedEventArgs("BoolValue"));
            OnPropertyChanged(new PropertyChangedEventArgs("NumberValue"));
        }

        /// <summary>
        /// Gets a string representation of the attribute as name: value pair.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Name + ": " + Value;
        }

        /// <summary>
        /// Fires the PropertyChanged event.
        /// </summary>
        /// <param name="args"></param>
        protected virtual void OnPropertyChanged(PropertyChangedEventArgs args)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, args);
        }

        #endregion

    }

}
