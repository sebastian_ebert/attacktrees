﻿namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
using Model.Functions;

    /// <summary>
    /// Repository to access/store functions in a persistent way.
    /// </summary>
    public interface IFunctionRepository
    {

        bool SaveOrUpdate(Function function);

        bool Delete(Function function);

        Function GetById(Guid id);

        Function FindByName(string name);

        List<Function> FindAllByName(string pattern);

        IEnumerable<Function> FindByAttributes(IEnumerable<string> attributeNames);

    }
}
