﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.Trees;
using Model;
using System.Security.Cryptography;

namespace Model
{

    public class UserSession
    {
        #region Properties

        public Tree CurrentTree;

        public IUserRepository UserRepository    
        {
            get;
            set;
        }

        public ITreeRepository TreeRepository
        {
            get;
            set;
        }

        public IFunctionRepository FuncRepository
        {
            get;
            set;
        }

        public List<Tree> LoadedTrees
        {
            get;
            set;
        }

        public User CurrentUser
        {
            get;
            set;
        }

        #endregion

        public UserSession(IUserRepository userRepo, ITreeRepository treeRepo, IFunctionRepository functionsRepo)
        {
            if (userRepo != null && treeRepo != null && functionsRepo != null)
            {
                this.UserRepository = userRepo;
                this.TreeRepository = treeRepo;
                this.FuncRepository = functionsRepo;
            }
            else { 
            //Throw some exception
            }
        }

        #region Methods

        /// <summary>
        /// Creates a new empty tree
        /// </summary>
        /// <returns></returns>
        public Tree CreateTree()
        {
            LoadedTrees.Add(new Tree());
            CurrentTree = LoadedTrees.ElementAt(LoadedTrees.Count - 1);
            return CurrentTree;
        }

        /// <summary>
        /// Deletes the selected tree
        /// </summary>
        /// <param name="treeId"></param>
        /// <returns></returns>
        public bool DeleteTree(string treeId)
        {
            //check if there are oder loaded trees
            if (LoadedTrees.Count > 1)
            {
                LoadedTrees.Remove(CurrentTree);
                CurrentTree = LoadedTrees.ElementAt(LoadedTrees.Count - 1);
                return true;
            }
            else {
                LoadedTrees.Clear();
                CurrentTree = new Tree();
                LoadedTrees.Add(CurrentTree);
                return true;
            }
        }

        public Tree GetByAttributes()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Searches for a tree which matches the id
        /// </summary>
        /// <param name="treeId"></param>
        /// <returns></returns>
        public Tree GetById(Guid treeId)
        {
           return TreeRepository.GetById(treeId);
        }

        /// <summary>
        /// Searches for a tree which matches the name
        /// </summary>
        /// <param name="treeName"></param>
        /// <returns></returns>
        public Tree GetByName(string treeName)
        {
            
            foreach (var tree in LoadedTrees)
            {
                //ToDo insert tree name
                if (treeName.Equals(""))
                {
                    return tree;
                }
            }
            return null;
        }

        /// <summary>
        /// Logs a user in
        /// </summary>
        /// <param name="username">the user's username</param>
        /// <param name="password">the user's password</param>
        /// <returns>a user object if succedded or null if fails</returns>
        public User Login(string username, string password)
        {
            User tempUser = null;
            if (UserRepository.UserExists(username))
            {
                tempUser = this.UserRepository.ReadUser(username);
                string tempPassHash = CreatePasswordHash(password + tempUser.Salt);
                //if correct return user
                if (tempPassHash.Equals(tempUser.Password))
                {
                    this.CurrentUser = tempUser;
                    this.CurrentUser.Rights = this.UserRepository.GetPermissions(CurrentUser);
                    return this.CurrentUser;
                }
                else
                {
                    return null;
                }
            }
            
            //Returning an user to UI in case it doesnt exist 
            else 
            {
                tempUser = new User();
                tempUser.Username = username;
                tempUser.Salt = "notExist";
                return tempUser;
            }
        }

        public bool Logout(User user)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Creates a new user
        /// </summary>
        /// <param name="username">the user's username</param>
        /// <param name="firstname">The user's first name</param>
        /// <param name="lastname">The user's last name</param>
        /// <param name="password">The user's password</param>
        /// <returns>A user instance if created succeedded or null if not</returns>
        public User CreateUser(string username, string firstname, string lastname, string password)
        {
            User newUser = new User();
            newUser.Username = username;
            newUser.FirstName = firstname;
            newUser.LastName = lastname;
            newUser.Salt = CreateSalt();
            Console.WriteLine("User Salt: "+newUser.Salt);
            newUser.Password = CreatePasswordHash(password + newUser.Salt);
            Console.WriteLine("User Pass: "+ newUser.Password);

            //save the user instance in the database
            bool userCU = this.UserRepository.CreateUpdateUser(newUser);

            if (userCU)
            {
                return newUser;
            }
            else
            {
                return null;
            }
            
        }

        /// <summary>
        /// Creates a random salt
        /// code from Stackoverflow
        /// </summary>
        /// <returns>A string representation of the byte array</returns>
        private static string CreateSalt()
        {
            //Generate a cryptographic random number.
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] buff = new byte[10];
            rng.GetBytes(buff);

            // Return a Base64 string representation of the random number.
            return Convert.ToBase64String(buff);
        }

        /// <summary>
        /// Creates a SHA256 hash from a password string
        /// code from Stackoverflow
        /// </summary>
        /// <param name="password">the user's password</param>
        /// <returns></returns>
        private static string CreatePasswordHash(string password)
        {
             Byte[] inputBytes = Encoding.UTF8.GetBytes(password);
             Byte[] hashedBytes = (new SHA256CryptoServiceProvider()).ComputeHash(inputBytes);
             return BitConverter.ToString(hashedBytes); ;
        }

        /// <summary>
        /// Saves or updates a tree in the database
        /// </summary>
        /// <param name="tree">the tree which should be saved</param>
        /// <returns>true if succedded or false if failed</returns>
        public bool SaveOrUpdateTree(Tree tree)
        {
            return TreeRepository.SaveOrUpdate(tree); ;
        }

        /// <summary>
        /// Loads a tree from the database
        /// </summary>
        /// <param name="treeId">The id of the tree you want to load</param>
        /// <returns>a tree if succedded or null if failed</returns>
        public Tree LoadTree(string treeId)
        {
            Tree loadingTree = null;
            //ToDo request to DB
            Guid treeGuid =new Guid(treeId);
            loadingTree = TreeRepository.GetById(treeGuid);
            if (loadingTree != null)
            {
                LoadedTrees.Add(loadingTree);
            }
            return loadingTree;
        }

        public List<Tree> GetViewableTrees(List<Tree> trees)
        {
            List<Tree> allTrees = new List<Tree>();
            if (this.CurrentUser.Rights[0].Equals("admin"))
            {
                return trees;
            }
            if (this.CurrentUser.Rights[0].Equals("dev"))
            {
                foreach (Tree tree in trees)
                {
                    //using the first element of the array
                    if (this.TreeRepository.HasPermission(tree, "dev"))
                    {
                        allTrees.Add(tree);
                    }
                }
                return allTrees;
            }
            if (this.CurrentUser.Rights[0].Equals("sup"))
            {
                foreach (Tree tree in trees)
                {
                    //using the first element of the array
                    if (this.TreeRepository.HasPermission(tree, "sup") || this.TreeRepository.HasPermission(tree, "dev"))
                    {
                        allTrees.Add(tree);
                    }
                }
                return allTrees;
            }
            return allTrees;
        }

        public List<Tree> GetAllTrees()
        {
            return GetViewableTrees(TreeRepository.GetAllTrees());
        }

        /// <summary>
        /// Check if a tree already exists in the DB
        /// </summary>
        /// <param name="id"></param>
        /// <returns>True if it exists / false if not</returns>
        public bool TreeExists(Guid id)
        {
            return TreeRepository.TreeExists(id);
        }


        /// <summary>
        /// Checks if a user is allowed to view a tree.
        /// </summary>
        /// <param name="tree">Tree to check.</param>
        /// <param name="user">User to check.</param>
        /// <returns><c>true</c> if the user is allowed to view the tree.</returns>
        public bool CanView(Tree tree, User user)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Checks if a user is allowed to edit a tree.
        /// </summary>
        /// <param name="tree">Tree to check.</param>
        /// <param name="user">User to check.</param>
        /// <returns><c>true</c> if the user is allowed to edit the tree.</returns>
        public bool CanEdit(Tree tree, User user)
        {
            throw new NotImplementedException();
        }

        #endregion

    }
}
