﻿using System.Collections.Generic;
using DbConnection.Model;
using DbConnection.Persistence;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MySql.Data.MySqlClient;

namespace DbConnectTests {
    
    [TestClass]
    public class TreeRepositoryTest {

        /// <summary>
        /// Creates a new repository and (re-)creates the schema. So the db always starts out
        /// empty after this.
        /// </summary>
        /// <returns></returns>
        private PersistentTreeRepository CreateRepository() {
            var repo = new PersistentTreeRepository("Server=localhost;Database=dbconnect;User ID=root;Password=abcd");
            repo.CreateSchema();
            return repo;
        }

        private PersistentTreeRepository CreateSampleRepo() {
            var repo = CreateRepository();
            var tree = new Tree("TestTree");
            tree.Description = "A tree to test out basic repository operations";
            repo.AddTree(tree);

            return repo;
        }


        [ClassInitialize]
        public static void Initialize(TestContext context) {
            var connectionString = "Server=localhost;User ID=root;Password=abcd";
            var connection = new MySqlConnection(connectionString);
            try {
                var command = connection.CreateCommand();
                connection.Open();
                command.CommandText = "CREATE DATABASE IF NOT EXISTS dbconnect";
                command.ExecuteNonQuery();
            } finally {
                connection.Close();
            }
        }


        [TestMethod]
        public void CanCreateRepo() {
            // Just create a new repository
            var repo = new PersistentTreeRepository("Server=localhost;Database=dbconnect;User ID=root;Password=abcd");
        }

        [TestMethod]
        public void CanAddNewTree() {
            // Create a new repository
            var repo = CreateRepository();

            // Create a new tree
            var tree = new Tree("TestTree");
            tree.Description = "A tree to test out basic repository operations";

            // Store in repo
            Assert.IsTrue(repo.AddTree(tree));

            using (var session = repo.SessionFactory.OpenSession()) {
                var treeInDb = session.Get<Tree>(tree.Id);
                Assert.IsNotNull(treeInDb);
                Assert.AreEqual(tree.Id, treeInDb.Id);
                Assert.AreEqual(tree.Name, treeInDb.Name);
                Assert.AreEqual(tree.Description, treeInDb.Description);
            }
        }

        [TestMethod]
        public void CanDeleteTree() {
            // Create a new repository
            var repo = CreateRepository();

            // Create and store a new tree
            var tree = new Tree("TestTree");
            tree.Description = "A tree to test out basic repository operations";
            repo.AddTree(tree);

            Assert.IsTrue(repo.DeleteTree(tree));

            using (var session = repo.SessionFactory.OpenSession()) {
                var treeInDb = session.Get<Tree>(tree.Id);
                Assert.IsNull(treeInDb);
            }
        }

        [TestMethod]
        public void CanUpdateTree() {
            // Create a new repository
            var repo = CreateRepository();

            // Create and store a new tree
            var tree = new Tree("TestTree");
            tree.Description = "A tree to test out basic repository operations";
            repo.AddTree(tree);

            tree.Name = "UpdateTestTree";
            Assert.IsTrue(repo.UpdateTree(tree));

            using (var session = repo.SessionFactory.OpenSession()) {
                var treeInDb = session.Get<Tree>(tree.Id);
                Assert.IsNotNull(treeInDb);
                Assert.AreEqual(tree.Name, treeInDb.Name);
            }
        }

        [TestMethod]
        public void CanGetTreeById() {
            // Create a new repository
            var repo = CreateRepository();

            // Create and store a new tree
            var tree = new Tree("TestTree");
            tree.Description = "A tree to test out basic repository operations";
            repo.AddTree(tree);

            var treeInRepo = repo.GetTreeById(tree.Id);
            Assert.IsNotNull(treeInRepo);
            Assert.AreEqual(tree.Id, treeInRepo.Id);
            Assert.AreEqual(tree.Name, treeInRepo.Name);
            Assert.AreEqual(tree.Description, treeInRepo.Description);
        }

        [TestMethod]
        public void CanGetTreeByName() {
            // Create a new repository
            var repo = CreateRepository();

            // Create and store a new tree
            var tree = new Tree("TestTree");
            tree.Description = "A tree to test out basic repository operations";
            repo.AddTree(tree);

            var treeInRepo = repo.GetTreeByName(tree.Name);
            Assert.IsNotNull(treeInRepo);
            Assert.AreEqual(tree.Id, treeInRepo.Id);
            Assert.AreEqual(tree.Name, treeInRepo.Name);
            Assert.AreEqual(tree.Description, treeInRepo.Description);
        }

        [TestMethod]
        public void CanGetTreesByNamePattern() {
            // Create a new repository
            var repo = CreateRepository();

            // Create and store some new trees
            var treesToMatch = new List<Tree>() {
                new Tree("TestTree1"),
                new Tree("TestTree2"),
                new Tree("TestTree3")
            };
            var treesNotToMatch = new List<Tree>() {
                new Tree("ATree"),
                new Tree("AnotherTree"),
                new Tree("OneMoreTree")
            };

            // Store test trees
            using (var session = repo.SessionFactory.OpenSession()) {
                using (var transaction = session.BeginTransaction()) {
                    foreach (var tree in treesToMatch) {
                        session.Save(tree);
                    }
                    foreach (var tree in treesNotToMatch) {
                        session.Save(tree);
                    }
                    transaction.Commit();
                }
            }

            var matchedTrees = repo.GetTreesByName("TestTree%");

            Assert.AreEqual(treesToMatch.Count, matchedTrees.Count);
            foreach (var tree in matchedTrees) {
                Assert.IsTrue(treesToMatch.Exists(t => t.Id == tree.Id));
            }
        }

    }

}
