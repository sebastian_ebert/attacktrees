﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using MySql.Data.MySqlClient;


namespace DbConnect.Tests {

    [TestClass]
    public class XmlMappingTests {

        private Configuration Configure() {
            // Configure DB connection
            var config = new Configuration();
            var properties = new Dictionary<string, string>() {
                { "hibernate.connection.provider", "NHibernate.Connection.DriverConnectionProvider" },
                { "hibernate.connection.driver_class", "NHibernate.Driver.MySqlDataDriver" },
                { "hibernate.dialect", "NHibernate.Dialect.MySQLDialect" },
                { "hibernate.connection.connection_string", "Server=localhost;Database=dbconnect;User ID=root;Password=abcd" },
                { "hibernate.show_sql", "true" }
            };
            config.AddProperties(properties);
            config.Configure();
            return config;
        }

        private static void CreateMapping(Configuration config) {
            config.AddAssembly(typeof(DbConnection.Model.Tree).Assembly);
        }


        [ClassInitialize]
        public static void Initialize(TestContext context) {
            var connectionString = "Server=localhost;User ID=root;Password=abcd";
            var connection = new MySqlConnection(connectionString);
            try {
                var command = connection.CreateCommand();
                connection.Open();
                command.CommandText = "CREATE DATABASE IF NOT EXISTS dbconnect";
                command.ExecuteNonQuery();
            } finally {
                connection.Close();
            }
        }


        [TestMethod]
        public void CanConfigureHibernate() {
            // Configure DB connection
            var config = Configure();

            // Configure mapping
            CreateMapping(config);

            // Check resulting mapping
            config.BuildMappings(); // Just to be sure ...
            Assert.AreEqual(4, config.ClassMappings.Count);
            Assert.AreEqual(3, config.CollectionMappings.Count);
        }

        [TestMethod]
        public void CanCreateSchemaFromMapping() {
            // Configure DB connection and mapping
            var config = Configure();
            CreateMapping(config);

            // Test schema export
            var export = new SchemaExport(config);
            export.Execute(true, false, false);
        }

    }
}
