﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DbConnection.Model;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;

namespace DbConnection.Mapping {

    public class EdgeMapping : ClassMapping<Edge> {

        public EdgeMapping() {
            Id<int>(obj => obj.Id, mapping => mapping.Generator(Generators.Native));
            ManyToOne<Node>(
                obj => obj.ParentNode,
                mapping => {
                    mapping.Cascade(Cascade.None);
                    mapping.ForeignKey("EdgeParentNode");
                }
            );
            List<Node>(
                obj => obj.ChildNodes,
                mapping => {
                    mapping.Cascade(Cascade.None);
                    mapping.Index(index => index.Column("IndexInParentEdge"));
                },
                list => list.OneToMany(
                    mapping => mapping.Class(typeof(Node))
                )
            );
        }

    }

}
