﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DbConnection.Model;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;

namespace DbConnection.Mapping {

    public class TreeMapping : ClassMapping<Tree> {

        public TreeMapping() {
            Id<int>(obj => obj.Id, mapping => mapping.Generator(Generators.Native));
            Property<string>(obj => obj.Name);
            Property<string>(obj => obj.Description);
            ManyToOne<Node>(obj => obj.RootNode, mapping => {
                mapping.Cascade(Cascade.None);
                mapping.ForeignKey("TreeRoot");
            });
        }

    }

}
