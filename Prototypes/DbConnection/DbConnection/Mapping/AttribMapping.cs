﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DbConnection.Model;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;

namespace DbConnection.Mapping {

    public class AttribMapping : ClassMapping<Attrib> {

        public AttribMapping() {
            Id<int>(obj => obj.Id, mapping => mapping.Generator(Generators.Native));
            Property<string>(obj => obj.Name);
            Property<string>(obj => obj.Value);
            ManyToOne<Node>(
                obj => obj.Owner,
                mapping => {
                    mapping.Cascade(Cascade.None);
                    mapping.ForeignKey("OwnerNode");
                }
            );
        }

    }

}
