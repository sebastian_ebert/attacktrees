﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DbConnection.Model;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;

namespace DbConnection.Mapping {
    
    public class NodeMapping : ClassMapping<Node> {

        public NodeMapping() {
            Id<int>(obj => obj.Id, mapping => mapping.Generator(Generators.Native));
            Property<string>(obj => obj.Name);
            ManyToOne<Edge>(
                obj => obj.ParentEdge,
                mapping => {
                    mapping.Cascade(Cascade.None);
                    mapping.ForeignKey("NodeParentEdge");
                }
            );
            List<Edge>(
                obj => obj.ChildEdges,
                mapping => {
                    mapping.Cascade(Cascade.None);
                    mapping.Index(index => index.Column("IndexInParentNode"));
                },
                list => list.OneToMany(
                    mapping => {
                        mapping.Class(typeof(Edge));
                    }
                )
            );
            List<Attrib>(
                obj => obj.Attributes,
                mapping => {
                    mapping.Cascade(Cascade.None);
                    mapping.Index(index => index.Column("IndexInOwningNode"));
                },
                list => list.OneToMany(
                    mapping => mapping.Class(typeof(Attrib))
                )
            );
        }

    }

}
