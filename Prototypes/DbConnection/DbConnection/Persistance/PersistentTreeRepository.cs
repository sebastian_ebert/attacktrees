﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DbConnection.Model;
using NHibernate.Cfg;
using NHibernate;
using NHibernate.Mapping.ByCode;
using NHibernate.Tool.hbm2ddl;
using NHibernate.Criterion;

namespace DbConnection.Persistence {
    
    /// <summary>
    /// Persistent implementation of <see cref="T:DbConnect.Model.ITreeRepository"/>. Stores
    /// trees in a database.
    /// </summary>
    public class PersistentTreeRepository : ITreeRepository {

        private Configuration dbConfig;
        private ISessionFactory sessionFactory;

        public ISessionFactory SessionFactory {
            get {
                if (sessionFactory == null) {
                    sessionFactory = dbConfig.BuildSessionFactory();
                }
                return sessionFactory;
            }
        }


        /// <summary>
        /// Initialize a new PersistantTreeRepository from a DB connection string.
        /// </summary>
        /// <param name="connectionString">A DB connection string for the database to use
        /// for the repositiory.</param>
        public PersistentTreeRepository(string connectionString) {
            // Configure DB connection
            dbConfig = new Configuration();
            var properties = new Dictionary<string, string>() {
                { "hibernate.connection.provider", "NHibernate.Connection.DriverConnectionProvider" },
                { "hibernate.connection.driver_class", "NHibernate.Driver.MySqlDataDriver" },
                { "hibernate.dialect", "NHibernate.Dialect.MySQLDialect" },
                { "hibernate.connection.connection_string", connectionString },
                #if DEBUG
                { "hibernate.show_sql", "true" }
                #endif
            };
            dbConfig.AddProperties(properties);
            dbConfig.Configure();

            // Create OR mapping
            var mapping = new ModelMapper();
            mapping.AddMapping(typeof(Mapping.TreeMapping));
            mapping.AddMapping(typeof(Mapping.NodeMapping));
            mapping.AddMapping(typeof(Mapping.EdgeMapping));
            mapping.AddMapping(typeof(Mapping.AttribMapping));
            var orMap = mapping.CompileMappingForAllExplicitlyAddedEntities();
            dbConfig.AddMapping(orMap);
        }


        /// <summary>
        /// (Re-)Creates the database schema. Use i.e. for testing.
        /// </summary>
        public void CreateSchema() {
            #if DEBUG
            new SchemaExport(dbConfig).Execute(true, true, false);
            #else
            new SchemaExport(dbConfig).Execute(false, true, false);
            #endif
        }


        public bool AddTree(Tree tree) {
            using (var session = SessionFactory.OpenSession()) {
                // Check if tree already exists
                if (session.Get<Tree>(tree.Id) != null)
                    return false;

                // Store tree
                using (var transaction = session.BeginTransaction()) {
                    session.Save(tree);
                    transaction.Commit();
                    return transaction.WasCommitted;
                }
            }
        }

        public bool DeleteTree(Tree tree) {
            using (var session = SessionFactory.OpenSession()) {
                using (var transaction = session.BeginTransaction()) {
                    session.Delete(tree);
                    transaction.Commit();
                    return transaction.WasCommitted;
                }
            }
        }

        public bool UpdateTree(Tree tree) {
            using (var session = SessionFactory.OpenSession()) {
                using (var transaction = session.BeginTransaction()) {
                    try {
                        session.Update(tree);
                        transaction.Commit();
                    } catch (HibernateException) {
                        transaction.Rollback();
                        return false;
                    }
                    return transaction.WasCommitted;
                }
            }
        }

        public Tree GetTreeById(int id) {
            using (var session = SessionFactory.OpenSession()) {
                return session.Get<Tree>(id);
            }
        }

        public Tree GetTreeByName(string name) {
            using (var session = SessionFactory.OpenSession()) {
                return session.CreateCriteria<Tree>().Add(Restrictions.Eq("Name", name)).UniqueResult<Tree>();
            }
        }

        public IList<Tree> GetTreesByName(string namePattern) {
            using (var session = SessionFactory.OpenSession()) {
                return session.CreateCriteria<Tree>().Add(Restrictions.Like("Name", namePattern)).List<Tree>();
            }
        }

    }

}
