﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DbConnection.Model {
    
    public class Node {

        public virtual int Id { get; protected set; }

        public virtual string Name { get; set; }

        public virtual Edge ParentEdge { get; set; }

        public virtual IList<Edge> ChildEdges { get; protected set; }

        public virtual IList<Attrib> Attributes { get; protected set; }

        public virtual bool IsRoot {
            get { return ParentEdge == null; }
        }

        public virtual bool IsLeaf {
            get { return ChildEdges.Count <= 0; }
        }


        internal Node()
            : this(null) {
        }

        public Node(string name) {
            this.Name = name;
            this.ParentEdge = null;
            this.ChildEdges = new List<Edge>();
            this.Attributes = new List<Attrib>();
        }

    }

}
