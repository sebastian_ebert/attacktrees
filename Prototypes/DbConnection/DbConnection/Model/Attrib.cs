﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DbConnection.Model {

    public class Attrib {

        public virtual int Id { get; protected set; }

        public virtual string Name { get; set; }

        public virtual string Value { get; set; }

        public virtual Node Owner { get; set; }

        internal Attrib()
            : this(null, null, null) {
        }

        public Attrib(Node owner, string name, string value) {
            this.Owner = owner;
            this.Name = name;
            this.Value = value;
        }

    }

}
