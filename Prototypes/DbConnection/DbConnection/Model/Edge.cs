﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DbConnection.Model {

    public class Edge {

        public virtual int Id { get; protected set; }

        public virtual Node ParentNode { get; set; }

        public virtual IList<Node> ChildNodes { get; protected set; }

        public virtual bool IsAnd {
            get { return ChildNodes.Count > 1; }
        }

        public virtual bool IsOr {
            get { return !IsAnd; }
        }
        

        internal Edge()
            : this(null) {
        }

        public Edge(Node parent) {
            this.ParentNode = parent;
            this.ChildNodes = new List<Node>();
        }

    }

}
