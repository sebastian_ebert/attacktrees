﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DbConnection.Model {
    
    /// <summary>
    /// Interface for tree repositories. Allows to store and retreive trees.
    /// </summary>
    public interface ITreeRepository {

        /// <summary>
        /// Adds a new tree to the repository.
        /// </summary>
        /// <param name="tree">Tree to add.</param>
        /// <returns><code>true</code> if the tree was successfully added to the repository.</returns>
        bool AddTree(Tree tree);

        /// <summary>
        /// Deletes an existing tree from the repositiory.
        /// </summary>
        /// <param name="tree">Tree to delete.</param>
        /// <returns><code>true</code> if the tree was found in the repository and deleted.</returns>
        bool DeleteTree(Tree tree);

        /// <summary>
        /// Updates an existing tree in the repository.
        /// </summary>
        /// <param name="tree">Tree to update.</param>
        /// <returns><code>true</code> if the tree was successfully updated.</returns>
        bool UpdateTree(Tree tree);

        /// <summary>
        /// Retreives a tree from the repository. The tree is identified
        /// by it's unique id.
        /// </summary>
        /// <param name="id">Unique id of the tree to retreive.</param>
        /// <returns>The tree for the given unique id or <code>null</code> if no such tree was found.</returns>
        Tree GetTreeById(int id);

        /// <summary>
        /// Retreives the first tree found in the repository that has the given name.
        /// </summary>
        /// <param name="name">Name of the tree to retreive.</param>
        /// <returns>The first tree in the repository that has the given name or <code>null</code> if
        /// no such tree was found.</returns>
        Tree GetTreeByName(string name);

        /// <summary>
        /// Retreives all trees whos name matches the given pattern. The pattern has to be a valid SQL
        /// LIKE expression.
        /// </summary>
        /// <param name="namePattern">Pattern for tree names to search for. Has to be a valid SQL LIKE
        /// pattern.</param>
        /// <returns>All trees in the repository whos names match the given pattern. If no
        /// such trees where found, the collection is empty.</returns>
        IList<Tree> GetTreesByName(string namePattern);

    }

}
