﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DbConnection.Model {
    
    public class Tree {

        public virtual int Id { get; protected set; }

        public virtual string Name { get; set; }

        public virtual string Description { get; set; }

        public virtual Node RootNode { get; set; }


        internal Tree()
            : this(null) {
        }

        public Tree(string name) {
            this.Name = name;
        }

    }

}
