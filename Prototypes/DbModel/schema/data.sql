use attacktrees;
delete from trees;
delete from nodes_edges;
delete from nodes_attributes;
delete from nodes;
delete from attributes;
delete from functions;

/*
INSERT ATTRIBUTES
*/
INSERT INTO attributes (id,name,type) 
VALUES (1,'possible','boolean'),
(2,'cost','int');

/*
INSERT NODES
*/
INSERT INTO nodes (id,name,timestamp_created)
VALUES (1,'Open Safe',null),
(2,'Pick Lock',null),
(3,'Learn Combo',null),
(4,'Cut Open Safe',null),
(5,'Install Improperly',null),
(6,'Find Written Combo',null),
(7,'Get Combo from Target',null),
(8,'Threaten',null),
(9,'Blackmail',null),
(10,'Eavesdrop',null),
(11,'Bribe',null),
(12,'Listen to Conversation',null),
(13,'Get Target to State Combo',null);

/*
INSERT EDGES
*/
-- root node
INSERT INTO nodes_edges (ancestor, descendant,edge_type, depth)
SELECT t.ancestor, 2, 'ref', t.depth+1
FROM nodes_edges AS t
WHERE t.descendant = 1
UNION ALL
SELECT 1, 1, 'ref', 0;
-- pick lock
INSERT INTO nodes_edges (ancestor, descendant,edge_type, depth)
SELECT t.ancestor, 2, 'ref', t.depth+1
FROM nodes_edges AS t
WHERE t.descendant = 1
UNION ALL
SELECT 2, 2, 'ref', 0;
-- learn combo
INSERT INTO nodes_edges (ancestor, descendant,edge_type, depth)
SELECT t.ancestor, 3, 'ref', t.depth+1
FROM nodes_edges AS t
WHERE t.descendant = 1
UNION ALL
SELECT 3, 3, 'ref', 0;
-- cut open
INSERT INTO nodes_edges (ancestor, descendant,edge_type, depth)
SELECT t.ancestor, 4, 'ref', t.depth+1
FROM nodes_edges AS t
WHERE t.descendant = 1
UNION ALL
SELECT 4, 4, 'ref', 0;
-- install improperly
INSERT INTO nodes_edges (ancestor, descendant,edge_type, depth)
SELECT t.ancestor, 5, 'ref', t.depth+1
FROM nodes_edges AS t
WHERE t.descendant = 1
UNION ALL
SELECT 5, 5, 'ref', 0;
-- find written combo (child of 'learn combo')
INSERT INTO nodes_edges (ancestor, descendant,edge_type, depth)
SELECT t.ancestor, 6, 'ref', t.depth+1
FROM nodes_edges AS t
WHERE t.descendant = 3
UNION ALL
SELECT 6, 6, 'ref', 0;
-- get combo from target (child of 'learn combo')
INSERT INTO nodes_edges (ancestor, descendant,edge_type, depth)
SELECT t.ancestor, 7, 'ref', t.depth+1
FROM nodes_edges AS t
WHERE t.descendant = 3
UNION ALL
SELECT 7, 7, 'ref', 0;
-- threaten (child of 'get combo from target')
INSERT INTO nodes_edges (ancestor, descendant,edge_type, depth)
SELECT t.ancestor, 8, 'ref', t.depth+1
FROM nodes_edges AS t
WHERE t.descendant = 7
UNION ALL
SELECT 8, 8, 'ref', 0;
-- blackmail (child of 'get combo from target')
INSERT INTO nodes_edges (ancestor, descendant,edge_type, depth)
SELECT t.ancestor, 9, 'ref', t.depth+1
FROM nodes_edges AS t
WHERE t.descendant = 7
UNION ALL
SELECT 9, 9, 'ref', 0;
-- eavesdrop (child of 'get combo from target')
INSERT INTO nodes_edges (ancestor, descendant,edge_type, depth)
SELECT t.ancestor, 10, 'ref', t.depth+1
FROM nodes_edges AS t
WHERE t.descendant = 7
UNION ALL
SELECT 10, 10, 'ref', 0;
-- bribe (child of 'get combo from target')
INSERT INTO nodes_edges (ancestor, descendant,edge_type, depth)
SELECT t.ancestor, 11, 'ref', t.depth+1
FROM nodes_edges AS t
WHERE t.descendant = 7
UNION ALL
SELECT 11, 11, 'ref', 0;
-- listen to conversation (child of 'eavesdrop')
INSERT INTO nodes_edges (ancestor, descendant,edge_type, depth)
SELECT t.ancestor, 12, 'and', t.depth+1
FROM nodes_edges AS t
WHERE t.descendant = 10
UNION ALL
SELECT 12, 12, 'ref', 0;
-- get target to state combo (child of 'eavesdrop')
INSERT INTO nodes_edges (ancestor, descendant,edge_type, depth)
SELECT t.ancestor, 13, 'and', t.depth+1
FROM nodes_edges AS t
WHERE t.descendant = 10
UNION ALL
SELECT 13, 13, 'ref', 0;

/*
INSERT ATTRIBUTES
*/
INSERT INTO nodes_attributes(node_id,attribute_id,value)
-- root
SELECT 1,1,'true'
UNION ALL
SELECT 1,2,'10000'
-- pick lock
UNION ALL
SELECT 2,1,'false'
UNION ALL
SELECT 2,2,'30000'
-- learn combo
UNION ALL
SELECT 3,1,'true'
UNION ALL
SELECT 3,2,'30000'
-- cut open
UNION ALL
SELECT 4,1,'true'
UNION ALL
SELECT 4,2,'30000'
-- install improperly
UNION ALL
SELECT 5,1,'false'
UNION ALL
SELECT 5,2,'30000'
-- find written combo
UNION ALL
SELECT 6,1,'false'
UNION ALL
SELECT 6,2,'30000'
-- get combo from target
UNION ALL
SELECT 7,1,'true'
UNION ALL
SELECT 7,2,'30000'
-- threaten
UNION ALL
SELECT 8,1,'false'
UNION ALL
SELECT 8,2,'30000'
-- blackmail
UNION ALL
SELECT 9,1,'false'
UNION ALL
SELECT 9,2,'30000'
-- eavesdrop
UNION ALL
SELECT 10,1,'false'
UNION ALL
SELECT 10,2,'30000'
-- bribe
UNION ALL
SELECT 11,1,'true'
UNION ALL
SELECT 11,2,'30000'
-- listen to conversation
UNION ALL
SELECT 12,1,'true'
UNION ALL
SELECT 12,2,'30000'
-- get target to state combo
UNION ALL
SELECT 13,1,'false'
UNION ALL
SELECT 13,2,'30000';

/*
INSERT TREE
*/
INSERT INTO trees (id,root_node_id, name,description)
VALUES (1,1,'Break Safe Tree','This tree describes possible attack vectors to break a safe. It is taken from Bruce Schneiers 
Paper on Attack Trees (1999)');

/*
INSERT FUNCTIONS
*/
INSERT INTO functions(id,name,statement) 
VALUES (1,'Cost Lower Than',
'SELECT * from nodes n
JOIN nodes_attributes na on n.id = na.node_id
JOIN attributes a on na.attribute_id = a.id
WHERE a.name = ? and CAST(na.value as DECIMAL) < ?;');

INSERT INTO functions_attributes(function_id,attribute_id,value)
VALUES(1,2,50000);