SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `attacktrees` ;
CREATE SCHEMA IF NOT EXISTS `attacktrees` DEFAULT CHARACTER SET utf8 ;
USE `attacktrees` ;

-- -----------------------------------------------------
-- Table `attributes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `attributes` ;

CREATE  TABLE IF NOT EXISTS `attributes` (
  `id` CHAR(16) BINARY NOT NULL ,
  `name` VARCHAR(255) NOT NULL ,
  `type` VARCHAR(30) NOT NULL DEFAULT 'string' ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Stores possible node attributes';


-- -----------------------------------------------------
-- Table `nodes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `nodes` ;

CREATE  TABLE IF NOT EXISTS `nodes` (
  `id` CHAR(16) CHARACTER SET 'latin1' BINARY NOT NULL ,
  `name` VARCHAR(255) NOT NULL ,
  `timestamp_modified` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Gets automatically updated on every insert / update.' ,
  `timestamp_created` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Set to NULL for auto-initialization with current timestamp' ,  
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'This table stores the attack trees nodes.';


-- -----------------------------------------------------
-- Table `nodes_attributes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `node_attributes` ;

CREATE  TABLE IF NOT EXISTS `node_attributes` (
  `node_id` CHAR(16) BINARY NOT NULL ,
  `attribute_id` CHAR(16) BINARY NOT NULL ,
  `value` VARCHAR(255) NULL DEFAULT NULL ,
  PRIMARY KEY (`node_id`, `attribute_id`) ,
  INDEX `fk_node_id_idx` (`node_id` ASC) ,
  INDEX `fk_attribute_id_idx` (`attribute_id` ASC) ,
  CONSTRAINT `fk_attribute_id`
    FOREIGN KEY (`attribute_id` )
    REFERENCES `attributes` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_node_id`
    FOREIGN KEY (`node_id` )
    REFERENCES `nodes` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT='Attaches attributes to nodes and sets values.';


-- -----------------------------------------------------
-- Table `nodes_edges`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `node_edges` ;

CREATE  TABLE IF NOT EXISTS `nodes_edges` (
  `ancestor` CHAR(16) BINARY NOT NULL ,
  `descendant` CHAR(16) BINARY NOT NULL ,
  `edge_type` VARCHAR(45) NOT NULL DEFAULT 'ref' ,
  `depth` BIGINT(20) NOT NULL DEFAULT '0' ,
  `and_group` BIGINT(20) DEFAULT NULL ,
  PRIMARY KEY (`ancestor`, `descendant`) ,
  INDEX `fk_ancestor_node_id_idx` (`ancestor` ASC) ,
  INDEX `fk_descendatn_node_id_idx` (`descendant` ASC) ,
  CONSTRAINT `fk_ancestor_node_id`
    FOREIGN KEY (`ancestor` )
    REFERENCES `nodes` (`id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_descendent_node_id`
    FOREIGN KEY (`descendant` )
    REFERENCES `nodes` (`id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Stores node connections as a closure table.';


-- -----------------------------------------------------
-- Table `trees`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `trees` ;

CREATE  TABLE IF NOT EXISTS `trees` (
  `id` CHAR(16) BINARY NOT NULL ,
  `root_node_id` CHAR(16) BINARY NOT NULL ,
  `name` VARCHAR(255) NOT NULL ,
  `description` TEXT NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_root_node_d_idx` (`root_node_id` ASC) ,
  CONSTRAINT `fk_root_node_id`
    FOREIGN KEY (`root_node_id` )
    REFERENCES `nodes` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT='Tags represent permissions and permission requirements which can be attached to users and trees.';

-- -----------------------------------------------------
-- Table `functions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `functions` ;

CREATE  TABLE IF NOT EXISTS `functions` (
  `id` CHAR(16) BINARY NOT NULL ,  
  `name` VARCHAR(255) NOT NULL ,
  `statement` TEXT NOT NULL ,
  PRIMARY KEY (`id`)  ,
  UNIQUE INDEX `uq_function_name` (`name` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT='Define functions as prepared statements.';

-- -----------------------------------------------------
-- Table `function_attributes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `function_attributes` ;

CREATE  TABLE IF NOT EXISTS `function_attributes` (
  `function_id` CHAR(16) BINARY NOT NULL ,
  `attribute_id` CHAR(16) BINARY NOT NULL ,
  `value` VARCHAR(255) NULL DEFAULT NULL ,
  PRIMARY KEY (`function_id`, `attribute_id`) ,
  INDEX `fk_function_id_idx` (`function_id` ASC) ,
  INDEX `fk_function_attribute_id_idx` (`attribute_id` ASC) ,
  CONSTRAINT `fk_function_attribute_id`
    FOREIGN KEY (`attribute_id` )
    REFERENCES `attributes` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_function_id`
    FOREIGN KEY (`function_id` )
    REFERENCES `functions` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT='Functions reference attributes.';

-- -----------------------------------------------------
-- Table `users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `users` ;
CREATE  TABLE `users` (
  `id` CHAR(16) BINARY NOT NULL ,
  `name` VARCHAR(45) NOT NULL,
  `password` VARCHAR(64) NOT NULL ,
  `salt` VARCHAR(64) NOT NULL,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) )
COMMENT = 'This table contains system user information. password = hash(pw+salt)';

-- -----------------------------------------------------
-- Table `tags`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tags` ;

CREATE TABLE `tags` (
  `id` char(16) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `name` varchar(45) NOT NULL,
  `permission` enum('create','read','update','delete') NOT NULL,
  PRIMARY KEY (`id`)
) 

ENGINE=InnoDB 
DEFAULT CHARSET=utf8 
COMMENT='Tags represent permissions and permission requirements which can be attached to users and trees.';

-- -----------------------------------------------------
-- Table `user_tags`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `user_tags` ;

CREATE  TABLE IF NOT EXISTS `user_tags` (
  `user_id` CHAR(16) BINARY NOT NULL ,
  `tag_id` CHAR(16) BINARY NOT NULL ,
  `active` TINYINT NOT NULL DEFAULT 1 ,
  PRIMARY KEY (`user_id`, `tag_id`) ,
  INDEX `fk_tags_user_id_idx` (`user_id` ASC) ,
  INDEX `fk_tags_tag_id_idx` (`tag_id` ASC) ,
  CONSTRAINT `fk_tags_user_id`
    FOREIGN KEY (`user_id` )
    REFERENCES `users` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tags_tag_id`
    FOREIGN KEY (`tag_id` )
    REFERENCES `tags` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT='Attaches tags to users.';

-- -----------------------------------------------------
-- Table `tree_tags`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tree_tags` ;

CREATE  TABLE IF NOT EXISTS `tree_tags` (
  `tree_id` CHAR(16) BINARY NOT NULL ,
  `tag_id` CHAR(16) BINARY NOT NULL ,
  `active` TINYINT NOT NULL DEFAULT 1 ,
  PRIMARY KEY (`tree_id`, `tag_id`) ,
  INDEX `fk_tree_tags_tree_id_idx` (`tree_id` ASC) ,
  INDEX `fk_tree_tags_tag_id_idx` (`tag_id` ASC) ,
  CONSTRAINT `fk_tags_tree_id`
    FOREIGN KEY (`tree_id` )
    REFERENCES `trees` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tree_tags_tag_id`
    FOREIGN KEY (`tag_id` )
    REFERENCES `tags` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT='Attaches tags to trees.';

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
