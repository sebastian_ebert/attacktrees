use attacktrees_with_functions;
/*
Execute a prepared statement
*/
SET @qry = (SELECT statement FROM functions f where f.name LIKE 'Cost Lower Than');
PREPARE stmt FROM  @qry;
SET @variable = (
SELECT name from attributes a 
join functions_attributes fa on a.id = fa.attribute_id);
SET @cost_upper_threshold = (
SELECT value from attributes a
JOIN functions_attributes fa on a.id = fa.function_id);
EXECUTE stmt USING @variable, @cost_upper_threshold;