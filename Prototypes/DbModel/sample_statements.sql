use attacktrees_with_functions;
/*
Deleting a node is basically a single statement

The Node is deleted and the subnodes are still attached to the parent of the deleted node
*/
delete from nodes where id = 3;

/*
Deleting a subtree is easy as well

(a little hacky because mysql doesn't allow subqueries on tables that are updated,
works without hacks if you use the id's, this was just for demonstration that we
can also delete by name)
*/
DELETE FROM nodes WHERE id IN (
    SELECT * FROM (
        select ne.descendant from nodes n
		join nodes_edges ne on n.name LIKE 'Learn Combo'
		where n.id = ne.ancestor
    ) as n_id
);

/*
Moving a subtree

e.g move the "Learn Combo" [3] Subtree to Node "Install Improperly" [5] 
(doesn't make any sense logically, just for demo purposes)
*/
DELETE a FROM nodes_edges AS a
JOIN nodes_edges AS d ON a.descendant = d.descendant
LEFT JOIN nodes_edges AS x
ON x.ancestor = d.ancestor AND x.descendant = a.ancestor
WHERE d.ancestor = 3 AND x.ancestor IS NULL;

INSERT INTO nodes_edges (ancestor, descendant, depth)
SELECT supertree.ancestor, subtree.descendant,
supertree.depth+subtree.depth+1
FROM nodes_edges AS supertree JOIN nodes_edges AS subtree
WHERE subtree.ancestor = 3
AND supertree.descendant = 5
order by supertree.depth;

/*
Printing a tree (SQL)
*/
select group_concat(n.name order by a.depth desc separator ' -> ') as path
from nodes_edges d
join nodes_edges a on (d.descendant = a.descendant)
join nodes n on (n.id = a.ancestor)
where d.ancestor = 1 and d.descendant != d.ancestor 
group by d.descendant;

/*
Selecting all attributes of a node
*/
select n.id,n.name nodename,a.name attr_name, a.type as attr_type, na.value
from nodes n
join nodes_attributes na on n.id = na.node_id
join attributes a on na.attribute_id = a.id
where n.id = 14;

/*
Inserting a new leaf node
*/
-- Create node
INSERT INTO nodes (id,name,timestamp_created)
VALUES (null,'Hulk Smash',null);

-- Add edges
INSERT INTO nodes_edges (ancestor, descendant,edge_type, depth)
SELECT t.ancestor, last_insert_id(), 'ref', t.depth+1
FROM nodes_edges AS t
WHERE t.descendant = 7
UNION ALL
SELECT last_insert_id(), last_insert_id(), 'ref', 0;

-- Set some attributes
INSERT INTO nodes_attributes(node_id,attribute_id,value)
SELECT last_insert_id(),1,'true'
UNION ALL
SELECT last_insert_id(),2,'1000000';

/*
Executing a basic function
*/
-- Select All Nodes which have a cost of less than 50000
SELECT * from nodes n
JOIN nodes_attributes na on n.id = na.node_id
JOIN attributes a on na.attribute_id = a.id
WHERE a.name = 'cost' and CAST(na.value as DECIMAL) < 50000;

-- Select All Nodes which only take 2 steps
SELECT * from nodes n
JOIN nodes_edges ne on n.id = ne.descendant
WHERE ne.depth = 2 GROUP BY id;

-- Select All Nodes which are possible
SELECT * from nodes n
JOIN nodes_attributes na on n.id = na.node_id
JOIN attributes a on na.attribute_id = a.id
WHERE a.name = 'possible' and na.value LIKE 'true';

